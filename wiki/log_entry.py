#coding=utf8
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION 
from django.contrib.contenttypes.models import ContentType
from django.utils.encoding import force_unicode
from lib.cuser.middleware import CuserMiddleware


SHOP_SCAN = 4 		# сканирование товаров в магазине
SHOP_UPDATE = 5 	# обновление товаров в магазине
SHOP_HAS_PRODUCT_UPDATE = 6	# обновление значений
SHOP_HAS_PRODUCT_DISABLE = 7# обновление значений

def log_entry_add(obj):
	log_entry_fix(obj, ADDITION)

def log_entry_change(obj):
	log_entry_fix(obj, CHANGE)
	
def log_entry_del(obj):
	log_entry_fix(obj, DELETION)

def log_entry_fix(obj, type, mes='-'):
	user = CuserMiddleware.get_user()
	if not user:
		from django.contrib.auth.models import User
		user = User.objects.get(username='robot')
	if user:
		LogEntry.objects.log_action(
			user_id			= user.id, 
			content_type_id	= ContentType.objects.get_for_model(obj).pk,
			object_id		= obj.pk,
			object_repr		= force_unicode(obj), 
			change_message 	= mes, 
			action_flag		= type
		)