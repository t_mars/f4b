#coding=utf8
import time, sys
from datetime import datetime
from optparse import make_option
import json, os, logging

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from wiki.models import *
from wiki.log_entry import *
from lib.prodmatch import Grouper, Indexer

class Command(BaseCommand):
	help = u'Parse shop'
	option_list = BaseCommand.option_list + (
		# конкретный магазин
		make_option(
			"-s", 
			"--shop-id", 
			dest = "sid",
			metavar = "INT",
		),
		# сохранять
		make_option('--debug',
            action='store_true',
			dest='debug',
			default=False,
		),
		# выводить в косоль
		make_option('--out',
            action='store_true',
			dest='out',
			default=False,
		),
		# искать новые товары
		make_option('--walk',
            action='store_true',
			dest='walk',
			default=False,
		),
	)

	def init(self, debug=False):
		self.debug = debug
		self.indexer = Indexer()
		self.grouper = Grouper()

	def handle(self, *args, **options):
		self.init()
		
		self.debug = options['debug']
		
		# вывод логов
		if options['out']:
			logging.basicConfig(#filename='log',
				level=logging.INFO&logging.DEBUG, 
				format='%(message)s')
		else:
			from time import gmtime, strftime
			fn = '%s.log' % (strftime("%d.%m.%Y", gmtime()))
			logging.basicConfig(filename=os.path.join(settings.SHOP_PARSER_LOG, fn), 
				level=logging.INFO, format='%(message)s')
	
		sid = options.get('sid', None)
		if sid:
			shops = Shop.objects.filter(id=sid)
		else:
			shops = Shop.objects.all()
		
		self.run(shops, options['walk'])

	def find_info(self, info_list, url, hash, index):
		# ищем товар соответствующий данным
		if url not in info_list:
			return None

		if hash in info_list[url]:
			# пытаемся найти по хешу значений
			info = info_list[url][hash]
			del info_list[url][hash]
			return info

		# определяем схожий товар
		if len(info_list[url]) == 1:
			# если один товар на странице то не сравниваем
			info = info_list[url].values()[0]
			del info_list[url]
			return info

		# если несколько товаров то сравниваем
		info_hash = ''
		diff = 999
		for h,i in info_list[url].iteritems():
			inx = self.indexer.get_index(i['name'], i['brand'], i['packing'])
			s = self.indexer.get_signs(**inx)
			d = self.grouper.diff_groups(s, index, strict=False)
			if d >= 0 and diff > d:
				diff = d
				info_hash = h

		# не найден похожий
		if not len(info_hash):
			return None

		print 'url',url
		print 'hash',info_hash
		info = info_list[url][info_hash]
		del info_list[url][info_hash]
		return info

	def reset_counters(self):
		self.counters = {
			'old_count': 0,
			'old_found': 0,
			'old_not_found': 0,
			'old_new_values': 0,
			'old_save': 0,
			'old_save_error': 0,
			'new_found': 0,
			'new_save': 0,
			'new_save_error': 0
		}

	def run(self, shops, walk):
		for shop in shops:
			logging.info(u'Shop [%d] %s' % (shop.id, shop.name))
			
			shop.scan_status = True
			shop.save()

			self.reset_counters()
	
			# готовим ссылки товаров
			items = ProductItem.objects.filter(shop=shop)
			urls = [item.url for item in items]
			
			logging.info(u'has %d items' % (len(items)))
			self.counters['old_count'] = len(items)
		
			try:
				start = time.time()
				info_list, urls = shop.parser.scan(urls, walk)
				stop = time.time()
				logging.info(u'scanned %d urls' % len(urls))
				logging.info(u'parsing time %.2f' % (stop-start))
			except Exception as e:
				logging.info(u'Parser error: %s (%s)' % (str(e), sys.exc_traceback.tb_lineno))
				shop.scan_status = False
				shop.save()
				continue 
			
			self.save_old(items, info_list)

			self.save_new(shop, info_list)

			if not self.debug:
				log_entry_fix(shop, SHOP_UPDATE, 
					u'UPD %d/%d, NEW %d/%d' % (
						self.counters['old_save'],
						self.counters['old_count'], 
						self.counters['new_save'],
						self.counters['new_found'], 
					)
				)
			
			shop.scan_status = None
			shop.save()

			logging.info(u'Старых всего:       %d' % self.counters['old_count'])
			logging.info(u'       найдено:     %d' % self.counters['old_found'])
			logging.info(u'       не найдено:  %d' % self.counters['old_not_found'])
			logging.info(u'       обновлено:   %d' % self.counters['old_new_values'])
			logging.info(u'       сохранено:   %d' % self.counters['old_save'])
			logging.info(u'       не сохранено:%d' % self.counters['old_save_error'])
			logging.info(u'Новых найдено:      %d' % self.counters['new_found'])
			logging.info(u'      сохранено:    %d' % self.counters['new_save'])
			logging.info(u'      не сохранено: %d' % self.counters['new_save_error'])

	def save_new(self, shop, info_list):
		for url in info_list:
			for hash,info in info_list[url].iteritems():
				self.counters['new_found'] += 1
				try:
					item = ProductItem()
					item.hash = hash
					item.shop = shop
					item.url = url
					item.name = info['name']
					item.price = info['price']
					item.image = info['image']
					item.available = info['available']
					item.brand = info['brand']
					item.category = info['category']
					item.packing = info['packing']
					item.description = info['description']
					
					# индексируем
					self.set_index(item)
					
					if not self.debug:
						item.save()
						self.counters['new_save'] += 1
				except Exception as e:
					self.counters['new_save_error'] += 1
					logging.info(u'Save error %s: %s (%s)' % (url, str(e), sys.exc_traceback.tb_lineno))
				
	def save_old(self, items, info_list):
		changes = ['name', 'brand', 'packing', 'price', 'available']
		reset_product_ids = set()
		for item in items:
			iden = u'(item %d %s)' % (item.id, item.url)
			upds = []
			signs = self.indexer.get_signs(**item.get_index())
			info = self.find_info(info_list, item.url, item.hash, signs)
			
			# обновляем цены 
			if item.product_id:
				reset_product_ids.add(item.product_id)
			
			if info == None:
				if not self.debug:
					item.actual = False
					item.save()
					log_entry_fix(item, SHOP_HAS_PRODUCT_DISABLE, u'Не найден на сайте.')
				self.counters['old_not_found'] += 1

				logging.info(u'Not found %s' % (iden))
				continue
			
			self.counters['old_found'] += 1
			item.updated = datetime.now()

			# изменяем значения
			item.description = info['description']
			for key in changes:
				# ['',None] - цена и упаковка могут быть скрыты
				if getattr(item, key) != info.get(key):# and info.get(key) not in ['',None]:
					upds.append(u'%s [%s]>[%s]' % (key, getattr(item, key), info.get(key)))
					setattr(item, key, info.get(key))
				
			# значения изменены
			if item.hash != info['hash'] and len(upds):
				self.counters['old_new_values'] += 1
					
				logging.info(u'New vals %s' % (iden))
				logging.info(u';'.join(upds))
				logging.info(u'---')
				
				# обновляем значения 
				item.hash = hash

			if self.debug:
				continue

			# сохраняем значения
			try:
				# индексируем
				self.set_index(item)

				item.actual = True
				item.save()
				
				self.counters['old_save'] += 1
				log = ProductItemLog()
				log.shop_has_product = item
				log.price = info['price']
				log.available = info['available']
				log.update = datetime.now()
				log.save()

				log_entry_fix(item, SHOP_HAS_PRODUCT_UPDATE, u'Обновлено: %s.' % (';'.join(upds)))
			
			except Exception as e:
				log_entry_fix(item, SHOP_HAS_PRODUCT_UPDATE, u'Ошибка сохранения.')
				self.counters['old_save_error'] += 1
				logging.info(u'Save error %s: %s' % (iden, str(e)))

		for product in Product.objects.filter(pk__in=reset_product_ids):
			product.reset_prices()		

	def set_index(self, item):
		index = self.indexer.get_index(item.name, item.brand, item.packing)
		item.name_norm 			= index['name_norm']
		item.index_name 		= index['name']
		item.index_name_digit	= index['name_digit']
		item.index_brand 		= index['brand']
		item.index_packing 		= index['packing']