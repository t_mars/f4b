import pprint
import uuid
import logging
import pickle
import re

from django.core.management.base import BaseCommand, CommandError
from lib.parser.spider import ScanSpider
from lib.parser.pparser import InfoParser
from lib.prodmatch import Indexer

class Command(BaseCommand):
	
	def get_prods(self):
		prods = {}
		indexer = Indexer()

		def _prod(url, p):
			ing = self.ings[p['code']][0]
			packing = ing['servings']+' servings ' + \
				re.sub('(\d+) - ', r'\1 servings ', p['packing']) + \
				re.sub('(\d+) - ', r'\1 servings ', ing['packing']) 
	
			index = indexer.get_index(p['name'], p['brand'], packing)
			signs = indexer.get_signs(
				name=index['name'],
				name_digit=index['name_digit'],
				brand=index['brand'],
				packing=index['packing'],
			)
			return {
				'url': url,
				'name': p['name'],
				'brand': p['brand'],
				'image': p['image'],
				'code': p['code'],
				'category1': p['category1'],
				'category2': p['category2'],
				'serving_size': ing['serving_size'].encode('utf8'),
				'index_name': index['name'],
				'index_name_digit': index['name_digit'],
				'index_brand': index['brand'],
				'index_packing': index['packing'],
				'ingredients': ing['table'],
				'signs': signs,
			}

		for url,prod in self.infos.iteritems():
			if prod[0]['packings_exist'] == '1':
				for p in prod:
					prods[p['code']] = _prod(url, p)
					
			else:
				p = prod[0]
				p['code'] = p['single_code']
				prods[p['code']] = _prod(url, p)
				pass
		
		return prods

	def save_prods(self):
		indexer = Indexer()
		
		prods = []
		for _,p in self.get_prods().iteritems():
			if not len(p['index_brand']):
				continue
			prods.append(p)

		pickle.dump(prods, open('bb-prods.dat', 'w'))

	def out_prods(self):
		prods = self.get_prods()
		keys = ['url','name','brand','image',
			'code','category1','category2','serving_size',
			'index_name','index_name_digit','index_brand','index_packing']
		print ';'.join(keys)
		for _,p in prods.iteritems():
			for k in keys:
				try:
					print p[k],
				finally:
					print ';',
			print 

	def out_ings(self):
		for code,ings in self.ings.iteritems():
			print '<h1>%s<h1><br>%s' % (
				code, 
				ings[0]['table'].encode('utf8'),
			)

	def handle(self, *args, **options):
		self.prepare()

		for mode in args:
			if hasattr(self, mode):
				getattr(self, mode)()
			else:
				print 'error'
				return

	def info(self):
		print 'ings',len(self.ings)
		print 'prods',len(self.infos)
			
	def prepare(self):
		self.host = 'http://www.bodybuilding.com/'
		self.proxy = False
		self.thread_number = 16
		
		logging.basicConfig(level=logging.INFO&logging.DEBUG,format='%(message)s')
		
		try:
			self.infos = pickle.load(open('bb-infos.dat', 'r'))
		except:
			self.infos = {}
		
		try:
			self.ings = pickle.load(open('bb-ings.dat', 'r'))
		except:
			self.ings = {}

	def scan_ings(self):
		codes = set()
		for url,infos in self.infos.iteritems():
			for info in infos:
				if len(info['code']):
					codes.add(info['code'])
				if len(info['single_code']):
					codes.add(info['single_code'])
		
		selectors = {
			"table": {
				"path": "//div[@id='label_preview']",
				"type": "html",
				"required": "1",
			},
			"servings": {
				"path": "//td[contains(@class,'seq_span')][contains(.,'Servings Per Container')]",
				"type": "text",
				"required": "0",
				"parser": "substr@Servings Per Container@"
			},
			"serving_size": {
				"path": "//td[contains(@class,'seq_span')][contains(.,'Serving Size')]",
				"type": "text",
				"required": "0",
				"parser": "substr@Serving Size@"
			},
			"packing": {
				"path": "//td[contains(@class,'seq_span label_size')]",
				"type": "text",
				"required": "0",
			}
		}
		parser = InfoParser(selectors)
				
		cs = []
		def callback(lst, infos, url):
			_,code = url.split('=')
			# print code,infos
			if code in self.ings:
				print 'OLD',len(cs)
				cs.append(code) 
			else:
				print 'NEW',len(self.ings)
			self.ings[code] = infos
			
		base_urls = ['http://www.bodybuilding.com/store/common/ajax/catalog/ingredient-xhr.jsp?skuId=%s' % code for code in codes]
		spider = ScanSpider(host=self.host, parser=parser, 
			base_urls=base_urls,
			walk=False,
			thread_number=self.thread_number, proxy=self.proxy,
			callback=callback)
		
		# spider.debug = True
		
		try:
			spider.run()
		except KeyboardInterrupt:
			print 'Finished.'

		pickle.dump(self.ings, open('bb-ings.dat', 'w'))
		print 'ings',len(self.ings)

	def scan_prods(self):
		selectors =  {
			"name": {
				"path": ".//h1[@class='fn']/text()[last()]",
				"type": "text", 
				"required": "1",
			},
			"image": {
				"path": ".//a[@class='bb-image-viewer']",
				"type": "attr@href", 
				"required": "1",
			},
			"brand": {
				"path": ".//h1[@class='fn']//span[@class='presents']",
				"type": "text",
				"parser": "substr@@:",
				"required": "1",
			},
			"packing": {
				"path": ".//select[@id='ingredientLabelSelector']/option",
				"type": "text",
				"multi": "1",
				"required": "0",
			},
			"code": {
				"path": ".//select[@id='ingredientLabelSelector']/option",
				"type": "attr@value",
				"multi": "1",
				"required": "0",
			},
			"packings_exist": {
				"path": ".//select[@id='ingredientLabelSelector']",
				"type": "exist",
			},
			"single_code": {
				"path": ".//div[@id='label_preview']",
				"type": "attr@data-ingredient-sku",
				"required": "1",
			},
			"category1": {
				"path": ".//a[@class='bb-crumb__link'][position()=last()-1]",
				"type": "text",
				"required": "0",
			},
			"category2": {
				"path": ".//a[@class='bb-crumb__link'][position()=last()]",
				"type": "text",
				"required": "0",
			}
		}
		base_urls = [
			'http://www.bodybuilding.com/store/bcaa.html'
			'http://www.bodybuilding.com/store/goalpreworkout.htm',
			'http://www.bodybuilding.com/store/protein.htm',
			'http://www.bodybuilding.com/store/recovery.htm',
			'http://www.bodybuilding.com/store/goalintraworkout.htm',
		]
		parser = InfoParser(selectors)

		urls = []
		def callback(lst, infs, url):
			urls.append(url)
			if url in self.infos.keys():
				print 'OLD',len(urls)
			else:
				print 'NEW',len(lst)
			self.infos[url] = infs
			return True

		spider = ScanSpider(host=self.host, parser=parser, 
			base_urls=base_urls,
			walk=True,
			thread_number=self.thread_number, proxy=self.proxy, 
			excludes=['(?!/store/).+'],
			# includes=['/store/[^/]+/[^/]+\.html'],
			callback=callback)
		
		# spider.debug = True
		
		try:
			spider.run()
		except KeyboardInterrupt:
			print 'Finished.'

		pickle.dump(self.infos, open('bb-infos.dat', 'w'))
		print 'prods',len(self.infos)