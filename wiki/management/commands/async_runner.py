#coding=utf8
import os, logging, pickle
from time import sleep
from multiprocessing import Process, Pipe
from StringIO import StringIO

from django.db import transaction
from django.core.management.base import BaseCommand

from wiki.models import AsyncTask

def is_active_proc(pid):
	if pid == None:
		return False
	try:
		os.kill(pid, 0)
	except OSError:
		return False
	else:
		return True
		
class Command(BaseCommand):
	
	def handle(self, *args, **options):
		self.max_try_count = 3
		self.run()
	
	def run_task(self, task):
		
		log_stream = StringIO()
		log_handler = logging.StreamHandler(log_stream)
		
		logging.getLogger().addHandler(log_handler)
		logging.getLogger().setLevel(logging.INFO)
		
		log = ''
		try:
			task.status = AsyncTask.EXECUTE
			task.pid = os.getpid()
			task.try_count += 1
			task.save()

			print "Запуск %d [%s]" % (task.id, task.comment.encode('utf8'))
			args = pickle.loads(task.arguments)
			kwargs = pickle.loads(task.kwarguments)	
			
			result = getattr(AsyncTask, '_'+task.command)(*args, **kwargs)
			print "Завершен"

			log_handler.flush()
			log = unicode(log_stream.getvalue())
			
			task.result = log + u'\nRESULT: ' + unicode(result)
			task.pid = None
			task.status = AsyncTask.SUCCESS
			task.save()
		
		except Exception as e:
			print 'Ошибка выполнения:', e
			import traceback
			
			task.result = log 
			task.result += u'\nEXCEPTION: \n' + unicode(traceback.format_exc())
			task.status = AsyncTask.FAILURE
			task.pid = None
			task.save()

	def run(self):
		# закрываем задачи с исчерпанным кол-вом повторов
		AsyncTask.objects\
			.filter(try_count__gt=self.max_try_count,status__in=[AsyncTask.EXECUTE,AsyncTask.FAILURE])\
			.update(status=AsyncTask.CLOSED,pid=None)
		
		# убиваем задачи
		tasks = AsyncTask.objects.raw(
			'SELECT * FROM wiki_asynctask at '
			'WHERE at.status=%d '
			'ORDER BY at.id FOR UPDATE' % (AsyncTask.KILL)
		)
		for task in tasks:
			try:
				os.kill(task.pid, 9)
			except:
				pass
			task.pid = None
			task.status = AsyncTask.KILLED
			task.save()
		
		# проверка 'отвалившихся' задач
		tasks = AsyncTask.objects.raw(
			'SELECT * FROM wiki_asynctask at '
			'WHERE at.status=%d '
			'ORDER BY at.id FOR UPDATE' % (AsyncTask.EXECUTE)
		)
		for task in tasks:
			if is_active_proc(task.pid):
				continue
			print "Восстановление задачи %d [%s]" % (task.id, task.comment.encode('utf8'))
			self.run_task(task)
			return

		# проверка аварийно завершенных задач
		tasks = AsyncTask.objects.raw(
			'SELECT * FROM wiki_asynctask at '
			'WHERE at.status=%d '
			'ORDER BY at.id FOR UPDATE' % (AsyncTask.FAILURE)
		)
		for task in tasks:
			print "Перезапуск задачи %d [%s]" % (task.id, task.comment.encode('utf8'))
			self.run_task(task)
			return

		# запуск новых задач
		tasks = AsyncTask.objects.raw(
			'SELECT * FROM wiki_asynctask at '
			'WHERE at.status=%d '
			'ORDER BY at.id FOR UPDATE' % AsyncTask.CREATED
		)
		for task in tasks:
			print "Запуск задачи %d [%s]" % (task.id, task.comment.encode('utf8'))
			self.run_task(task)
			return

		print 'Нет задач'