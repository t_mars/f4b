#coding=utf8
import operator, time, pickle, sys
import re, operator

from django.core.management.base import BaseCommand, CommandError
from django.db import models, connection

from lib.prodmatch import Grouper,Indexer
from wiki.models import *

class Command(BaseCommand):
	
	modes = [
		('clear', 'Удаление всех продуктов'),
		('group', 'Группировка товаров'),
		('categorize', 'Разбиение продуктов по категориям'),
		('validate', 'Поиск ошибок в распределении товаров'),
		('find_info', 'Поиск bb.com'),
		('reset', 'Сброс кешированных данных'),
		('merge', 'Объединения товаров с разной упаковкой'),
	]

	def handle(self, *args, **options):
		if 'help' in args:
			for mode,desc in self.modes:
				print '%s: %s' % (mode, desc)
			return

		if len(args) == 0 or args == ('clear',):
			args += ('group', 'categorize', 'validate', 'bb_find', 'reset_prices', 'merge')
		
		if 'clear' not in args:
			self._init()

		for mode,desc in self.modes:
			if mode in args:
				print '%s:' % desc
				getattr(self, '_'+mode)()

	def _merge(self):
		product_ids = Product.objects.filter(
			primary_product=None,
			is_primary=False,
			original_product=None
		).values_list('id',flat=True)
		count = 0
		excludes = set()
		for id in product_ids:
			product = self.products[id]
			signs = self.product_signs[id]

			sys.stdout.write('id=%d %d/%d   \r' % (product.id, count, product_ids.count()))
			sys.stdout.flush()
				
			if product.id in excludes:
				continue

			ns = [models.Q(**{'index_name__icontains': n}) for n in signs['name']]
			cond_equal = reduce(operator.and_, ns)

			ps = Product.objects.exclude(id=product.id)\
				.filter(cond_equal)\
				.filter(
					brand_id=product.brand_id,
					category_id=product.category_id,
					original_product=None,
				).values_list('id',flat=True)

			slaves = []
			for id in ps:
				if set(self.product_signs[id]['name']) == signs['name'] \
					and set(self.product_signs[id]['name_digit']) == signs['name_digit']:
					slaves.append(self.products[id])

			if len(slaves):
				count += 1

				# если объединение уже произошло
				primary = None
				for slave in slaves:
					if slave.primary_product:
						primary = slave.primary_product
						break

				primary = primary or product
				
				print '%d основной [%d] %s: %s' % (count, primary.id, 
					primary.name.encode('utf8'), str([s.id for s in slaves]))
				
				slaves = list(slaves) + [product]
				for slave in slaves:
					excludes.add(slave.id)
					slave.primary_product = primary
					slave.is_primary = False
					slave.save()
				
				primary.primary_product = None
				primary.is_primary = True
				primary.save()

	def _init(self):
		# 0. Инициализация
		self.item_threshold = 5
		self.shop_threshold = 3
		
		self.indexer = Indexer()
		self.grouper = Grouper()
		
		# 1. выбираем все ProductItem без товара
		ss = ProductItem.objects.filter(product_id__isnull=True)
		self.items = {}
		self.item_signs = {}
		for s in ss:
			self.item_signs[s.id] = self.indexer.get_signs(**s.get_index())
			self.items[s.id] = s
		print 'Товаров', len(self.items)

		# 2. выбираем все Product и их признаки
		ps = Product.objects.all()
		self.products = {}
		self.product_signs = {}
		for p in ps:
			self.products[p.id] = p
			self.product_signs[p.id] = self.indexer.get_signs(**p.get_index())
		print 'Продуктов', len(self.products)


	def _reset(self):
		count = 0
		for id,product in self.products.iteritems():
			product.reset_cache()

			sys.stdout.write('id=%d %d/%d   \r' % (product.id, count, len(self.products)))
			sys.stdout.flush()
			count += 1


	def _bb_find(self):
		infos = pickle.load(open('bb-prods.dat', 'r'))
		print 'infos',len(infos)
		
		info_signs = {}
		for ind,info in enumerate(infos):
			info_signs[ind] = self.indexer.get_signs(
				name=info['index_name'], 
				name_digit=info['index_name_digit'], 
				brand=info['index_brand'], 
				packing=info['index_packing'], 
			)
		
		ind = 0
		count = 0
		for id,sign in self.product_signs.iteritems():
			sys.stdout.write('id=%d %d/%d найдено:%d  \r' % (id, ind, len(self.product_signs), count))
			sys.stdout.flush()
			ind += 1

			# ищем лучшее схождение
			inds = []
			diff = None
			for i,isign in info_signs.iteritems():
				d = self.grouper.diff_groups(sign, isign, strict=False, packing=False)
				if d >= 0:
					if d < diff or diff is None:
						inds = []
						diff = d
					if d == diff:
						inds.append(i)					
			if not len(inds):
				continue

			# смотрим упаковки
			packing = sign['packing']
			for item in ProductItem.objects.filter(product_id=id):
				pack = self.indexer.get_sign_packing(item.index_packing)
				for k,av in pack.iteritems():
					if k in packing:
						bv = packing[k]
						if abs(av-bv) <= (max(av, bv) * 0.1):
							packing[k] = max(av, bv)
					else:
						packing[k] = av

			# pack_sims = {infos[i]['code']:set() for i in inds}
			pack_sims = {i:0 for i in inds}
			for i in inds:
				isign = info_signs[i]
				for k,v in packing.iteritems():
					if k not in isign['packing']:
						continue
					iv = isign['packing'][k]
					if abs(v-iv) > (max(v, iv) * 0.1):
						continue
					# pack_sims[i].add( (k,iv) )
					pack_sims[i] += 1
			
			# max_sim = max([len(ps) for ps in pack_sims.values()])
			max_sim = max(pack_sims.values())
			if max_sim == 0:
				continue
			
			inds = [i for i,v in pack_sims.iteritems() if v == max_sim]
			
			info = infos[inds[0]]
			info_sign = info_signs[inds[0]]
			# сохраняем данные
			product = self.products[id]
			# product.name = info['name'] # потому что whey gold standart + natural 
			# product.image = info['image']
			product.serving_size = info['serving_size']
			product.ingredients = info['ingredients']
			product.save()
			count += 1

		print 'Найдено на bb.com %d            ' % count

	def _clear(self):
		ProductItem.objects.all().update(product=None,product_reason=None)
		SimilarProduct.objects.all().delete()
		SimilarItem.objects.all().delete()
		Product.objects.all().delete()

		cursor = connection.cursor()
		cursor.execute("TRUNCATE TABLE `wiki_similarproduct`")
		cursor.execute("TRUNCATE TABLE `wiki_similaritem`")
		cursor.execute("TRUNCATE TABLE `wiki_product`")

		self._init()

	def _group(self):
		self.counts = {
			'exist': 0,
			'new': 0,
			'similar': 0,
		}

		# 3. обход всех ProductItem по кот можно создать товар
		print 'Создание групп:'
		groups = self.create_groups()
		print 'Создано групп: %d' % len(groups)
		
		print 'Сопоставление товаров:'
		self.collate_items(groups)

		print 'Сопоставление продуктов:'
		self.collate_products()

		print ''
		print 'Создано продуктов: %d' % self.counts['new']
		print 'Найдено товаров: %d' % self.counts['exist']
		print 'Схожих товаров: %d' % self.counts['similar']
		

	# поиск слишком больших разниц в ценах 
	# и не совпадающих упаковок среди товаров внутри одного продукта
	def _validate(self):
		count = 0
		ind = 0
		for id,product in self.products.iteritems():
			sys.stdout.write('id=%d %d/%d найдено=%d  \r' % (product.id, ind, len(self.products), count))
			sys.stdout.flush()
			ind += 1

			items = ProductItem.objects.filter(product_id=id)
			
			# проверка упоковок
			packing = self.product_signs[id]['packing']
			try:
				for item in items:
					pack = self.indexer.get_sign_packing(item.index_packing)
					for k,av in pack.iteritems():
						if k in packing:
							bv = packing[k]
							if abs(av-bv) <= (max(av, bv) * 0.1):
								packing[k] = max(av, bv)
							else: # не совпадает упаковка
								raise Exception()
						else:
							packing[k] = av
			except:
				product.error_code = Product.ERROR_PACKING
				product.save()
				count += 1
				continue

			# проверка цен
			prices = [item.price for item in items]
			if not prices:
				continue
			max_price = float(max(prices))
			min_price = float(min(prices))
			t = max_price/min_price
			if t > 10:
				product.error_code = Product.ERROR_PRICE
				product.save()
				count += 1

		print 'Найдено ошибок %d            ' % count

	# разбиение по категориям
	def _categorize(self):
		self.categories = {}
		root_categories = []
		for c in Category.objects.all():
			self.categories[c.id] = c
			if c.level == 0:
				root_categories.append(c)
		
		count = 0
		success = 0;
		for id,product in self.products.iteritems():
			sys.stdout.write('id=%d %d/%d   \r' % (product.id, count, len(self.products)))
			sys.stdout.flush()
			count += 1

			category = self.identify_category(product, root_categories)
			if category:
				product.category = category
				product.save()
				success += 1
		print '\nsuccess',success

	def collate_products(self):
		count = 0
		for prod_id,prod_signs in self.product_signs.iteritems():
			sys.stdout.write('%d/%d            \r' % (count,len(self.product_signs)))
			sys.stdout.flush()
			count += 1

			ns = [models.Q(**{'index_name__icontains': n}) for n in prod_signs['name']]
			cond_equal = reduce(operator.and_, ns)
			
			sample_ids = Product.objects.values_list('id', flat=True)\
				.filter(cond_equal,index_brand=prod_signs['brand'])
			
			if not len(sample_ids):
				continue

			# ищем НЕчеткое соответствие
			similar_product_id = None
			diff = None
			for id in sample_ids:
				if id == prod_id:
					continue 
				# если добавились новые товары с новыми id
				if id not in self.product_signs: 
					continue 
				d = self.grouper.diff_groups(prod_signs, self.product_signs[id], strict=False)
				if d >= 0 and (d < diff or not diff):
					diff = d
					similar_product_id = id
			if not similar_product_id:
				continue
			
			print 'collate product',prod_id,similar_product_id,diff,

			sp = SimilarProduct()
			sp.source_id = prod_id
			sp.target_id = similar_product_id
			sp.diff = diff
			try:
				sp.save()
				print 'OK'
			except:
				print 'error.'


	def collate_items(self, groups):
		count = 0
		for gp in groups:
			sys.stdout.write('%d/%d            \r' % (count,len(groups)))
			sys.stdout.flush()
			count += 1
		
			group = gp['ids']
			cond = gp['cond']
			base_id = gp['base_id']
			base_signs = self.item_signs[base_id]

			# ищем НЕчеткое соответствие
			sample_ids = Product.objects.values_list('id', flat=True).filter(cond)
			similar_ids = {}
			for id in sample_ids:
				# если добавились новые товары с новыми id
				if id not in self.product_signs: 
					continue 
				d = self.grouper.diff_groups(base_signs, self.product_signs[id], 
					strict=False, left_prioritet=True)
				if d >= 0:
					if d not in similar_ids:
						similar_ids[d] = []
					similar_ids[d].append(id)
			
			if not len(similar_ids):
				continue

			min_diff = min(similar_ids.keys())
			# для точных совпадений и единственных совпадений сразу совмещаем
			if min_diff == 0 or len(similar_ids[min_diff]) == 1:
				prod_id = similar_ids[min_diff][0]
				product = self.products[prod_id]
				# в случае если совпадают бренды - fix 18/02/2015 (иначе куча мусора добавляется)
				if base_signs['brand'] == product.index_brand:
					self.save_group_products(group, product, min_diff)
					self.counts['similar'] += len(group)
					continue
				
			# для остальных совпадений записываем схожести
			for diff,prod_ids in similar_ids.iteritems():
				if diff > min_diff:
					continue
				for prod_id in prod_ids:
					for item_id in group:
						si = SimilarItem()
						si.product_id = prod_id
						si.item_id = item_id
						si.diff = diff
						try:
							si.save()
						except:
							pass

	def create_groups(self):
		count = 0
		processed_ids = set()
		groups = []
		for base_id, base_signs in self.item_signs.iteritems():
			sys.stdout.write('id=%d %d/%d            \r' % (base_id,count,len(self.item_signs)))
			sys.stdout.flush()
			count += 1
		
			if base_id in processed_ids:
				continue
			if not len(base_signs['packing']):
				continue
			
			# подготавливаем условия выборки
			ns = [models.Q(**{'index_name__icontains': n}) for n in base_signs['name']]
			ns.append(models.Q(**{'index_brand': base_signs['brand']}))
			cond_similar = reduce(operator.or_, ns)
			cond_equal = reduce(operator.and_, ns)

			# формируем группу ProductItem
			sample_ids = ProductItem.objects.values_list('id', flat=True)\
				.filter(cond_equal, product_id__isnull=True)
			group = self.find_equal_item_ids(sample_ids, base_id)
			shop_ids = set([self.items[id].shop_id for id in group])

			# ищем точный Product
			sample_ids = Product.objects.values_list('id', flat=True)\
				.filter(cond_equal)
			product_id = self.find_equal_product_id(sample_ids, base_id)
			if product_id:
				self.counts['exist'] += 1
				
				self.save_group_products(group, self.products[product_id], ProductItem.IDENTICAL)
				processed_ids.update(group)
				
				print '%d/%d группа из %d, товар [%s]: найден' % \
					(len(processed_ids), len(self.items), len(group), product_id)
				continue

			# если группа достаточно большая то можно создать новый продукт
			if len(group) >= self.item_threshold or len(shop_ids) >= self.shop_threshold:
				# cоздаем по основному товару
				product = self.create_prod(base_id, group)
				if product:
					self.counts['new'] += 1
						
					product.save()
					self.save_group_products(group, product, ProductItem.BASIC)
					processed_ids.update(group)
					
					self.products[product.id] = product
					self.product_signs[product.id] = self.indexer.get_signs(**product.get_index())

					print '%d/%d группа из %d, товар [%s]: создан %s' % \
						(len(processed_ids), len(self.items), len(group), product.id, product.name.encode('utf8'))
					continue

			groups.append({
				'cond': cond_similar,
				'ids': group,
				'base_id': base_id,
			})
			
		return groups

	def save_group_products(self, group, product, reason):
		product = product.original_product or product
		for id in group:
			self.items[id].product_reason = reason
			self.items[id].product = product
			self.items[id].save()

	def find_equal_item_ids(self, sample_ids, base_id):
		base_signs = self.item_signs[base_id]

		# ищем четкое соответствие
		equal_ids = set([base_id])
		for id in sample_ids:
			# если добавились новые товары с новыми id
			if id not in self.item_signs: 
				continue 
			if not self.grouper.in_group(base_signs, self.item_signs[id]):
				continue 
			equal_ids.add(id)

		return equal_ids

	def find_equal_product_id(self, sample_ids, base_id):
		base_signs = self.item_signs[base_id]
		
		# ищем четкое соответствие
		for id in sample_ids:
			# если добавились новые товары с новыми id
			if id not in self.product_signs: 
				continue 
			if self.grouper.in_group(self.product_signs[id], base_signs):
				return id

		return None

	def create_prod(self, base_id, group):
		base_item = self.items[base_id]
		base_signs = self.item_signs[base_id]
		
		try:
			brand = Brand.objects.get(name=base_signs['brand'])
		except:
			return None

		product = Product()
		product.brand = brand

		name = self.identify_name([self.items[id].name_norm for id in group]) or base_item.name

		product.name 				= name 
		product.image 				= base_item.image
		product.index_name 			= base_item.index_name
		product.index_name_digit 	= base_item.index_name_digit
		product.packing 			= base_item.index_packing
		product.index_packing 		= base_item.index_packing
		product.index_brand 		= base_item.index_brand
		
		return product				

	def identify_name(self, names):
		percent = 0.9
		
		words = set([n.lower() for n in names])
		
		# подсчет частоты слов
		variants = {}
		for ws in words:
			for w in ws.split(' '):
				if w not in variants:
					variants[w] = 0
		for ws in words:
			for w in variants:
				if w in ws:
					variants[w] += 1
		
		# определение самых частых
		from math import trunc
		variants = sorted(variants.items(), key=operator.itemgetter(1), reverse=True)
		border = trunc(len(words) * percent)
		patterns = []
		for v,c in variants:
			if c < border:
				break
			patterns.append(re.compile(re.escape(v), flags=re.U|re.I))
			
		neg_pattern = re.compile(
			'[^(%s)]' % '|'.join([p.pattern for p in patterns]), 
			flags=re.U|re.I
		)
		
		output = None
		diff = None
		for name in names:
			f = True
			for p in patterns:
				r = p.findall(name)
				if not r:
					f = False
					break
			if not f:
				continue
			r = neg_pattern.findall(name)
			if diff == None or len(r) < diff:
				output = name
				diff = len(r)
		return output

	def identify_category(self, product, categories):
		name = product.name
		category_names = [item.category for item in ProductItem.objects.filter(product=product)]
		res = {}

		for c in categories:
			for k in c.all_name_keywords():
				if k in name.lower():
					if c.id not in res:
						res[c.id] = 0 
					res[c.id] += 1
		
		for category_name in category_names:
			for c in categories:
				for k in c.all_keywords():
					if k in category_name.lower():
						if c.id not in res:
							res[c.id] = 0 
						res[c.id] += 1

		if len(res): 
			id = sorted(res.items(), key=operator.itemgetter(1), reverse=True)[0][0]
			category = self.identify_category(product, self.categories[id].children.all())
			if category is None:
				return self.categories[id]
			return category
		return None