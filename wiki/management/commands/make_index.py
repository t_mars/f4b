#coding=utf8
import sys
from optparse import make_option

from django.core.management.base import BaseCommand, CommandError

from lib.prodmatch import Selector, Indexer
from wiki.models import ProductItem, Product, Brand

class Command(BaseCommand):
	option_list = BaseCommand.option_list + (
		# конкретный товар
		make_option(
			"-p", 
			"--pid", 
			dest = "pid",
			metavar = "INT",
		),
	)

	def print_item(self, item):
		print 'product:'
		print 'name:', item.name.encode('utf8')
		print 'brand:', item.brand.encode('utf8')
		print 'packing:', item.packing.encode('utf8')
		print

	def print_index(self, index):
		print 'index:'
		for k in index:
			try:
				print k,':', index[k]
			except:
				print k,':', index[k].encode('utf8')
		print 

	def handle(self, *args, **options):
		indexer = Indexer()
		
		# выбираем объекты индексирования
		id = options.get('pid', None)
		
		# конкретный товар
		if id:
			item = ProductItem.objects.get(id=int(id))
			if not item:
				print 'error'
			self.print_item(item)
			
			index = indexer.get_index(item.name, item.brand, item.packing)
			self.print_index(index)
			
			signs = indexer.get_signs(**index)
			self.print_index(signs)

		else:
			# собираем данные
			datas = {}
			items = {}
			for item in ProductItem.objects.all():
				items[item.id] = item
				datas[item.id] = (item.name, item.brand, item.packing)
			
			# индексируем
			indexer.clear()
			indexes = indexer.make_index(datas,status=True)
			indexer.save()		

			# сохраняем
			errors = {}
			ind = 0
			for id,index in indexes.iteritems():
				sys.stdout.write('saving: id=%d %d/%d            \r' % (id,ind,len(indexes)))
				sys.stdout.flush()

				ind += 1

				item = items[id]
				item.name_norm 			= index['name_norm']
				item.index_name 			= index['name']
				item.index_name_digit	= index['name_digit']
				item.index_brand 		= index['brand']
				item.index_packing 		= index['packing']
				
				try:
					item.save()
				except Exception as e:
					errors[id] = str(e)

			# выводим ошибки 
			print len(errors),'errors'
			for id,mes in errors.iteritems():
				print 'error',mes
				print 'item',id
				self.print_item(items[id])
				self.print_index(indexes[id])
