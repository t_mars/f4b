#coding=utf8
import pickle, sys, codecs, time
from optparse import make_option

from django.core.management.base import BaseCommand, CommandError
from wiki.models import Shop
from prodmatch import Grouper

class Command(BaseCommand):
	def handle(self, *args, **options):
		import json

		f = open('brand.sql')
		lines = f.readlines()
		for ind,line in enumerate(lines):
			name,url,_,_,config_str,prod_url,_,_,_= line[1:-1].split("','")
			
			config_str = config_str.replace('\\r\\n', '')
			config_str = config_str.replace('\\', '')
			config = json.loads(config_str)
			
			shop = Shop()
			
			shop.parser_name = json.dumps(config['selectors'].get('name'))[1:-1].replace('", "', '",\r\n"')
			shop.parser_price = json.dumps(config['selectors'].get('price'))[1:-1].replace('", "', '",\r\n"')
			shop.parser_image = json.dumps(config['selectors'].get('image'))[1:-1].replace('", "', '",\r\n"')
			shop.parser_available = json.dumps(config['selectors'].get('available'))[1:-1].replace('", "', '",\r\n"')
			shop.parser_category = json.dumps(config['selectors'].get('category'))[1:-1].replace('", "', '",\r\n"')
			shop.parser_brand = json.dumps(config['selectors'].get('brand'))[1:-1].replace('", "', '",\r\n"')
			shop.parser_packing = json.dumps(config['selectors'].get('packing'))[1:-1].replace('", "', '",\r\n"')
			
			del config['selectors']
			if 'host' in config:
				del config['host']
			shop.parser_config = json.dumps(config)[1:-1]
			
			shop.name = name
			shop.site = url
			shop.test_url = prod_url
			shop.test_url = prod_url
			try:
				shop.save()
				print shop.id
			except:
				pass