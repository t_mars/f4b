#coding=utf8
import sys, re
from optparse import make_option

from django.core.management.base import BaseCommand, CommandError
from django.db import connection

from lib.prodmatch import Indexer
from wiki.models import ProductItem, Brand

# мусор которые попадает в бренд
brand_trashes = [
	u'энергети.+',
	u'что такое.+',
	u'силовые.+',
	u'скамьи.+',
	u'спортивн.+',
	u'спортпит.+',
	u'стойки.+',
	u'аксессуар.+',
	u'аминокислот.+',
	u'анаболическ.+',
	u'суперсет',
	u'беговые.+',
	u'бинт.+',
	u'для.+',
	u'эллипт.+',
	u'эспандер.+',
	u'электр.+',
	u'гейнер.+',
	u'боксер.+',
	u'гантели.+',
	u'витамин.+',
	u'гантели.+',
	u'борц.+',
	u'главная.*',
	u'росси.*',
	u'title',
	u'012',
	u'журнал.*',
]

# поиск новых брендов
class Command(BaseCommand):

	def handle(self, *args, **options):
		indexer = Indexer()

		trashes = []
		for s in brand_trashes:
			trashes.append(re.compile(ur'^(%s)$' % indexer.trans_chars(s), flags=re.I|re.U))

		cursor = connection.cursor()
		brand_names = cursor.execute(
			'SELECT t.brand, t.id, COUNT(t.id), COUNT(DISTINCT t.shop_id)'
			'FROM wiki_productitem t '
			'WHERE t.brand <> "" AND '
			't.index_brand = "" '
			'GROUP BY t.brand'
		)
		rs = cursor.fetchall()
		rows = []
		cc1 = 0
		cc2 = 0
		for r in rs:
			cc1 += int(r[2])
			bn = indexer.trans_chars(r[0])

			is_exact = False
			# если в бренд попал мусор-то бренд пуст(что значит может быть любой)
			for p in trashes:
				if p.findall(bn): 
					is_exact = True
					break
			
			if not is_exact:
				is_exact, _ = indexer.find_brand(bn)
			
			if is_exact:
				continue
						
			cc2 += int(r[2])
			rows.append(r)

		rows.sort(key = lambda x: int(x[2]))
		print 'id, prods, shops, name'
		ind = 1
		for row in rows:
			print '%d:\t%d\t%d\t%d\t|%s|' % (ind, row[1], row[2], row[3], row[0].encode('utf8'))
			ind += 1
		print 'id, prods, shops, name'
		print cc1,'/',len(rs),',',cc2,'/',len(rows)
		