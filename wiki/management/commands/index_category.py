#coding=utf8
import pickle

from django.core.management.base import BaseCommand, CommandError

from wiki.models import *
class Command(BaseCommand):

	def handle(self, *args, **options):
		categories = Category.objects.all()
		for category in categories:
			products = Product.objects.filter(category=category)
			packing_variants = {}
			for product in products:
				for mes,val in product.packings.iteritems():
					if mes not in packing_variants:
						packing_variants[mes] = [val, val]
					else:
						if packing_variants[mes][0] > val:
							packing_variants[mes][0] = val
						if packing_variants[mes][1] < val:
							packing_variants[mes][1] = val
			category.packing_variants = packing_variants
			category.save()