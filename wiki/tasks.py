from wiki.models import AsyncTask, Shop

class AsyncTask():
	def run():
		logging.basicConfig(level=logging.INFO&logging.DEBUG)
		exception_count = 0
		stop = False
		current_task_id = None
		while True:
			try:
				with transaction.commit_on_success():
					tasks = AsyncTask.objects.raw(
						'SELECT * FROM wiki_asynctask at'
						' WHERE at.status = 0'
						' ORDER BY at.id LIMIT 1'
					)
					if len(list(tasks)):
						task = tasks[0]
						logging.info("Running %s" % task.script)
						current_task_id = task.id
						exec(task.script)
						exception_count = 0
						# task.executed = True
						task.save()
					else:
						current_task_id = None
			except:
				logging.exception('Async Crashed')
				exception_count += 1
 
				# we need to mark the failed task as executed in another DB transaction.
				if current_task_id:
					with transaction.commit_on_success():
						tasks = AsyncTask.objects.raw(
							'SELECT * FROM wiki_asynctask at'
							' WHERE at.id = %s'
							' ORDER BY at.id FOR UPDATE', [current_task_id]
						)
						if len(list(tasks)):
							task = tasks[0]
							# task.executed = True
							task.save()
 
			if exception_count >= self.EXCEPTION_THRESHOLD:
				sleep(10)
			else:
				sleep(0.5)
		

	def parse_shop(id, walk):
		from wiki.managment.commands import parse_shop
		c = parse_shop.Command()
		c.init(False)
		c.run([Shop.objects.get(pk=id)], walk=walk)