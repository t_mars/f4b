#coding=utf8

import pickle, sys, os, operator

from django.contrib import admin
from django.contrib.admin.models import LogEntry
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin import SimpleListFilter
from django.shortcuts import render, render_to_response
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.db.models import Count, Max, Min
from django.db import transaction, connection
from wiki.models import *
from wiki.log_entry import *

from lib.django_object_actions import DjangoObjectActions

from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import force_unicode

class MyAdmin(admin.ModelAdmin):
	def response_change(self, request, obj):
		"""
		Determines the HttpResponse for the change_view stage.
		"""
		if request.POST.has_key("_viewnext"):
			msg = (_('The %(name)s "%(obj)s" was changed successfully.') %
				   {'name': force_unicode(obj._meta.verbose_name),
					'obj': force_unicode(obj)})
			next = obj.__class__.objects.filter(id__gt=obj.id).order_by('id')[:1]
			if next:
				self.message_user(request, msg)
				return HttpResponseRedirect("../%s/" % next[0].pk)
		return super(MyAdmin, self).response_change(request, obj)

def parse_prod(shop, url):
	help_text = ''
	help_text += '<span style="font-size:12pt;color:black;">'
	help_text += u'<a target="_blank" href="%s">Перейти</a><br>' % url
			
	try:
		# настраиваем парсер
		infos = shop.parser.find(url)
		ind = 0
		if not len(infos):
			help_text += 'none'
		for info in infos:
			help_text += ('info %d<br>' % ind)
			ind += 1
			for k in info.keys():
				help_text += ('%s:%s<br>' % (k, info[k]))
			help_text += ('<img src="%s" style="max-width:100px;max-height:100px"><br>' % (info['image']))
			help_text += ('---<br>')
	except Exception as e:
		help_text += 'Parser error: ' + str(e).decode('utf8')
	help_text += '</span>'
	
	return help_text

####################
## Shop object
####################
class ShopForm(forms.ModelForm):

	class Meta:
		model = Shop
		fields = ['name','site','image','description','contact_info','delivery_info',
			'verified','active','test_urls',
			'parser_config','parser_name','parser_price','parser_image',
			'parser_available','parser_brand','parser_packing','parser_category', 'parser_description'
		]

	def __init__(self, *args, **kwargs):
		super(ShopForm, self).__init__(*args, **kwargs)
		obj = kwargs.get('instance')
		if obj:
			# парсим тестовый товар
			if len(obj.test_urls):
				for url in obj.test_urls.split('\r\n'):
					try:
						infos = obj.parser.find(url)
					except Exception as e:
						self.fields['test_urls'].help_text += str(e).decode('utf8')+'<br>'
						infos = []
					for info in infos:
						for key,val in info.items():
							help_text = '<span style="font-size:12pt;color:black;">'
							if key == 'image':
								help_text += '<img src="%s" style="max-height:150px;"><br><a href="%s" target="_blank">%s</a>' % (val, val, val)
							else:
								help_text += '%s' % val
							help_text += '</span><br>---<br>'
							self.fields['parser_'+key].help_text += help_text
					self.fields['test_urls'].help_text += '<a target="_blank" href="%s">link</a>' % url
		else:
			self.fields['parser_name'].initial = \
""""path": ".//h1",
"type": "text"
"""	
			self.fields['parser_price'].initial = \
""""path": ".//div[contains(@class,'price')]",
"type": "text"
"""	
			self.fields['parser_image'].initial = \
""""path": ".//a",
"type": "attr@href"
"""	
			self.fields['parser_available'].initial = \
""""path": ".//div",
"type": "exist"
"""	
			self.fields['parser_category'].initial = \
""""path": ".//div//a[last()]",
"type": "text"
"""	
			self.fields['parser_brand'].initial = \
""""path": ".//div//a",
"type": "text"
"""	
			self.fields['parser_packing'].initial = \
""""path": ".//div",
"type": "text"
"""
			self.fields['parser_description'].initial = \
""""path": ".//div",
"type": "text"
"""

class ShopAdmin(admin.ModelAdmin):
	form = ShopForm
	list_display = ['name', 'active', 'verified', 'scan_status', 'show_prod_count', 
		'created', 'updated', 'last_log_entry', 'last_log_entry_date', 'views']
	actions = ['update', 'scan']

	def scan(self, request, queryset):
		for shop in queryset:
			task = AsyncTask.delay().parse_shop(shop.id, True, comment=u'Сканировать %s' % shop.name)
	scan.short_description = u"Сканировать"
	
	def update(self, request, queryset):
		for shop in queryset:
			task = AsyncTask.delay().parse_shop(shop.id, False, comment=u'Обновить %s' % shop.name)
	update.short_description = u"Обновить"
	
	def queryset(self, request):
		return Shop.objects.annotate(prod_count=Count('productitem'))

	def show_prod_count(self, obj):
		return obj.prod_count
	show_prod_count.admin_order_field = 'prod_count'

	def last_log_entry(self, obj):
		if not hasattr(self, 'logs'):
			self.logs = LogEntry.objects.filter(
				content_type_id	= ContentType.objects.get_for_model(obj).pk,
				action_flag__gt = 3
			)
		logs = self.logs.filter(object_id=obj.pk).order_by('-action_time')
		if len(logs):
			return u'%s' % (logs[0].change_message) 
		return '-'

	def last_log_entry_date(self, obj):
		logs = self.logs.filter(object_id=obj.pk).order_by('-action_time')
		if len(logs):
			return u'%s' % (logs[0].action_time.strftime('%m.%d.%Y %H:%M')) 
		return '-'
		
	
admin.site.register(Shop, ShopAdmin)

class SimilarsField(forms.MultipleChoiceField):
	def __init__(self, name, *args, **kwargs):
		kwargs['required'] = False
		self.name = name
		kwargs['widget'] = forms.CheckboxSelectMultiple
		super(SimilarsField, self).__init__(args, kwargs)
	
	def validate(self, value):
		pass

	def set_objects(self, obj, similars):
		from django.template.loader import render_to_string
		choices = [(-1, 'no')]
		items = [obj.get_data()]
		for id,(diff,s) in similars:
			d = s.get_data()
			d['diff'] = diff
			d['id'] = s.id
			items.append(d)
			choices.append(('%d_y' % id, s.name))
			choices.append(('%d_n' % id, s.name))
		self.help_text = render_to_string('admin/wiki/similars.html', {'items': items, 'name': self.name})
		self.choices = choices


####################
## ProductItem object
####################
class ProductItemForm(forms.ModelForm):
	# product_name = forms.CharField(label=u'Название продукта', required=False)
	# product_brand_s = forms.ChoiceField(label=u'Бренд', required=False)
	# product_brand = forms.CharField(label=u'Новый бренд', required=False)
	
	# product_category_s = forms.ChoiceField(label=u'Категория', required=False)
	# product_category = forms.CharField(label=u'Новая категория', required=False)
	# similars = SimilarsField(label=u'Схожие товары')
	
	class Meta:
		model = ProductItem
		fields = ['product','shop',
			'name','brand','packing','category',
			'index_name','index_name_digit', 'index_brand','index_packing',
			'price','available','image','description','url']

	def __init__(self, *args, **kwargs):
		super(ProductItemForm, self).__init__(*args, **kwargs)
		
		if 'instance' in kwargs and kwargs['instance']:
			obj = kwargs['instance']
			self.fields['image'].help_text = '<a target="_blank" href="%s"><img src="%s" style="max-width:200px; max-height: 200px;"></a>' % (obj.image, obj.image)
			self.fields['url'].help_text = parse_prod(obj.shop, obj.url)

	def save(self, commit=True):
		instance = super(ProductItemForm, self).save(commit=False)
		instance.verified = True
		
		if commit:
			# указываем что продукт установлен вручную
			if self.cleaned_data['product']:
				instance.product_reason = ProductItem.ADDED_MANUALLY
			instance.save()
		
		return instance
	
class HasProductListFilter(SimpleListFilter):
	parameter_name = 'has_product'
	title = 'Наличие продукта'
	
	def lookups(self, query, model_admin):
		return [('1', 'Есть продукт'), ('0', 'Нет продукта')]

	def queryset(self, request, queryset):
		if self.value() == '1':
			return queryset.filter(product__isnull=False)
		if self.value() == '0':
			return queryset.filter(product__isnull=True)

class ProductItemAdmin(admin.ModelAdmin):

	form = ProductItemForm
	ordering = ['verified', 'name']
	list_filter = ['actual', 'verified', 'product__category', 'shop', HasProductListFilter]
	list_display = ['image_tag','index_name','index_name_digit', 'index_brand','index_packing', 
		'name','name_norm',
		'product', 'product_reason_text', 
		'shop', 'verified', 'created', 'updated', 'actual', #'last_log_entry',
		'available', 'price', 
		#'min_price', 'max_price',
		'show_jump_count'
	]
	raw_id_fields = ('product',)
	related_lookup_fields = {
		'fk': ['product'],
	}

	def queryset(self, request):
		return ProductItem.objects.annotate(jump_count=Count('productitemjump'))

	def show_jump_count(self, obj):
		return obj.jump_count
	show_jump_count.admin_order_field = 'jump_count'

	def save_model(self, request, obj, form, change):
		old_product_id = obj.product_id
		super(ProductItemAdmin, self).save_model(request, obj, form, change)
		if obj.product:
			obj.product.reset_prices()
		if old_product_id:
			Product.objects.get(pk=old_product_id).reset_prices()

	# def last_log_entry(self, obj):
	# 	if not hasattr(self, 'logs'):
	# 		self.logs = LogEntry.objects.filter(
	# 			content_type_id	= ContentType.objects.get_for_model(obj).pk,
	# 			action_flag__gt = 3
	# 		)
	# 	logs = LogEntry.objects.filter(
	# 			content_type_id	= ContentType.objects.get_for_model(obj).pk,
	# 			action_flag__gt = 3
	# 		).filter(object_id=obj.pk).order_by('-action_time')
		
	# 	if len(logs):
	# 		return u'%s' % (logs[0].change_message)
	# 	return '-'

	def product_reason_text(self, obj):
		if obj.product_reason is None:
			return '-'
		if obj.product_reason == ProductItem.ADDED_MANUALLY:
			return 'вручную'
		if obj.product_reason == ProductItem.ADDED_ON_CUE:
			return 'по подсказке'
		if obj.product_reason == ProductItem.IDENTICAL:
			return 'идентичный'
		if obj.product_reason == ProductItem.BASIC:
			return 'основа'
		if obj.product_reason == ProductItem.EQUAL:
			return 'схожий d=0'
		return 'единственный d=%d' % obj.product_reason
	product_reason_text.short_description = 'Причина добавления'
	product_reason_text.admin_order_field = 'product_reason'
	
	def image_tag(self, obj):
		return u'<img src="%s" style="max-width:100px;max-height:100px">' % obj.image
	image_tag.short_description = 'Image'
	image_tag.allow_tags = True
	
	# def queryset(self, request):
	# 	return ProductItem.objects.annotate(min_price=Min('shopproductlog__price'), max_price=Max('shopproductlog__price'))

	# def min_price(self, obj):
	# 	return obj.min_price

	# def max_price(self, obj):
	# 	return obj.max_price

admin.site.register(ProductItem, ProductItemAdmin)

####################
## Brand object
####################
class BrandForm(forms.ModelForm):
	class Meta:
		model = Brand
		fields = ['name', 'name_keywords', 'site', 'image', 'description', 'verified']
		
		
	def __init__(self, *args, **kwargs):
		super(BrandForm, self).__init__(*args, **kwargs)
		
		if 'instance' in kwargs and kwargs['instance']:
			obj = kwargs['instance']
			if len(obj.site):
				self.fields['site'].help_text = u'<a href="%s">Ссылка</a>' % (obj.site)
			self.fields['image'].help_text = '<a target="_blank" href="%s"><img src="%s" style="max-width:200px; max-height: 200px;"></a>' % \
				(obj.image, obj.image)
			self.fields['name'].help_text = '<a href="javascript:window.open(\'../../product/?_popup=1&brand__id__exact='+\
				str(obj.id)+'\', \'Продукты\', \'height=500,width=800,resizable=yes,scrollbars=yes\')" target="_blank">В магазинах</a>'
			

class MergeBrandsForm(forms.Form):
	_selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
	new_name = forms.CharField(label=u'Новое название')

class BrandAdmin(admin.ModelAdmin):
	ordering = ['verified', 'name']
	list_display = ['name', 'verified']
	form = BrandForm
	actions = ['merge_brands']

	def merge_brands(self, request, queryset):
		if 'apply' in request.POST.keys():
			form = MergeBrandsForm(request.POST)
			if form.is_valid():
				new_name = form.cleaned_data['new_name']

				base_brand = queryset[0]
				base_brand.name = new_name
				base_brand.save()

				for brand in queryset[1:]:
					for product in Product.objects.filter(brand=brand):
						product.brand = base_brand
						product.save()
					brand.delete()

				self.message_user(request, u"%d объединены как %s." % (len(queryset), new_name))
		else:
			form = MergeBrandsForm(initial={'_selected_action': request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})
			return render(request, 'admin/wiki/brand/merge_brands.html', 
				{'brands': queryset,'form': form, 'title':u'Объединить категории'})

			
	merge_brands.short_description = u"Объединить категории"

admin.site.register(Brand, BrandAdmin)

####################
## Product object
####################
class ProductForm(forms.ModelForm):
	sim_items = SimilarsField(name='sim_items')
	sim_products = SimilarsField(name='sim_products')
	class Meta:
		model = Product
		fields = ['original_product', 'name','image','brand','category',
			'substance','description','ingredients','packing',
			'index_name','index_name_digit','index_packing','index_brand']
		
	def __init__(self, *args, **kwargs):
		super(ProductForm, self).__init__(*args, **kwargs)
		
		if 'instance' in kwargs:
			obj = kwargs['instance']
			
			self.fields['image'].help_text = '<a target="_blank" href="%s"><img src="%s" style="max-width:200px; max-height: 200px;"></a>' % \
				(obj.image, obj.image)
			self.fields['name'].help_text = '<a href="javascript:window.open(\'../../productitem/?_popup=1&product__id__exact='+\
				str(obj.id)+'\', \'Продукты\', \'height=500,width=800,resizable=yes,scrollbars=yes\')" target="_blank">В магазинах</a>'
	
			similar_items = SimilarItem.objects.filter(product_id=obj.id,accepted=None)\
				.order_by('-item__index_brand','diff')
			similars = [(s.item_id,(s.diff,s.item)) for s in similar_items]
			self.fields['sim_items'].set_objects(obj, similars)

			similar_products = SimilarProduct.objects.filter(target_id=obj.id,accepted=None)\
				.order_by('diff')
			similars = [(s.source_id,(s.diff,s.source)) for s in similar_products]
			self.fields['sim_products'].set_objects(obj, similars)

			
	def save(self, commit=True):
		instance = super(ProductForm, self).save(commit=False)
		instance.verified = True
		if len(self.cleaned_data['sim_items']):
			for val in self.cleaned_data['sim_items']:
				id,accept = val.split('_')
				si = SimilarItem.objects.get(product_id=instance.id,item_id=id)
				if accept == 'y':
					# отвергаем другие связи
					with transaction.atomic():
						SimilarItem.objects.filter(item_id=id).update(accepted=False)
					si.accepted = True
					si.save()
					
					item = si.item
					item.verified = True
					item.product = instance
					item.product_reason = ProductItem.ADDED_ON_CUE
					item.save()
				elif accept == 'n':
					si.accepted = False
					si.save()

		if len(self.cleaned_data['sim_products']):
			for val in self.cleaned_data['sim_products']:
				id,accept = val.split('_')
				sp = SimilarProduct.objects.get(target_id=instance.id,source_id=id)
				if accept == 'y':
					with transaction.atomic():
						SimilarProduct.objects.filter(source_id=id).update(accepted=False)
					sp.accepted = True
					sp.save()
					p = sp.source
					
					# перебрасываем все товары
					with transaction.atomic():
						ProductItem.objects\
							.filter(product=p)\
							.update(product=instance)
					p.reset_prices()

					# перебрасываем схожести
					items = SimilarItem.objects.filter(product=p)
					for item in items:
						item.product = instance
						try:
							with transaction.atomic():
								item.save()
						except:
							pass
					with transaction.atomic():
						p.delete()

				elif accept == 'n':
					sp.accepted = False
					sp.save()
		
		# слияние с продуктом с конкретным id
		if self.cleaned_data['original_product']:
			try:
				# перебрасываем все товары
				original_product = self.cleaned_data['original_product']
				instance.original_product = original_product
				
				ProductItem.objects\
					.filter(product=instance)\
					.update(product=original_product)
				original_product.reset_prices()

			except:
				pass


		instance.reset_prices()
		if commit:
			instance.save()
		
		return instance

class ProductAdmin(MyAdmin):
	ordering = ['verified', 'name']
	list_filter = ['brand'] 
	list_display = ['image_tag', 'name', 'packing', 'brand', 'category', 
		'show_prod_count', 'profit_price', 'show_similar_items_count', 'show_similar_products_count',
		'verified', 'error_code_text', 'has_ingredients', 'has_description', 'views']
	
	raw_id_fields = ('original_product',)
	related_lookup_fields = {
		'fk': ['original_product'],
	}
	
	form = ProductForm

	def queryset(self, request):
		qset = Product.objects\
			.annotate(prod_count=Count('productitem', distinct = True)) \
			.annotate(similar_items_count=Count('similar_items', distinct = True)) \
			.annotate(similar_products_count=Count('similar_products', distinct = True))
		return qset
	
	def show_prod_count(self, obj):
		return '<a href="javascript:window.open(\'../productitem/?_popup=1&product__id__exact='+\
			str(obj.id)+'\', \'Продукты\', \'height=500,width=800,resizable=yes,scrollbars=yes\')" target="_blank">Товаров '+str(obj.prod_count)+'</a>'

	show_prod_count.admin_order_field = 'prod_count'
	show_prod_count.allow_tags = True

	def show_similar_items_count(self, obj):
		return obj.similar_items_count
	show_similar_items_count.admin_order_field = 'similar_items_count'

	def show_similar_products_count(self, obj):
		return obj.similar_products_count
	show_similar_products_count.admin_order_field = 'similar_products_count'


	def image_tag(self, obj):
		return u'<img src="%s" style="max-width:100px;max-height:100px">' % obj.image
	image_tag.short_description = 'Image'
	image_tag.allow_tags = True

	def error_code_text(self, obj):
		if obj.error_code == Product.ERROR_NO:
			return 'нет ошибок'
		if obj.error_code == Product.ERROR_PRICE:
			return 'ошибка цены'
		if obj.error_code == Product.ERROR_PACKING:
			return 'ошибка упаковки'
	error_code_text.short_description = 'Ошибка в товаре'
	error_code_text.admin_order_field = 'error_code'

	def has_ingredients(self, obj):
		return obj.ingredients is not None and len(obj.ingredients) > 0
	has_ingredients.short_description = 'Ингридиенты'
	has_ingredients.boolean = True
	has_ingredients.admin_order_field = 'ingredients'

	def has_description(self, obj):
		return obj.description is not None and len(obj.description) > 0
	has_description.short_description = 'Описание'
	has_description.boolean = True
	has_description.admin_order_field = 'description'

admin.site.register(Product, ProductAdmin)

####################
## Substance object
####################
admin.site.register(Substance)

####################
## Category object
####################
class CategoryForm(forms.ModelForm):
	class Meta:
		model = Category
		fields = ['name', 'parent', 'name_keywords', 'keywords']
		
	def save(self, commit=True):
		instance = super(CategoryForm, self).save(commit=False)
		instance.verified = True
			
		if commit:
			instance.save()
		
		return instance
	
	def __init__(self, *args, **kwargs):
		super(CategoryForm, self).__init__(*args, **kwargs)
		
		if 'instance' in kwargs and kwargs['instance']:
			obj = kwargs['instance']
			self.fields['name'].help_text = '<a href="javascript:window.open(\'../../productitem/?_popup=1&product__category__id__exact='+\
				str(obj.id)+'\', \'Продукты\', \'height=500,width=800,resizable=yes,scrollbars=yes\')" target="_blank">В магазинах</a>'
	
			
class CategoryAdmin(admin.ModelAdmin):
	ordering = ['id']
	list_display = ['name', 'id','level', 'verified']
	form = CategoryForm
	
admin.site.register(Category, CategoryAdmin)

####################
## CategoryGroup object
####################
from lib.mptt.admin import MPTTModelAdmin

class CategoryGroupAdmin(MPTTModelAdmin):
	# speficfy pixel amount for this ModelAdmin only:
	mptt_level_indent = 20

admin.site.register(CategoryGroup, CategoryGroupAdmin)

class NotAcceptedListFilter(SimpleListFilter):
	parameter_name = 'accepted'
	title = 'Accepted'
	
	def lookups(self, query, model_admin):
		return [('1', 'yes'), ('0', 'no')]

	def queryset(self, request, queryset):
		if self.value() == '1':
			return queryset.filter(status=AsyncTask.ACCEPTED)
		if self.value() == '0':
			return queryset.exclude(status=AsyncTask.ACCEPTED)

class AsyncTaskAdmin(admin.ModelAdmin):
	fields = ['comment', 'command', 'result', '_status',
		'date_created', 'last_modified']
	readonly_fields = ['comment', 'command', 'result', '_status',
		'date_created', 'last_modified']
	list_display = ['comment', 'command', '_arguments', '_kwarguments', 
		'_status', 'try_count', 'pid', '_active', 'date_created', 'last_modified']
	actions = ['restart', 'kill', 'accept']
	list_filter = [NotAcceptedListFilter]

	def restart(self, request, queryset):
		for task in queryset:
			task.pid = None
			task.try_count = 0
			task.status = AsyncTask.CREATED
			task.save()
	restart.short_description = u"Перезапустить"

	def kill(self, request, queryset):
		for task in queryset:
			task.status = AsyncTask.KILL
			task.save()
	kill.short_description = u"Завершить"

	def accept(self, request, queryset):
		for task in queryset:
			task.status = AsyncTask.ACCEPTED
			task.save()
	accept.short_description = u"Принять"

	def _active(self, obj):
		if not obj.pid:
			return False
		import os.path
		return os.path.exists("/proc/%d" % obj.pid)
	_active.boolean = True

	def _status(self, obj):
		try:
			return ASYNC_STATUS_CHOICE[obj.status]
		except:
			return 'UNKNOW '+obj.status

	def _arguments(self, obj):
		try:
			return str(pickle.loads(obj.arguments))
		except:
			return '-'
			
	def _kwarguments(self, obj):
		try:
			return str(pickle.loads(obj.kwarguments))
		except:
			return '-'

admin.site.register(AsyncTask, AsyncTaskAdmin)