#coding=utf8
import json, re, operator, pickle, os
from unidecode import unidecode

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

from tinymce import models as tinymce_models
from lib.djangosphinx.models import SphinxSearch

def get_slug(name):
	return slugify(unidecode(name))

class MyManager(models.Manager):
	def split_words(self, string):
		if string == '':
			return []
		return re.sub(r'([a-z])([A-Z])', r'\1 \2', string).lower().split(' ')

	def search(self, field, string):
		words = self.split_words(string)
		filt = field+'__icontains'
		return self.filter(
			reduce(operator.or_, (models.Q(**{filt: w}) for w in words)) 
		)

	def order_by_search(self, field, string):
		words = self.split_words(string)
		filt = field+'__icontains'
		counts = {}
		for w in words:
			for p in self.filter(**{filt: w}):
				if p.id not in counts:
					counts[p.id] = 0
				counts[p.id] += 1
		return sorted(self.all(), key=lambda p: counts[p.id] if p.id in counts else 0, reverse=True)

class Brand(models.Model):
	objects = MyManager()
	
	name = models.CharField(max_length=200)
	slug = models.CharField(max_length=200)
	name_keywords = models.CharField(max_length=200)
	site = models.CharField(max_length=200, blank=True)
	image = models.CharField(max_length=200, blank=True)
	description = models.TextField(blank=True)
	verified = models.BooleanField(default=False)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		return unicode(self)

	def __unicode__(self):
		return '#%d %s' % (self.id, self.name)
	
	def save(self, *args, **kwargs):
		self.slug = get_slug(self.name)
		super(Brand, self).save(*args, **kwargs)

from lib.mptt.models import MPTTModel, TreeForeignKey
class Category(MPTTModel):
	objects = MyManager()
	parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
	verified = models.BooleanField(default=False)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	slug = models.CharField(max_length=200)
	name = models.CharField(max_length=200)
	name_keywords = models.CharField(max_length=500, null=True, blank=True)
	keywords = models.CharField(max_length=500, null=True, blank=True)
	
	_packing_variants = models.TextField(db_column='packing_variants')

	def __str__(self):
		return unicode(self)

	def __unicode__(self):
		return '#%d %s' % (self.id, self.name)

	def all_name_keywords(self):
		if not hasattr(self, '_all_name_keywords'):
			self._all_name_keywords = set()
			for w in self.name_keywords.split(','):
				self._all_name_keywords.add(w)
			for c in self.children.all():
				self._all_name_keywords.update(c.all_name_keywords()) 
			
		return self._all_name_keywords

	def all_keywords(self):
		if not hasattr(self, '_all_keywords'):
			self._all_keywords = set()
			for w in self.keywords.split(','):
				self._all_keywords.add(w)
			for c in self.children.all():
				self._all_keywords.update(c.all_keywords()) 
			
		return self._all_keywords

	def url(self):
		from django.core.urlresolvers import reverse
		url = reverse('category', args=[self.id,self.slug])
		if self.is_leaf_node():
			return url
		else:
			return '%sother/' % url 

	@property
	def packing_variants(self):
		try:
			return pickle.loads(self._packing_variants)
		except:
			return {}

	@packing_variants.setter
	def packing_variants(self, value):
		self._packing_variants = pickle.dumps(value)

	def save(self, *args, **kwargs):
		self.slug = get_slug(self.name)
		super(Category, self).save(*args, **kwargs)
		
class CategoryGroup(MPTTModel):
	name = models.CharField(max_length=200, unique=True)
	parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
	categories = models.ManyToManyField(Category)

	def __str__(self):
		return unicode(self)

	def __unicode__(self):
		return '#%d %s' % (self.id, self.name)

ERROR_CODE_CHOICE = {
	0: 'ERROR_NO',
	1: 'ERROR_PRICE',
	2: 'ERROR_PACKING',
}
class Product(models.Model):
	search = SphinxSearch(
		index='products',
		weights={
			'index_name': 100,
			'name': 100,
		}
	)
	objects = MyManager()

	slug = models.CharField(max_length=200)
	name = models.CharField(max_length=200)
	image = models.CharField(max_length=200, blank=True)
	brand = models.ForeignKey(Brand, null=True, blank=True)
	category = models.ForeignKey(Category, null=True)
	substance = models.ForeignKey('Substance', blank=True, null=True)
	description = tinymce_models.HTMLField(blank=True,null=True)
	verified = models.BooleanField(default=False)
	packing = models.CharField(max_length=200, blank=True, null=True)
	serving_size = models.CharField(max_length=200, blank=True, null=True)
	ingredients = tinymce_models.HTMLField(blank=True, null=True)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	rating = models.DecimalField(max_digits=2,decimal_places=1,null=True)
	review_count = models.PositiveIntegerField(default=0,null=False,blank=False)
	item_count = models.PositiveIntegerField(default=0,null=False,blank=False)
	available_item_count = models.PositiveIntegerField(default=0,null=False,blank=False)
	views = models.PositiveIntegerField(default=0,null=False,blank=False)
	
	# самая дешевая цена
	best_deal = models.ForeignKey('ProductItem',null=True,related_name='best_deal',on_delete=models.SET_NULL)
	has_offers = models.BooleanField(default=False)

	error_code = models.IntegerField(default=0,choices=ERROR_CODE_CHOICE.items(),null=False,blank=False)

	ERROR_NO = 0
	ERROR_PRICE = 1
	ERROR_PACKING = 2
	
	similar_prods = models.ManyToManyField('self', symmetrical=False, \
		through='SimilarProduct', related_name='similar_products')
	similar_items = models.ManyToManyField('ProductItem', through='SimilarItem',
		 related_name='similar_items')

	profit_price = models.DecimalField(max_digits=10, decimal_places=0, null=True)
	avg_price = models.DecimalField(max_digits=10, decimal_places=0, null=True)
	min_price = models.DecimalField(max_digits=10, decimal_places=0, null=True)
	max_price = models.DecimalField(max_digits=10, decimal_places=0, null=True)
	related_product_ids = models.TextField(blank=True, null=True)
	
	# в случае если товары дублируются
	original_product = models.ForeignKey('self',null=True,blank=True,related_name='duplicate_products')
	
	# для объединения одинаковых товаров в разной упаковке
	primary_product = models.ForeignKey('self',null=True,blank=True,related_name='slave_products')
	is_primary = models.BooleanField(default=False)

	index_name = models.CharField(max_length=200, blank=True, null=True)
	index_name_digit = models.CharField(max_length=200, blank=True, null=True)
	index_brand = models.CharField(max_length=200, blank=True, null=True)
	index_packing = models.CharField(max_length=200, blank=True, null=True)
	
	def get_index(self):
		return {
			'name': 		self.index_name,
			'name_digit': 	self.index_name_digit,
			'brand': 		self.index_brand,
			'packing': 		self.index_packing,
		}

	def reset_cache(self):
		# перебиваем описание товара
		descriptions = ProductItem.objects\
			.filter(product=self)\
			.exclude(description__isnull=True)\
			.exclude(description__exact='')\
			.values_list('description', flat=True)
		if len(descriptions):
			self.description = descriptions[0]
		
		self.reset_rating(False)
		self.reset_prices(False)
		self.reset_related_products(False)
		self.save()

	def reset_related_products(self,commit=True):
		sp = Product.search\
			.filter(category_id=self.category_id)\
			.query(self.index_name)
		ids = []
		for s in sp:
			if s.id != self.id:
				ids.append(s.id)

		self.related_product_ids = ','.join([str(i) for i in ids])
		if commit:
			self.save()

	def reset_rating(self,commit=True):
		from main.models import ProductReview
		rs = ProductReview.objects\
			.filter(product_id=self.id)\
			.values_list('rating', flat=True)
		self.review_count = len(rs)
		if len(rs):
			self.rating = sum(rs) / len(rs)
		else:
			self.rating = 0.0
		if commit:
			self.save()

	def reset_prices(self,commit=True):
		from django.db.models import Q
		import operator
		items = ProductItem.objects.filter(product=self,shop__active=True,actual=True)
		available_items = items.filter(Q(available__isnull=True) | Q(available=True))

		self.item_count = len(items)
		self.available_item_count = len(available_items)
		prices = {item.id:item.price for item in available_items}

		self.max_price = None
		self.min_price = None
		self.avg_price = None 
		self.profit_price = None
		self.best_deal_id = None

		if prices:
			self.best_deal_id = min(prices, key=prices.get)
			self.max_price = max(prices.values())
			self.min_price = min(prices.values())
			self.avg_price = int(round(sum(prices.values()) / len(prices)))
			self.profit_price = self.max_price - self.min_price
		
		self.has_offers = False
		from store.models import ProductOffer
		offers = ProductOffer.objects.filter(product_id=self.id,active=True)
		if len(offers):
			self.has_offers = True
			# offer = offers[0]
			# if offer.price < self.min_price:
			# 	self.min_price = offer.price 

		if commit:
			self.save()

	def __str__(self):
		return unicode(self)
	
	def __unicode__(self):
		try:
			b = unicode(self.brand)
		except:
			b = '-'
		try:
			c = unicode(self.category)
		except:
			c = '-'	
		return "#%d %s (%s) [%s]" % (self.id, self.name, b, c)

	@property
	def packings(self):
		p = {}
		for t in self.packing.split(','):
			val,mes = t.split(' ')
			val = int(val)
			p[mes] = val
		
		return p

	def get_data(self):
		return {
			'image': self.image,
			'name': self.name,
			'brand': self.brand.name,
			'packing': self.packing
		}

	def offers(self):
		from store.models import ProductOffer
		return ProductOffer.objects\
			.filter(product=self,active=True)

	def save(self, *args, **kwargs):
		self.slug = get_slug(self.name)
		super(Product, self).save(*args, **kwargs)

	def group_products(self, include_self=True):
		if self.is_primary:
			primary = self.pk
		else:
			primary = self.primary_product_id
		
		products = []
		if primary:
			products = list(
				Product.objects\
					.filter(primary_product_id=primary)\
					.filter(original_product=None)
			)
		
		if include_self:
			products.append(self)
		
		return products
	
class SimilarProduct(models.Model):
	source = models.ForeignKey(Product, related_name='source')
	target = models.ForeignKey(Product, related_name='target')
	diff = models.PositiveSmallIntegerField(blank=False, null=False)
	accepted = models.NullBooleanField(default=None)

	class Meta:
		unique_together= (('source', 'target'),)

class Shop(models.Model):
	slug = models.CharField(max_length=200)
	name = models.CharField(max_length=200,unique=True)
	site = models.CharField(max_length=200)
	image = models.CharField(max_length=200, blank=True)
	description = models.TextField(blank=True)
	contact_info = tinymce_models.HTMLField(blank=True)
	delivery_info = tinymce_models.HTMLField(blank=True)
	
	parser_config = models.TextField(blank=True)
	parser_name = models.TextField(blank=True)
	parser_price = models.TextField(blank=True)
	parser_image = models.TextField(blank=True)
	parser_available = models.TextField(blank=True)
	parser_category = models.TextField(blank=True)
	parser_brand = models.TextField(blank=True)
	parser_packing = models.TextField(blank=True)
	parser_description = models.TextField(blank=True)

	test_urls = models.TextField()
	verified = models.BooleanField(default=False)
	active = models.BooleanField(default=False)
	# None=not False=scaning True=scanned
	scan_status = models.NullBooleanField(default=True)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	rating = models.DecimalField(max_digits=2,decimal_places=1,null=True)
	review_count = models.PositiveIntegerField(default=0,null=False,blank=False)
	views = models.PositiveIntegerField(default=0,null=False,blank=False)

	def reset_rating(self):
		from main.models import ShopReview
		rs = ShopReview.objects\
			.filter(shop_id=self.id)\
			.values_list('rating', flat=True)
		self.review_count = len(rs)
		if len(rs):
			self.rating = sum(rs) / len(rs)
		else:
			self.rating = 0.0
		self.save()

	def __str__(self):
		return self.name

	def __unicode__(self):
		return self.name

	@property
	def parser(self):
		# парсим конфиг
		try:
			config = json.loads('{%s}' % self.parser_config)
		except Exception as e:
			raise Exception('Shop config parser error: %s' % str(e))

		selectors = {}
		keys = ['name','price','image','available','category','brand','packing', 'description']
		for k in keys:
			try:
				selectors[k] = json.loads('{%s}' % getattr(self, 'parser_'+k))
			except Exception as e:
				raise Exception('Shop config parser error for %s: %s' % (k, str(e)))
	

		dump_path = os.path.join(settings.SHOP_PARSER_TMP, 'shop%d.url' % (self.id))
		from lib.parser import ProductParser
		return ProductParser(host=self.site, 
			selectors=selectors, 
			proxy='proxy' in config, 
			thread_number=int(config.get('thread_number',4)),
			excludes=config.get('excludes', []),
			dump_path=dump_path
		)

	def __init__(self, *args, **kwargs):
		super(Shop, self).__init__(*args, **kwargs)
		self.initial_active = self.active

	def save(self, *args, **kwargs):
		self.slug = get_slug(self.name)
		
		# если изменилась активность то сбрасываем товары
		if self.active != self.initial_active:
			ps = Product.objects.filter(productitem__shop_id=self.id)
			for p in ps:
				p.reset_prices()

		super(Shop, self).save(*args, **kwargs)

class ProductItem(models.Model):
	search = SphinxSearch(
		index='items',
		weights={
			'name_norm': 100,
			'name': 100,
		}
	)

	product = models.ForeignKey(Product,blank=True,null=True,on_delete=models.SET_NULL)
	shop = models.ForeignKey(Shop)
	
	url = models.CharField(max_length=200)
	price = models.DecimalField(max_digits=10, decimal_places=0, null=True)
	name = models.CharField(max_length=200)
	image = models.CharField(max_length=200, blank=True, null=True)
	description = models.TextField(blank=True)

	available = models.NullBooleanField()
	verified = models.BooleanField(default=False)
	actual = models.BooleanField(default=True)
	
	brand = models.CharField(max_length=200, blank=True, null=False)
	category = models.CharField(max_length=200, blank=True, null=False)
	packing = models.CharField(max_length=200, blank=True, null=False)
	
	hash = models.CharField(max_length=32)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now_add=True)

	# для создания Product
	name_norm = models.CharField(max_length=200, blank=True, null=True)

	index_name = models.CharField(max_length=200, blank=True, null=True)
	index_name_digit = models.CharField(max_length=200, blank=True, null=True)
	index_brand = models.CharField(max_length=200, blank=True, null=True)
	index_packing = models.CharField(max_length=200, blank=True, null=True)
	
	# причина прикрепления к продукту
	product_reason = models.IntegerField(null=True)
	
	ADDED_MANUALLY = -4 # добавлен вручную
	ADDED_ON_CUE = -3 # добавлен по подсказке
	IDENTICAL = -2 # найден идентичный продукт
	BASIC = -1 # послужил основой для создания продукта
	EQUAL = 0 # наден схожий продукт с diff=0
	# от 1 - единственный вариант в diff = product_reason

	def get_index(self):
		return {
			'name': 		self.index_name,
			'name_digit': 	self.index_name_digit,
			'brand': 		self.index_brand,
			'packing': 		self.index_packing,
		}

	def get_data(self):
		return {
			'image': self.image,
			'name': self.name_norm,
			'brand': self.index_brand,
			'packing': self.index_packing
		}

	def __str__(self):
		return self.name

	def __unicode__(self):
		return self.name

class ProductItemLog(models.Model):
	item = models.ForeignKey(ProductItem)
	price = models.DecimalField(max_digits=10, decimal_places=0, null=True)
	available = models.NullBooleanField()
	update = models.DateTimeField()

class ProductItemJump(models.Model):
	item = models.ForeignKey(ProductItem)
	created = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User, null=True, blank=True, default = None)
	referer_url = models.CharField(max_length=500)

class SimilarItem(models.Model):
	item = models.ForeignKey(ProductItem)
	product = models.ForeignKey(Product)
	diff = models.PositiveSmallIntegerField(blank=False, null=False)
	accepted = models.NullBooleanField(default=None)

	class Meta:
		unique_together= (('item', 'product'),)

class Substance(models.Model):
	name = models.CharField(max_length=200)
	description = models.TextField(blank=True)

	def __str__(self):
		return self.name

	def __unicode__(self):
		return self.name

ASYNC_STATUS_CHOICE = {
	0: 'CREATED',
	1: 'EXECUTE',
	2: 'SUCCESS',
	3: 'FAILURE',
	4: 'KILL',
	5: 'KILLED',
	6: 'ACCEPTED',
	7: 'CLOSED',
}

class AsyncTask(models.Model):
	comment = models.TextField(null=True)
	command = models.TextField()
	arguments = models.TextField()
	kwarguments = models.TextField()
	result = models.TextField()
	status = models.IntegerField(default=0,choices=ASYNC_STATUS_CHOICE.items(),null=False,blank=False)
	
	pid = models.PositiveSmallIntegerField(blank=True,null=True)
	try_count = models.PositiveSmallIntegerField(default=0,blank=False,null=False)

	date_created = models.DateTimeField(auto_now_add=True)
	last_modified = models.DateTimeField(auto_now=True)

	CREATED = 0
	EXECUTE = 1
	SUCCESS = 2
	FAILURE = 3
	KILL = 4
	KILLED = 5
	ACCEPTED = 6
	CLOSED = 7

	_instance = None
	@staticmethod
	def delay():
		if not AsyncTask._instance:
			AsyncTask._instance = AsyncTask()
		return AsyncTask._instance

	def __getattr__(self, attr):
		if hasattr(AsyncTask, '_'+attr):
			def wrapper(*args, **kwargs):
				task = AsyncTask()
				if 'comment' in kwargs:
					task.comment = kwargs['comment']
					del kwargs['comment']
				task.command = attr
				task.arguments = pickle.dumps(args)
				task.kwarguments = pickle.dumps(kwargs)
				task.save()
				return True
			return wrapper
		raise AttributeError(attr)

	@staticmethod
	def _parse_shop(id, walk):
		from wiki.management.commands import parse_shop
		c = parse_shop.Command()
		c.init(False)
		c.run([Shop.objects.get(pk=id)], walk=walk)