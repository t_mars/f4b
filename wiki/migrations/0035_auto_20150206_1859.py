# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0034_auto_20150206_1853'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shop',
            name='parser_available',
            field=models.CharField(default='{}', max_length=500),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_brand',
            field=models.CharField(default='{}', max_length=500),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_category',
            field=models.CharField(default='{}', max_length=500),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_image',
            field=models.CharField(default='{}', max_length=500),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_name',
            field=models.CharField(default='{}', max_length=500),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_packing',
            field=models.CharField(default='{}', max_length=500),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_price',
            field=models.CharField(default='{}', max_length=500),
            preserve_default=False,
        ),
    ]
