# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0024_remove_shop_config_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='shop',
            name='scan_status',
            field=models.NullBooleanField(default=True),
            preserve_default=True,
        ),
    ]
