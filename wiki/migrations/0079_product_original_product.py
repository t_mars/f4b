# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0078_product_item_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='original_product',
            field=models.ForeignKey(related_name='duplicate_products', to='wiki.Product', null=True),
            preserve_default=True,
        ),
    ]
