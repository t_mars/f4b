# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0036_auto_20150206_1913'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shop',
            name='parser_available',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_brand',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_category',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_image',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_name',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_packing',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shop',
            name='parser_price',
            field=models.TextField(),
            preserve_default=True,
        ),
    ]
