# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0091_product_has_offers'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='primary_product',
            field=models.ForeignKey(related_name='slave_products', blank=True, to='wiki.Product', null=True),
            preserve_default=True,
        ),
    ]
