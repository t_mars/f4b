# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0027_auto_20150119_0911'),
    ]

    operations = [
        migrations.AddField(
            model_name='shophasproduct',
            name='actual',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shophasproduct',
            name='brand_str',
            field=models.CharField(max_length=200, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shophasproduct',
            name='category_str',
            field=models.CharField(max_length=200, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shophasproduct',
            name='packing_str',
            field=models.CharField(max_length=200, blank=True),
            preserve_default=True,
        ),
    ]
