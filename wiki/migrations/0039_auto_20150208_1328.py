# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0038_auto_20150206_2003'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asynctask',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, b'CREATED'), (1, b'EXECUTE'), (2, b'SUCCESS'), (3, b'FAILURE'), (4, b'KILL'), (5, b'KILLED')]),
            preserve_default=True,
        ),
    ]
