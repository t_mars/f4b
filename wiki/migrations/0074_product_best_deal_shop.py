# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0073_auto_20150304_1043'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='best_deal_shop',
            field=models.ForeignKey(to='wiki.Shop', null=True),
            preserve_default=True,
        ),
    ]
