# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0040_auto_20150211_0600'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shophasproduct',
            old_name='brand_str',
            new_name='brand',
        ),
        migrations.RenameField(
            model_name='shophasproduct',
            old_name='category_str',
            new_name='category',
        ),
        migrations.RenameField(
            model_name='shophasproduct',
            old_name='packing_str',
            new_name='packing',
        ),
    ]
