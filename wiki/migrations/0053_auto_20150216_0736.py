# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0052_auto_20150215_1633'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(max_length=200)),
                ('price', models.DecimalField(null=True, max_digits=10, decimal_places=0)),
                ('name', models.CharField(max_length=200)),
                ('image', models.CharField(max_length=200, null=True, blank=True)),
                ('available', models.NullBooleanField()),
                ('verified', models.BooleanField(default=False)),
                ('actual', models.BooleanField(default=True)),
                ('brand', models.CharField(max_length=200, blank=True)),
                ('category', models.CharField(max_length=200, blank=True)),
                ('packing', models.CharField(max_length=200, blank=True)),
                ('hash', models.CharField(max_length=32)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('name_norm', models.CharField(max_length=200, null=True, blank=True)),
                ('index_name', models.CharField(max_length=200, null=True, blank=True)),
                ('index_name_digit', models.CharField(max_length=200, null=True, blank=True)),
                ('index_brand', models.CharField(max_length=200, null=True, blank=True)),
                ('index_packing', models.CharField(max_length=200, null=True, blank=True)),
                ('product', models.ForeignKey(blank=True, to='wiki.Product', null=True)),
                ('shop', models.ForeignKey(to='wiki.Shop')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductItemLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.DecimalField(null=True, max_digits=10, decimal_places=0)),
                ('available', models.NullBooleanField()),
                ('update', models.DateTimeField()),
                ('shop_has_product', models.ForeignKey(to='wiki.ProductItem')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='shophasproduct',
            name='product',
        ),
        migrations.RemoveField(
            model_name='shophasproduct',
            name='shop',
        ),
        migrations.RemoveField(
            model_name='shopproductlog',
            name='shop_has_product',
        ),
        migrations.DeleteModel(
            name='ShopProductLog',
        ),
        migrations.AlterField(
            model_name='product',
            name='similar_items',
            field=models.ManyToManyField(related_name='similar_items', through='wiki.SimilarItem', to='wiki.ProductItem'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='similaritem',
            name='item',
            field=models.ForeignKey(to='wiki.ProductItem'),
            preserve_default=True,
        ),
        migrations.DeleteModel(
            name='ShopHasProduct',
        ),
    ]
