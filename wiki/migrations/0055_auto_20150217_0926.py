# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0054_auto_20150216_0917'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shop',
            name='test_url',
        ),
        migrations.AddField(
            model_name='shop',
            name='test_urls',
            field=models.CharField(default='', max_length=1000, db_column=b'test_url'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shop',
            name='verified',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
