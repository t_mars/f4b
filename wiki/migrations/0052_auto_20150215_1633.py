# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0051_product_similar_prods'),
    ]

    operations = [
        migrations.AddField(
            model_name='similaritem',
            name='accepted',
            field=models.NullBooleanField(default=None),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='similarproduct',
            name='accepted',
            field=models.NullBooleanField(default=None),
            preserve_default=True,
        ),
    ]
