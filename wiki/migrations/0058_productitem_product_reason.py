# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0057_auto_20150218_0618'),
    ]

    operations = [
        migrations.AddField(
            model_name='productitem',
            name='product_reason',
            field=models.CharField(max_length=64, null=True, blank=True),
            preserve_default=True,
        ),
    ]
