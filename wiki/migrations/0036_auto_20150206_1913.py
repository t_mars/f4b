# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0035_auto_20150206_1859'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shop',
            old_name='_parser_config',
            new_name='parser_config',
        ),
    ]
