# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0082_auto_20150330_1113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shop',
            name='contact_info',
            field=tinymce.models.HTMLField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shop',
            name='delivery_info',
            field=tinymce.models.HTMLField(),
            preserve_default=True,
        ),
    ]
