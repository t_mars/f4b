# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0014_auto_20141217_1041'),
    ]

    operations = [
        migrations.AddField(
            model_name='shop',
            name='config_path',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
