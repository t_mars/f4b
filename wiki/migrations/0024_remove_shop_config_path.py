# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0023_shop_test_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shop',
            name='config_path',
        ),
    ]
