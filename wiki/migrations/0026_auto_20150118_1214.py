# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0025_shop_scan_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shophasproduct',
            name='brand_str',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='shophasproduct',
            name='category_str',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='shophasproduct',
            name='packing_str',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
