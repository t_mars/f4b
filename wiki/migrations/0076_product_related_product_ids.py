# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0075_auto_20150304_1059'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='related_product_ids',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
