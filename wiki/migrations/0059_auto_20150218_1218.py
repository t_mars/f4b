# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0058_productitem_product_reason'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productitem',
            name='product_reason',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
    ]
