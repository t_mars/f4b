# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0020_auto_20141225_1357'),
    ]

    operations = [
        migrations.AddField(
            model_name='shophasproduct',
            name='packing_norm',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
    ]
