# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0017_shophasproduct_signs'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shophasproduct',
            name='signs',
        ),
        migrations.AddField(
            model_name='brand',
            name='name_keywords',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shophasproduct',
            name='_signs',
            field=models.CharField(max_length=1000, null=True, db_column=b'signs', blank=True),
            preserve_default=True,
        ),
    ]
