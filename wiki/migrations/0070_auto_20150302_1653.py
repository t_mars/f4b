# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0069_auto_20150302_1110'),
    ]

    operations = [
        migrations.AddField(
            model_name='asynctask',
            name='pid',
            field=models.PositiveSmallIntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='asynctask',
            name='try_count',
            field=models.PositiveSmallIntegerField(default=0),
            preserve_default=True,
        ),
    ]
