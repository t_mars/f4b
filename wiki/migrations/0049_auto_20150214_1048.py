# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0048_product_similar_objects'),
    ]

    operations = [
        migrations.CreateModel(
            name='SimilarItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('diff', models.PositiveSmallIntegerField()),
                ('item', models.ForeignKey(to='wiki.ShopHasProduct')),
                ('product', models.ForeignKey(to='wiki.Product')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SimilarProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('diff', models.PositiveSmallIntegerField()),
                ('source', models.ForeignKey(related_name='source', to='wiki.Product')),
                ('target', models.ForeignKey(related_name='target', to='wiki.Product')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='similarproduct',
            unique_together=set([('source', 'target')]),
        ),
        migrations.AlterUniqueTogether(
            name='similaritem',
            unique_together=set([('item', 'product')]),
        ),
        migrations.RemoveField(
            model_name='product',
            name='similar_objects',
        ),
    ]
