# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0076_product_related_product_ids'),
    ]

    operations = [
        migrations.AddField(
            model_name='shop',
            name='active',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
