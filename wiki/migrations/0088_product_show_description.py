# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0087_auto_20150331_1237'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='show_description',
            field=tinymce.models.HTMLField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
