# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0032_auto_20150202_1041'),
    ]

    operations = [
        migrations.AddField(
            model_name='asynctask',
            name='comment',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='asynctask',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, b'CREATED'), (1, b'EXECUTE'), (2, b'SUCCESS'), (3, b'FAILURE')]),
            preserve_default=True,
        ),
    ]
