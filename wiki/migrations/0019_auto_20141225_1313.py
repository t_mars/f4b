# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0018_auto_20141222_1137'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shophasproduct',
            name='_signs',
            field=models.TextField(null=True, db_column=b'signs', blank=True),
            preserve_default=True,
        ),
    ]
