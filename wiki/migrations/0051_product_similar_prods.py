# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0050_product_similar_items'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='similar_prods',
            field=models.ManyToManyField(related_name='similar_products', through='wiki.SimilarProduct', to='wiki.Product'),
            preserve_default=True,
        ),
    ]
