# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0060_auto_20150219_0831'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='_packing_variants',
            field=models.TextField(default='', db_column=b'packing_variants'),
            preserve_default=False,
        ),
    ]
