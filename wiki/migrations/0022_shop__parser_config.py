# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0021_shophasproduct_packing_norm'),
    ]

    operations = [
        migrations.AddField(
            model_name='shop',
            name='_parser_config',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
