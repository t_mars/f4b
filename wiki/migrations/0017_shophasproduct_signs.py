# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0016_shophasproduct_hash'),
    ]

    operations = [
        migrations.AddField(
            model_name='shophasproduct',
            name='signs',
            field=models.CharField(max_length=1000, null=True, blank=True),
            preserve_default=True,
        ),
    ]
