# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0049_auto_20150214_1048'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='similar_items',
            field=models.ManyToManyField(related_name='similar_items', through='wiki.SimilarItem', to='wiki.ShopHasProduct'),
            preserve_default=True,
        ),
    ]
