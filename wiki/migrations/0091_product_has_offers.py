# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0090_auto_20150331_1304'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='has_offers',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
