# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0039_auto_20150208_1328'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shophasproduct',
            old_name='brand_norm',
            new_name='index_brand',
        ),
        migrations.RenameField(
            model_name='shophasproduct',
            old_name='name_norm',
            new_name='index_name',
        ),
        migrations.RenameField(
            model_name='shophasproduct',
            old_name='packing_norm',
            new_name='index_packing',
        ),
        migrations.RemoveField(
            model_name='shophasproduct',
            name='_signs',
        ),
        migrations.AddField(
            model_name='shophasproduct',
            name='index_name_digit',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
    ]
