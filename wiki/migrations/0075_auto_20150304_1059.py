# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0074_product_best_deal_shop'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='best_deal_shop',
        ),
        migrations.AddField(
            model_name='product',
            name='best_deal',
            field=models.ForeignKey(related_name='best_deal', to='wiki.ProductItem', null=True),
            preserve_default=True,
        ),
    ]
