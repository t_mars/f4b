# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0015_shop_config_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='shophasproduct',
            name='hash',
            field=models.CharField(default='', max_length=32),
            preserve_default=False,
        ),
    ]
