# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0088_product_show_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='show_description',
        ),
    ]
