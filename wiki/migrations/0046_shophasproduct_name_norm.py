# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0045_auto_20150211_0933'),
    ]

    operations = [
        migrations.AddField(
            model_name='shophasproduct',
            name='name_norm',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
    ]
