# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0092_product_primary_product'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='is_primary',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
