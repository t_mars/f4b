# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0089_remove_product_show_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='ingredients',
            field=tinymce.models.HTMLField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
