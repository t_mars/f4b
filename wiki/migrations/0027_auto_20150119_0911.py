# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0026_auto_20150118_1214'),
    ]

    operations = [
        migrations.AddField(
            model_name='brand',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 19, 9, 10, 9, 145242, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='brand',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 19, 9, 10, 19, 658064, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='category',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 19, 9, 10, 26, 953037, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='category',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 19, 9, 10, 33, 529278, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 19, 9, 10, 40, 89612, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 19, 9, 10, 46, 634068, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shop',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 19, 9, 10, 52, 353078, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shop',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 19, 9, 10, 58, 106439, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shophasproduct',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 19, 9, 11, 3, 930285, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shophasproduct',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 19, 9, 11, 13, 643127, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
