# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0068_auto_20150227_2011'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='_avg_price',
        ),
        migrations.RemoveField(
            model_name='product',
            name='_max_price',
        ),
        migrations.RemoveField(
            model_name='product',
            name='_min_price',
        ),
        migrations.RemoveField(
            model_name='product',
            name='_rating',
        ),
        migrations.RemoveField(
            model_name='shop',
            name='_rating',
        ),
        migrations.AddField(
            model_name='product',
            name='available_item_count',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='avg_price',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='max_price',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='min_price',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='profit_price',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='rating',
            field=models.DecimalField(null=True, max_digits=2, decimal_places=1),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='review_count',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='shop',
            name='rating',
            field=models.DecimalField(null=True, max_digits=2, decimal_places=1),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='shop',
            name='review_count',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
