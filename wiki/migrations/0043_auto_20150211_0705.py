# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0042_auto_20150211_0646'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shop',
            name='index_brand',
        ),
        migrations.RemoveField(
            model_name='shop',
            name='index_name',
        ),
        migrations.RemoveField(
            model_name='shop',
            name='index_name_digit',
        ),
        migrations.RemoveField(
            model_name='shop',
            name='index_packing',
        ),
        migrations.AddField(
            model_name='product',
            name='index_brand',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='index_name',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='index_name_digit',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='index_packing',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
    ]
