# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0065_shop__rating'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='error_code',
            field=models.IntegerField(default=0, choices=[(0, b'ERROR_NO'), (1, b'ERROR_PRICE')]),
            preserve_default=True,
        ),
    ]
