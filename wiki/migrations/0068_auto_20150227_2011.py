# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0067_auto_20150226_1350'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='error_code',
            field=models.IntegerField(default=0, choices=[(0, b'ERROR_NO'), (1, b'ERROR_PRICE'), (2, b'ERROR_PACKING')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='productitem',
            name='updated',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
    ]
