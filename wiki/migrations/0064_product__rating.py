# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0063_remove_product_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='_rating',
            field=models.DecimalField(null=True, decimal_places=1, max_digits=2, db_column=b'rating'),
            preserve_default=True,
        ),
    ]
