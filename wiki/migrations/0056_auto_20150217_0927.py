# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0055_auto_20150217_0926'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shop',
            name='test_urls',
            field=models.TextField(),
            preserve_default=True,
        ),
    ]
