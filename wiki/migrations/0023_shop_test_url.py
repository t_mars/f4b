# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0022_shop__parser_config'),
    ]

    operations = [
        migrations.AddField(
            model_name='shop',
            name='test_url',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
