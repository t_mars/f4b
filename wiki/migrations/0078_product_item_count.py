# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0077_shop_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='item_count',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
