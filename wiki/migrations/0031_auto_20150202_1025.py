# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0030_asynctask'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='asynctask',
            name='executed',
        ),
        migrations.RemoveField(
            model_name='asynctask',
            name='script',
        ),
        migrations.AddField(
            model_name='asynctask',
            name='arguments',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='asynctask',
            name='command',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='asynctask',
            name='kwarguments',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='asynctask',
            name='status',
            field=models.CharField(default='', max_length=1, choices=[(0, b'CREATED'), (1, b'EXECUTED'), (2, b'SUCCESS'), (3, b'FAILURE')]),
            preserve_default=False,
        ),
    ]
