# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0047_auto_20150212_1249'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='similar_objects',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
