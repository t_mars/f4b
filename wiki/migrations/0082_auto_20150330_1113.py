# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0081_productitem_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='shop',
            name='contact_info',
            field=models.TextField(default='', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shop',
            name='delivery_info',
            field=models.TextField(default='', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='product',
            name='best_deal',
            field=models.ForeignKey(related_name='best_deal', on_delete=django.db.models.deletion.SET_NULL, to='wiki.ProductItem', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='product',
            name='original_product',
            field=models.ForeignKey(related_name='duplicate_products', blank=True, to='wiki.Product', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='productitem',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='wiki.Product', null=True),
            preserve_default=True,
        ),
    ]
