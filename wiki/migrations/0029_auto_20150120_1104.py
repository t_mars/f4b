# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0028_auto_20150120_0940'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shophasproduct',
            name='price',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shopproductlog',
            name='price',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=0),
            preserve_default=True,
        ),
    ]
