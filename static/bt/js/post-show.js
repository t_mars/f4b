function reply(username) {
  $('#id_comment').val($('#id_comment').val() + '@' + username + ' ');
}
function like_post(id, obj) {
  $.ajax({
    url: "/ajax/post_like/" + id,
  })
  .done(function( data ) {
    data = $.parseJSON(data);
    if (data.status == 'success') {
      $('.post_'+id+'_like_count').html(data.count);
      $(obj).prop('disabled', 'disabled');
    } else {
      $(obj).replaceWith(data.message);
    }
  });
}
function complaint_post(id, obj) {
  $.ajax({
    url: "/ajax/post_complaint/" + id,
  })
  .done(function( data ) {
    data = $.parseJSON(data);
    if (data.status == 'success') {
      $(obj).hide();
    } else {
      $(obj).replaceWith(data.message);
    }
  });
}

$(document).ready(function() {
      
   var recent = $("#owl-recent");
   
  recent.owlCarousel({
    autoPlay: 3000000, //Set AutoPlay to 3 seconds
    items : 4,
    mouseDrag : false,
    pagination : false,
    afterInit: function() {
      var heights = $(".owl-item").map(function () {return $(this).height();}).get(),
        maxHeight = Math.max.apply(null, heights);
      $(".owl-item").height(maxHeight+10);
      $(".owl-wrapper-outer").height(maxHeight+15);
    }
  });
  
  $(".next").click(function(){
      recent.trigger('owl.next');
    })
    
    $(".prev").click(function(){
      recent.trigger('owl.prev');
    })
});