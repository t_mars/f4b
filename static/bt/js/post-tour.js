var tour = MyTour('post');
tour.addSteps([
  {
    element: "#theme_group",
    content: "Прежде всего - выберите тему Вашей статьи или отзыва о продукте спортивного питания",
    placement: 'top',
  },
  {
    element: "#title_group",
    content: "Напишите привлекательный заголовок",
    placement: 'top',
  },
  {
    element: "#description_group",
    content: "Кратко опишите основную мысль статьи",
    placement: 'top',
  },
  {
    element: "#content_group",
    content: "Можно приступать к написанию статьи",
    placement: 'top',
  },
  {
    element: "#content_group",
    content: "Не забывайте про фотографии и видео",
    placement: 'top',
  },
  {
    element: "#score_status",
    content: "Вы сразу сможете увидеть - сколько баллов получите за данную статью",
    placement: 'top',
  },
  {
    element: "#external_link_group_2",
    content: "Если Вы позаимствовали статью с другого сайта - обязательно укажите источник",
    placement: 'top',
  },
  {
    element: "#pub_button",
    content: "Поздравляем! Ваша статья готова - можно ее публиковать!",
    placement: 'right',
  },
])

tour.init();
