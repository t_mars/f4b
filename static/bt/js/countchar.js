function countChar(e) {
  var obj = e.target,
    max = parseInt($(obj).attr('maxlength')),
    min = parseInt($(obj).attr('minlength'));
  var len = obj.value.length;
  if (len > max) {
    $(obj).next('.counter').html('<span style="color:red">'+len+' символов, максимум '+max+' символов</span>');
  } else if (len < min) {
    $(obj).next('.counter').html('<span style="color:red">'+len+' символов, минимум '+min+' символов</span>');
  } else {
    $(obj).next('.counter').html('<span>'+len+' символов, максимум '+max+' символов</span>');
  }
};

$(document).ready(function(){
  $('[data-role="counter"]').after('<p class="counter"></p>').keyup(countChar)
});