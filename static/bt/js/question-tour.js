var tour = MyTour('qa');
tour.addSteps([
  {
    element: "#title_group",
    content: "Кратко задайте вопрос",
    placement: 'top',
  },
  {
    element: "#content_group",
    content: "В поле Вопрос - вы можете дать пояснения по Вашему вопросу",
    placement: 'top',
  },
  {
    element: "#content_group",
    content: "Так же Вы можете попросить конкретного пользователя ответить Вам, используя тег @nickname, где вместо nickname укажите логин пользователя",
    placement: 'top',
  },
  {
    element: "#save_button",
    content: "Ваш вопрос готов и его можно публиковать",
    placement: 'top',
  },
])

tour.init();
