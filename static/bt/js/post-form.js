$(function(){
	// баллы
	$('#id_content_iframe').load(function(){
		var content_block = $(document.getElementById('id_content_iframe').contentWindow.document).find('div.note-editable');
		var status_block = $('#score_status');
		var csrfmiddlewaretoken = $('[name=csrfmiddlewaretoken]').val();
		function refresh_scores() {
			//$('#score_status').html('<i class="fa fa-spinner fa-pulse"></i>');
			$.ajax({
				url: "/ajax/scores_for_post/",
				method: "POST",
				data: {
					content: content_block.html(),
					csrfmiddlewaretoken: csrfmiddlewaretoken,
				},
			})
			.done(function( data ) {
				status_block.css('opacity', '1');
				status_block.html(data);
			});
		}

		refresh_scores();

		var timerID1 = null, timerID2 = null;
		content_block
			.bind('DOMNodeInserted DOMNodeRemoved', function(){
				status_block.css('opacity', '0.6');
				clearTimeout(timerID1);
				timerID1 = setTimeout(refresh_scores, 100)
			})
			.keyup(function(){
				status_block.css('opacity', '0.6');
				clearTimeout(timerID2);
				timerID2 = setTimeout(refresh_scores, 1500)
			})
	});
});