var tour = MyTour('menu');
tour.addSteps([
  {
    element: "#menu_prizes",
    content: "Участвуйте в нашем конкурсе и получайте ценные призы!",
    onShow: dropdownOnShow,
    onShown: dropdownOnShown,
  },
  {
    element: "#menu_rules",
    content: "Ознакомьтесь с правилами",
    onShow: dropdownOnShow,
    onShown: dropdownOnShown,
  },
  {
    element: "#menu_scroing",
    content: "... и таблицей призовых баллов",
    onShow: dropdownOnShow,
    onShown: dropdownOnShown,
  },
  {
    element: "#menu_post",
    content: "Набирать баллы можно несколькими способами: Написанием интересных тематических статей.",
    placement: 'bottom',
  },
  {
    element: "#menu_qa",
    content: "Помощью пользователям в разделе Вопросы и Ответы",
    placement: 'bottom',
  },
  {
    element: "#menu_forum",
    content: "а так же общением на Форуме",
    placement: 'bottom',
  },
  {
    element: "#menu_users",
    content: "Следите за лидерами конкурса",
    onShow: dropdownOnShow,
    onShown: dropdownOnShown,
    placement: 'left',
  },
  {
    element: "#menu_scores",
    content: "и старайтесь набрать больше баллов. Удачи на проекте!",
    onShow: dropdownOnShow,
    onShown: dropdownOnShown,
    placement: 'left',
  }
])

tour.init();
