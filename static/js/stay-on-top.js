$(document).ready(function(){
  postop = $('.stay-on-top').offset().top
  function move_top() {
    var offset = 0;
    if ($(window).outerWidth() >= 975 && $(document).scrollTop() > postop) { 
        offset = ($(document).scrollTop() - postop);
    }
    $('.stay-on-top').css({top:offset+"px"});
    //$(name).animate({top:offset},{duration:500,queue:false});
  }

  $(window).resize(move_top);
  $(window).scroll(move_top);
});