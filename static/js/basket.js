$(document).ready(function(){
  $('.in_basket').popover({
    placement: 'bottom',
    content: $('#one-click-form').html(),
    container: 'body',
    html: 'true',
    trigger: 'manual',
  });

  $('body').on('click', function (e) {
    if (!$(e.target).hasClass('in_basket')
        && $(e.target).parents('.popover.in').length === 0) { 
      $('.in_basket').popover('hide');
      $('.popover').find('#id_phone_number').focus();
    }
  });
  $('.in_basket.yes').hover(
    function(){
      $('.in_basket').not($(this)).popover('hide');
      $(this).popover('show');
      $('.popover').find('#id_phone_number').focus();
    },
    function(){}
  );
});

function basket_toggle(id, target)
{
  $.ajax({
    url: '/basket/' + id + '/toggle',
    dataType: 'json'
  }).done(function(data) {
    $('.basket_count').html(data.count);
    var obj_yes = $('.basket_'+id+'.yes')
    var obj_no = $('.basket_'+id+'.no')
    if (data.result == '0') {
      $(obj_yes).hide();
      $(obj_no).show();
      $(obj_yes).popover('hide');
      $(obj_no).popover('hide');
      $(obj_no).unbind('mouseenter mouseleave');
    } else {
      $(obj_no).hide();
      $(obj_yes).show();
      $('.in_basket').not($(obj_yes)).popover('hide');
      $(obj_yes).popover('show');
      $('.popover').find('#id_phone_number').focus();
      $(obj_yes).hover(
        function(){
          $('.in_basket').not($(this)).popover('hide');
          $(this).popover('show');
          $('.popover').find('#id_phone_number').focus();
        },
        function(){}
      );
    }
  });
}
