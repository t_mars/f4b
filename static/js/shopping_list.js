function shopping_list_toggle(id, obj)
{
  $.ajax({
    url: '/shopping_list/' + id + '/toggle',
    dataType: 'json'
  }).done(function(data) {
    $('#shopping_list_count').html(data.count);
    if (data.result == '0') {
      $(obj).closest('div.in_list').find('.no').show();
      $(obj).closest('div.in_list').find('.yes').hide();
    } else {
      $(obj).closest('div.in_list').find('.no').hide();
      $(obj).closest('div.in_list').find('.yes').show();
    }
  });
}
