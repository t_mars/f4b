

$(document).ready(
	function(){
		var input = $('input[name=rating]');
		function setRatingVal() 
		{
			$(".rating-rev .bull-color").removeClass("bull-color")
			for (var i=0;i<input.val();i++) {
				$(".rating-rev .bull").eq(i).addClass("bull-color");
			}
		}
		setRatingVal();
		$(".rating-rev").hover(
			function() {
				$(this).addClass("hovered");
				$(".rating-rev.hovered .bull").hover(
					function() {
						$(".rating-rev.hovered .bull-color").removeClass("bull-color")
						for (var i=0;i<=$(this).index();i++) {
							$(".rating-rev.hovered .bull").eq(i).addClass("bull-color");
						}
					},
					function() {
						$(".rating-rev.hovered .bull-color.bull1").removeClass("bull-color").addClass("bull-grey");
					}
				);
			},
			function() {
				$(this).removeClass("hovered");
				setRatingVal();
			}
		);
		$(".rating-rev .bull").click(function(){
			console.log($(this).index());
			input.val($(this).index()+1);
		})
	}
);