# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0004_productoffer_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='productoffer',
            name='raw_flavors',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
