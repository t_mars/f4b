# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0008_auto_20150404_1803'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, b'\xd0\x9d\xd0\x9e\xd0\x92\xd0\xab\xd0\x99'), (1, b'\xd0\x9e\xd0\x91\xd0\xa0\xd0\x90\xd0\x91\xd0\x90\xd0\xa2\xd0\xab\xd0\x92\xd0\x90\xd0\x95\xd0\xa2\xd0\xa1\xd0\xaf'), (2, b'\xd0\x92 \xd0\x94\xd0\x9e\xd0\xa1\xd0\xa2\xd0\x90\xd0\x92\xd0\x9a\xd0\x95'), (3, b'\xd0\x97\xd0\x90\xd0\x9a\xd0\xa0\xd0\xab\xd0\xa2'), (4, b'\xd0\x9e\xd0\xa2\xd0\x9c\xd0\x95\xd0\x9d\xd0\x95\xd0\x9d')]),
            preserve_default=True,
        ),
    ]
