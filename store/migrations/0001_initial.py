# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0081_productitem_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductOffer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.DecimalField(null=True, max_digits=10, decimal_places=0)),
                ('image', models.CharField(max_length=200, null=True, blank=True)),
                ('active', models.BooleanField(default=False)),
                ('product', models.ForeignKey(blank=True, to='wiki.Product', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
