#coding=utf8

from subdomains.utils import reverse
from django.contrib import admin
from django import forms

from models import *

class ProductOfferAdmin(admin.ModelAdmin):
	model = ProductOffer
	list_display = ['image_tag', 'product', 'name', 'price', 'active',
		'product_min_price', 'product_avg_price', 'product_max_price']
	raw_id_fields = ('product',)
	related_lookup_fields = {
		'fk': ['product'],
	}

	def product_max_price(self, obj):
		return obj.product.max_price
	product_max_price.admin_order_field  = 'max_price'
	product_max_price.short_description = 'max price'

	def product_min_price(self, obj):
		return obj.product.min_price
	product_min_price.admin_order_field  = 'min_price'
	product_min_price.short_description = 'min price'

	def product_avg_price(self, obj):
		return obj.product.avg_price
	product_avg_price.admin_order_field  = 'avg_price'
	product_avg_price.short_description = 'avg price'

	def image_tag(self, obj):
		return u'<img src="%s" style="max-width:100px;max-height:100px">' % obj.image
	image_tag.short_description = 'Image'
	image_tag.allow_tags = True

admin.site.register(ProductOffer, ProductOfferAdmin)

class OrderForm(forms.ModelForm):
	comment= forms.CharField(widget=forms.Textarea)
	
	def __init__(self, *args, **kwargs):
		super(OrderForm, self).__init__(*args, **kwargs)
		obj = kwargs.get('instance')
		if obj:
			help_text = u'<table class="table"><thead><th>Продукт</th><th>Вкус</th><th>Кол-во</th><th>Цена</th></thead>'
			for row in obj.raw_products.split('\r'):
				try:
					offer_id,count,price = row.split(',')
					offer = ProductOffer.objects\
						.select_related('product,product__brand,product__category')\
						.get(pk=offer_id)
				except:
					continue
				help_text += '<tr><td><a href="%s" target="_blank">%s<a></td><td>%s</td><td>%s</td><td>%s</td></tr>' % (
					reverse('product', args=[offer.product.slug], subdomain='pureprotein'), 
					offer.product,offer.name,
					count,price
				)
			help_text += '</table>'

			self.fields['raw_products'].help_text = help_text

class OrderAdmin(admin.ModelAdmin):
	form = OrderForm
	list_display = ['id', 'created', 'total_sum', 'status', 'phone_number', 'user']

admin.site.register(Order, OrderAdmin)