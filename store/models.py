#coding=utf8

from django.db import models
from django.contrib.auth.models import User

from wiki.models import *

# предложения моего магазина!
class ProductOffer(models.Model):
	name = models.CharField(max_length=200,null=True,blank=True)
	product = models.ForeignKey(Product, blank=True, null=True)
	product = models.ForeignKey(Product, blank=True, null=True)
	price = models.DecimalField(max_digits=10,decimal_places=0,null=True)
	image = models.CharField(max_length=200,blank=True, null=True)
	active = models.BooleanField(default=False)
	raw_flavors = models.TextField(null=True,blank=True)

	def flavors(self):
		return self.raw_flavors.split('\n')

	@property
	def real_price(self):
	    return self.price
	

ORDER_STATUS_CHOICE = {
	0: 'НОВЫЙ',
	1: 'ОБРАБАТЫВАЕТСЯ',
	2: 'В ДОСТАВКЕ',
	3: 'ЗАКРЫТ',
	4: 'ОТМЕНЕН',
}

class Order(models.Model):
	raw_products = models.TextField(null=False,blank=False)
	raw_contact = models.TextField(null=False,blank=False)
	total_sum = models.DecimalField(max_digits=10,decimal_places=0,null=True)
	comment = models.CharField(max_length=1000, blank=True, null=True)

	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	phone_number = models.CharField(max_length=10)
	user = models.ForeignKey(User,null=True,blank=True,default=None)
	
	status = models.IntegerField(default=0,choices=ORDER_STATUS_CHOICE.items(),null=False,blank=False)
	
	NEW = 0
	PROCESSED = 1
	DELIVER = 2
	CLOSED = 3
	CANCELED = 4