#coding=utf8
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wiki.models import *
from main.models import *

from helpers import *

def get_brand(id, slug, name):
	brand = Brand.objects.get(id=id)
	if slug != brand.slug:
		url = reverse(name, args=[id,brand.slug])
		raise RedirectException(url)
	return brand

def info(request, id, slug):
	try:
		brand = get_brand(id, slug, 'brand')
	except RedirectException as e:
		return e.render(request)
	
	categories = Category.objects.raw('SELECT DISTINCT c.* FROM wiki_category c \
		INNER JOIN wiki_product p ON p.category_id=c.id\
		WHERE p.brand_id='+id)
	
	products = Product.objects\
		.filter(error_code=0,profit_price__gte=100,brand_id=id)\
		.order_by('?')[:6]
	
	return render(request, 'main/brand/info.html', {
		'breadcrumbs': [('Бренды', ''), (brand.name, '')], 
		'brand': brand, 
		'categories': categories,
		'products': products,
	})