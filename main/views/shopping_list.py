#coding=utf8
from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wiki.models import *

def clear(request):
	request.session['shopping_list'] = []
	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
	
def toggle(request, id):
	shopping_list = request.session.get('shopping_list', [])
	if id in shopping_list:
		shopping_list.remove(id)
		result = '0'
	else:
		shopping_list.append(id)
		result = '1'
	request.session['shopping_list'] = shopping_list
	
	if request.is_ajax():
		return HttpResponse(
			json.dumps({'result': result, 'count': len(shopping_list)}),
			content_type="application/json"
		)
	else:
		return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def main(request):
	products = Product.objects.filter(id__in=request.session.get('shopping_list', []))
	
	# собираем данные
	shop_prices = {}
	for product in products:
		items = ProductItem.objects.filter(product=product,shop__active=True,actual=True)
		for item in items:
			shop_id = item.shop_id
			if shop_id not in shop_prices:
				shop_prices[shop_id] = {'items': {}}
			shop_prices[shop_id]['items'][product.id] = item
	
	# просчитываем данные
	for shop_id in shop_prices:
		data = shop_prices[shop_id]
		count = 0
		total = 0
		for item in data['items'].values():
			total += item.price
			if item.available != False:
				count += 1
		data['count'] = count
		data['total'] = total
	

	shops = {}
	for s in Shop.objects.filter(id__in=shop_prices.keys()):
		shops[s.id] = s
	
	def _cmp(x, y):
		return cmp( 
			(-len(x[1]['items']), -x[1]['count'], x[1]['total']), 
			(-len(y[1]['items']), -y[1]['count'], y[1]['total']) 
		)
		# if x[1]['count'] != y[1]['count']:
		# else:
		# 	return x[1]['total'] > y[1]['total']

	shop_prices = sorted(shop_prices.items(), cmp=_cmp)
	# raise Exception(shop_prices)
	# пакуем данные
	datas = []
	for shop_id,data in shop_prices:
		prices = [data['items'].get(p.id, None) for p in products]
		datas.append((shops[shop_id], prices, data['total']))

	paginator = Paginator(datas, 10)
	page = request.GET.get('page')
	try:
		datas = paginator.page(page)
	except PageNotAnInteger:
		datas = paginator.page(1)
	except EmptyPage:
		datas = paginator.page(paginator.num_pages)
		
	return render(request, 'main/shopping_list.html', {
		'breadcrumbs': [('Список покупок', '')],
		'shop_prices': datas,
		'shops': shops,
		'products': products	
	})
