#coding=utf8
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wiki.models import *
from main.models import *

from helpers import *

def get_shop(id, slug, name):
	shop = Shop.objects.get(id=id,active=True,verified=True)
	if slug != shop.slug:
		url = reverse(name, args=[id,shop.slug])
		raise RedirectException(url)
	return shop

# def list(request):
# 	shops = Shop.objects\
# 		.filter(verified=True,active=True)\
# 		.order_by('-rating')

# 	paginator = Paginator(shops, 10)
# 	page = request.GET.get('page')
# 	try:
# 		shops = paginator.page(page)
# 	except PageNotAnInteger:
# 		shops = paginator.page(1)
# 	except EmptyPage:
# 		shops = paginator.page(paginator.num_pages)

# 	return render(request, 'main/shop/list.html', {
# 		'breadcrumbs': [('Магазины', '')],
# 		'shops': shops,
# 	})

def info(request, id, slug):
	try:
		shop = get_shop(id, slug, 'shop')
	except RedirectException as e:
		return e.render(request)
	
	if not request.user or request.user.id != 1: 
		shop.views += 1
		shop.save()

	categories = Category.objects.raw(
		'SELECT DISTINCT c.* FROM wiki_category c \
		INNER JOIN wiki_product p ON p.category_id=c.id\
		INNER JOIN wiki_productitem shp ON shp.product_id=p.id\
		WHERE shp.shop_id='+id)
	
	best_products = Product.objects\
		.filter(error_code=0,profit_price__gte=100,best_deal__shop_id=shop.id)\
		.select_related('best_deal')\
		.order_by('?')[:6]
	
	from django.db.models import Q
	items = ProductItem.objects\
		.filter(shop_id=shop.id,product_id__isnull=False,actual=True)\
		.filter(Q(available=True) | Q(available__isnull=True))\
		.select_related('product,product__brand,product__category')\
		.order_by('?')[:6]
	
	return render(request, 'main/shop/info.html', {
		'breadcrumbs': [
			# ('Магазины', reverse('shop_list')), 
			(shop.name, '')
		], 
		'shop': shop, 
		'categories': categories,
		'best_products': best_products,
		'items': items,
	})

def reviews(request, id, slug):
	try:
		shop = get_shop(id, slug, 'shop_reviews')
	except RedirectException as e:
		return e.render(request)
	
	from django import forms
	
	class ReviewForm(forms.Form):
		comment = forms.CharField(required=True,
			widget=forms.Textarea(attrs={'class':'form-control block pull-left','rows':'3','placeholder':'Введите текст отзыва..'}),
			max_length=1000,min_length=10)
		rating = forms.IntegerField(required=True)

	form = None
	if request.user.is_authenticated() \
		and len(ShopReview.objects.filter(shop_id=id,user_id=request.user.id)) == 0:
		if request.method == 'POST':
			form = ReviewForm(request.POST)
			if form.is_valid():
				review = ShopReview()
				review.comment = form.cleaned_data['comment']
				review.rating = form.cleaned_data['rating']
				review.user = request.user
				review.shop_id = id
				review.save()
				shop.reset_rating()
				form = None
		else:
			form = ReviewForm()

	reviews = ShopReview.objects.filter(shop_id=id)

	return render(request, 'main/shop/reviews.html', {
		'breadcrumbs': [
			# ('Магазины', reverse('shop_list')), 
			(shop.name, reverse('shop', args=[shop.id,shop.slug])),
			('Отзывы', ''),
		], 
		'shop': shop,
		'reviews': reviews,
		'form': form
	})