#coding=utf8
from subdomains.utils import reverse
from django.shortcuts import render, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect

from wiki.models import *
from store.models import *

def main(request):
	return HttpResponseRedirect(
		reverse('home',subdomain='pureprotein')
	)
	products = Product.objects\
		.filter(has_offers=True)
	
	category_ids = products.values_list('category_id', flat=True)
	categories = Category.objects\
		.filter(pk__in=category_ids)

	return render(request, 'main/store/main.html', {
		'breadcrumbs': [
			('Магазин', ''), 
		], 
		'categories': categories,
		'products': products,
	})

def delivery(request):
	products = Product.objects\
		.filter(has_offers=True)[:6]

	return render(request, 'main/store/delivery.html', {
		'breadcrumbs': [
			('Магазин', reverse('store')), 
			('Доставка', ''), 
		], 
		'products': products,
	})