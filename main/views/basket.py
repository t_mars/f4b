#coding=utf8
from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from store.models import *

def toggle(request, id):
	basket = request.session.get('basket', {})
	if id in basket:
		del basket[id]
		result = '0'
	else:
		basket[id] = 1
		result = '1'
	request.session['basket'] = basket
	
	if request.is_ajax():
		return HttpResponse(
			json.dumps({'result': result, 'count': len(basket)}),
			content_type="application/json"
		)
	else:
		return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def main(request):
	basket = request.session.get('basket', {})

	pos = ProductOffer.objects\
		.select_related('product,product__brand,product__category')\
		.filter(id__in=basket,active=True)
	offers = []

	# обновления количества товаров
	if 'refresh' in request.POST.keys():
		for id in basket:
			k = 'count_%s' % id
			if k in request.POST.keys():
				try:
					v = int(request.POST[k])
				except:
					v = 1
				basket[id] = v
		request.session['basket'] = basket

	from django import forms
	class ContactForm(forms.Form):
		comment = forms.CharField(required=False,
			widget=forms.Textarea(attrs={'class':'form-control block pull-left','rows':'3'}),
			max_length=1000)
		phone_number = forms.RegexField(
			regex=r'^\d{10}$',required=True,max_length=10,min_length=10,
			widget=forms.TextInput(attrs={'class':'form-control'})
		)
		name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))

	total_sum = 0
	for po in pos:
		count = int(basket.get(str(po.id), 1))
		s = count * po.real_price
		total_sum += s
		offers.append({
			'id': po.id,
			'product': po.product,
			'price': po.real_price,
			'count': count,
			'sum': s,
			'name': po.name,
		})

	form = None
	if len(offers):
		if 'order' in request.POST.keys():
			form = ContactForm(request.POST)
			if form.is_valid():
				order = Order()
				order.comment = form.cleaned_data['comment']
				order.phone_number = form.cleaned_data['phone_number']
				order.total_sum = total_sum
				order.raw_contact = u'Имя: %s' % form.cleaned_data['name']
				
				raw_products = ''
				for offer in offers:
					raw_products += '%s,%s,%s\r' % (offer['id'],offer['count'],offer['price'])
				order.raw_products = raw_products

				if request.user.is_authenticated():
					order.user = request.user
				order.save()
				
				del request.session['basket']

				return render(request, 'main/basket_thanks.html', {
					'breadcrumbs': [('Корзина', '')],
					'order_id': order.id,
					'total_sum': total_sum,
				})
		else:
			form = ContactForm()

	return render(request, 'main/basket.html', {
		'breadcrumbs': [('Корзина', '')],
		'form': form,
		'offers': offers,
		'total_sum': total_sum,	
	})