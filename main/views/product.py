#coding=utf8
from subdomains.utils import reverse
from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wiki.models import *
from main.models import *
from store.models import *

from helpers import *

def get_product(id, slug, name):
	product = Product.objects\
		.select_related('category','brand')\
		.get(id=id)
	# переадресовываем на pureprotein
	if product.has_offers:
		url = reverse('product', args=[product.slug], subdomain='pureprotein')
		raise RedirectException(url)
	if product.original_product:
		product = product.original_product
		slug = ''
	if slug != product.slug:
		url = reverse(name, args=[product.id,product.slug])
		raise RedirectException(url)
	return product

def get_breadcrumbs(product, self=False):
	breadcrumbs = []
	
	for c in product.category.get_ancestors(include_self=True):
		breadcrumbs.append( (c.name, reverse('category', args=[c.id,c.slug])) )
		if not c.is_leaf_node():
			breadcrumbs.append( ('Остальные', '%sother/' % reverse('category', args=[c.id,c.slug])) )
	
	breadcrumbs.append((
		product.brand.name, 
		'%s?brands=%d' % (product.category.url(), product.brand.id) 
	))
	url = ''
	if self:
		url = reverse('product', args=[product.id, product.slug])
	breadcrumbs.append( ('%s %s' % (product.name, product.packing), url) )
	return breadcrumbs

def ingredients(request, id, slug):
	try:
		product = get_product(id, slug, 'product_ingredients')
	except RedirectException as e:
		return e.render(request)
	
	# крошки
	breadcrumbs = get_breadcrumbs(product, self=True)
	breadcrumbs.append( ('Состав', '') )
	
	# предложения магазина
	offers = ProductOffer.objects\
		.filter(product_id=product.id,active=True)

	return render(request, 'main/product/ingredients.html', {
		'breadcrumbs': breadcrumbs, 
		'product': product, 
		'offers': offers,
	})

def prices(request, id, slug):
	try:
		product = get_product(id, slug, 'product')
	except RedirectException as e:
		return e.render(request)
	
	if not request.user or request.user.id != 1: 
		product.views += 1
		product.save()

	# фильтр доступности товаров
	if len(set(request.GET.keys()) & set(['av','na','un'])) == 0:
		av = True
		na = False
		un = True
	else:
		av = 'av' in request.GET
		na = 'na' in request.GET
		un = 'un' in request.GET
	
	# сортировка
	sort_fields = [
		('price', 'price', 'По цене'),
		('rating', 'shop__rating', 'По рейтингу'),
	]
	sort,order_by = sorter(sort_fields, request)
	# сортировка по рейтингу и цене
	order_by = [order_by]
	if 'rating' in sort:
		order_by.append('price')

	from django.db.models import Q
	available_filter = []
	if av: available_filter.append(Q(available=True))
	if na: available_filter.append(Q(available=False))
	if un: available_filter.append(Q(available__isnull=True))

	import operator
	items = ProductItem.objects\
		.select_related('shop')\
		.filter(product=product,shop__active=True,actual=True)\
		.filter(reduce(operator.or_, available_filter))\
		.order_by(*order_by)
	
	paginator = Paginator(items, 20)
	page = request.GET.get('page')
	try:
		items = paginator.page(page)
	except PageNotAnInteger:
		items = paginator.page(1)
	except EmptyPage:
		items = paginator.page(paginator.num_pages)
	

	related_products = []
	if product.related_product_ids:
		related_products = Product.objects\
			.select_related('brand','category')\
			.filter(id__in=product.related_product_ids.split(','))\
			.filter(original_product__isnull=True)\
			.order_by('?')[:2]

	# добавляем если меньше трех товаров
	related_products = list(related_products)
	if len(related_products) < 3:
		ps = Product.objects\
			.select_related('brand','category')\
			.filter(has_offers=True)\
			.exclude(id=product.id)\
			.order_by('?')
		related_products.extend(list(
			ps.filter(category_id=product.category.id)[:3-len(related_products)]
		))
		if len(related_products) < 3:
			related_products.extend(list(
				ps[:3-len(related_products)]
			))
	related_products = related_products[::-1]

	# крошки
	breadcrumbs = get_breadcrumbs(product)
	
	# предложения магазина
	offers = ProductOffer.objects\
		.filter(product_id=product.id,active=True)

	return render(request, 'main/product/prices.html', {
		'breadcrumbs': breadcrumbs,
		'queries': queries_without_page(request), 
		'related_products': related_products, 
		'product': product, 
		'items': items,
		'sort_fields': sort_fields,
		'sort': sort,
		'av': av,
		'na': na,
		'un': un,
		'offers': offers,
	})

def reviews(request, id, slug):
	try:
		product = get_product(id, slug, 'product_reviews')
	except RedirectException as e:
		return e.render(request)
	
	from django import forms
	class ReviewForm(forms.Form):
		comment = forms.CharField(required=True,
			widget=forms.Textarea(attrs={'class':'form-control block pull-left','rows':'3','placeholder':'Введите текст отзыва..'}),
			max_length=1000,min_length=10)
		rating = forms.IntegerField(required=True,initial=1)

	form = None
	if request.user.is_authenticated() \
		and len(ProductReview.objects.filter(product_id=id,user_id=request.user.id)) == 0:
		if request.method == 'POST':
			form = ReviewForm(request.POST)
			if form.is_valid():
				review = ProductReview()
				review.comment = form.cleaned_data['comment']
				review.rating = form.cleaned_data['rating']
				review.user = request.user
				review.product_id = id
				review.save()
				product.reset_rating()
				form = None
		else:
			form = ReviewForm()

	# сортировка
	# сортировка
	sort_fields = [
		('date', 'created', 'По дате'),
		('rating', 'rating', 'По рейтингу'),
	]
	sort,order_by = sorter(sort_fields, request, '-date')

	reviews = ProductReview.objects\
		.filter(product=product)\
		.order_by(order_by)

	paginator = Paginator(reviews, 10)
	page = request.GET.get('page')
	try:
		reviews = paginator.page(page)
	except PageNotAnInteger:
		reviews = paginator.page(1)
	except EmptyPage:
		reviews = paginator.page(paginator.num_pages)

	# крошки
	breadcrumbs = get_breadcrumbs(product, self=True)
	breadcrumbs.append( ('Отзывы', '') )
	
	# предложения магазина
	offers = ProductOffer.objects\
		.filter(product_id=product.id,active=True)

	return render(request, 'main/product/reviews.html', {
		'breadcrumbs': breadcrumbs, 
		'queries': queries_without_page(request), 
		'product': product,
		'reviews': reviews,
		'form': form,
		'sort': sort,
		'sort_fields': sort_fields,
		'offers': offers,
	})

def description(request, id, slug):
	try:
		product = get_product(id, slug, 'product_description')
	except RedirectException as e:
		return e.render(request)
	
	# крошки
	breadcrumbs = get_breadcrumbs(product, self=True)
	breadcrumbs.append( ('Описание', '') )
	
	related_products = []
	if product.related_product_ids:
		related_products = Product.objects\
			.filter(id__in=product.related_product_ids.split(','))\
			.filter(original_product__isnull=True)\
			.select_related('brand','category')\
			.order_by('?')[:3]
	
	# предложения магазина
	offers = ProductOffer.objects\
		.filter(product_id=product.id,active=True)

	return render(request, 'main/product/description.html', {
		'breadcrumbs': breadcrumbs, 
		'product': product,
		'related_products': related_products,
		'offers': offers,
	})

# сохранение даты открытия товара 
# переадресация в магазин
def item_jump(request, id):
	try:
		item = ProductItem.objects.get(pk=id)
		
		jump = ProductItemJump()
		if request.user.is_authenticated():
			jump.user_id = request.user.id
		jump.item = item
		jump.save()

		return HttpResponseRedirect(item.url)
	except:
		raise Http404