from django.http import HttpResponseRedirect

class RedirectException(Exception):
	def __init__(self, url):
		self.url = url

	def render(self, request):
		return HttpResponseRedirect(self.url)

def sorter(fields, request, default=None):
	modes = {}
	for name,field,desc in fields:
		modes[name] = field
		modes['-'+name] = '-'+field
	
	if 'sort' in request.GET and request.GET['sort'] in modes:
		s = request.GET['sort']
	else:
		s = default or fields[0][0]
	
	return s,modes[s]

def queries_without_page(request):
	r = request.GET.copy()
	if r.has_key('page'):
		del r['page']
	return r
	