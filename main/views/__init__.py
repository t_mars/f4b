#coding=utf8
from django.shortcuts import render, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect

from wiki.models import *
from main.models import *

from .helpers import *

def logout(request):
	from django.contrib.auth import logout
	logout(request)

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def home(request):
	from django.db.models import Count
	categories = Category.objects.all()
	products = Product.objects\
		.filter(error_code=0,available_item_count__gte=10,profit_price__gte=100)\
		.filter(original_product__isnull=True)\
		.order_by('?')[:6]
	
	offer_products = Product.objects\
		.filter(has_offers=True)\
		.order_by('?')[:6]
	
	return render(request, 'main/home.html', {
		'categories': categories,
		'products': products,
		'offer_products': offer_products,
	})

def search(request):
	if 'q' in request.GET:
		products = Product.search.query(request.GET['q'])
		items = ProductItem.search.query(request.GET['q'])
		items = [item for item in items if not item.product]
	return render(request, 'main/search.html', {
		'breadcrumbs': [('Поиск', '')],
		'products': products,
		'items': items,
	})