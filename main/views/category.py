#coding=utf8
from django.core.urlresolvers import reverse
from django.shortcuts import render, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect

from wiki.models import *
from main.models import *

from helpers import *

def get_category(id, slug, name):
	category = Category.objects.get(id=id)
	from unidecode import unidecode
	if slug != category.slug:
		url = reverse(name, args=[id,category.slug])
		raise RedirectException(url)
	return category

def get_breadcrumbs(category, include_self=False):
	breadcrumbs = []
	for c in category.get_ancestors(include_self=include_self):
		breadcrumbs.append( (c.name, reverse('category', args=[c.id,c.slug])) )
	if not include_self:
		breadcrumbs.append( (category.name, '') )
	return breadcrumbs

def subcategories(request, category):
	categories = category.get_children()
	
	breadcrumbs = get_breadcrumbs(category) 
	
	return render(request, 'main/category/subcategories.html', {
		'breadcrumbs': breadcrumbs, 
		'categories': categories, 
		'category': category, 
	})

def info(request, id, slug, other):
	try:
		category = get_category(id, slug, 'category')
	except RedirectException as e:
		return e.render(request)

	if not other and not category.is_leaf_node():
		return subcategories(request, category)
	
	product_list = Product.objects\
		.filter(category=category, original_product__isnull=True)
	
	# сортировка
	from helpers import sorter
	sort_fields = [
		('price', 'min_price', 'По цене'),
		('rating', 'rating', 'По рейтингу'),
	]
	sort, order_by = sorter(sort_fields, request)
	if 'price' in sort:
		product_list = product_list\
			.extra(select={'min_price_is_null': 'min_price is null'})\
			.order_by('min_price_is_null', order_by)
	else:
		product_list = product_list.order_by(order_by)
	
	packing_translate = {
		'ml': 'Милилитров',
		'gr': 'Грамм',
		'packs': 'Пачек',
		'tab': 'Таблеток',
		'servings': 'Порций',
		'amp': 'Ампул',
	}
	measures = []
	packs = {}
	for measure,(mn,mx) in category.packing_variants.iteritems():
		if mn==mx or measure=='packing':
			continue
		packs[measure] = {
			'min': int(mn), 
			'max': int(mx), 
			'value': '[%d,%d]' % (mn,mx),
			'label': packing_translate[measure],
			'enable': True
		}
		measures.append(measure)
	
	from django import forms
	# brand_ids = [p.brand.id for p in product_list]
	brand_ids = product_list.values_list('brand_id', flat=True)
	class FilterForm(forms.Form):
		brands = forms.MultipleChoiceField(
			choices=((b.id,b.name) for b in Brand.objects.filter(id__in=brand_ids)),
			widget=forms.SelectMultiple(attrs={'class':'select2 col-md-8 form-control'})
		)
	
	# фильтр по брендам
	sub_brand = None
	if 'brands' in request.GET.keys():
		form = FilterForm(request.GET)
		if form.is_valid():
			product_list = product_list.filter(brand_id__in=form.cleaned_data['brands'])
			if len(form.cleaned_data['brands']) == 1:
				sub_brand = Brand.objects.get(pk=form.cleaned_data['brands'][0])
	else:
		form = FilterForm()
	
	# поиск по названию
	if request.GET.get('name'):
		product_list = product_list.filter(
			pk__in=[p.id for p in Product.search.query(request.GET['name'])]
		)

	# фильтр по упаковке
	filter_packing = {}
	if set(request.GET.keys()) & set(packing_translate.keys()):
		for measure in measures:
			if measure in request.GET:
				mn,mx = request.GET[measure].split(',')
				packs[measure]['value'] = '[%s,%s]' % (mn,mx)
				filter_packing[measure] = (int(mn),int(mx))
			else:
				packs[measure]['enable'] = False
		products = []
		for product in product_list:
			ps = product.packings
			for measure,(mn,mx) in filter_packing.iteritems():
				if measure in ps:
					val = int(ps[measure])
					if val >= mn and val <= mx:
						products.append(product)
		
	else:
		products = product_list

	# хлебные крошки
	if other and not category.is_leaf_node():
		breadcrumbs = get_breadcrumbs(category,include_self=True)
		if sub_brand:
			breadcrumbs.append((
				'Остальные', 
				'%sother/' % reverse('category', args=[category.id,category.slug]) 
			))
			breadcrumbs.append( (sub_brand.name, '') )
		else:
			breadcrumbs.append( ('Остальные', '') )
	else:
		if sub_brand:
			breadcrumbs = get_breadcrumbs(category,include_self=True)
			breadcrumbs.append( (sub_brand.name, '') )
		else:
			breadcrumbs = get_breadcrumbs(category)

	# разбиение по страницам
	count = len(products)
	paginator = Paginator(products, 50)
	page = request.GET.get('page')
	try:
		products = paginator.page(page)
	except PageNotAnInteger:
		products = paginator.page(1)
	except EmptyPage:
		products = paginator.page(paginator.num_pages)

	offer_products = Product.objects\
		.filter(category=category,has_offers=True)

	other_offer_products = Product.objects\
		.filter(has_offers=True)\
		.order_by('?')[:3]

	return render(request, 'main/category/products.html', {
		'breadcrumbs': breadcrumbs, 
		'products': products, 
		'offer_products': offer_products, 
		'other_offer_products': other_offer_products, 
		'category': category,
		'packs': packs,
		'queries': queries_without_page(request),
		'form': form,
		'sort': sort,
		'sort_fields': sort_fields,
	})
