from django.shortcuts import render_to_response, redirect
from django import template

from wiki.models import Category

register = template.Library()

@register.inclusion_tag('main/category_menu.html')
def category_menu():
	return {'categories': Category.objects.all()}