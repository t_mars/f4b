# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_productreview_fake_username'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productreview',
            name='created',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
    ]
