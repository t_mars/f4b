# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('wiki', '0061_category__packing_variants'),
        ('main', '0002_auto_20150223_1431'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShopReview',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.PositiveIntegerField()),
                ('comment', models.CharField(max_length=1000)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('shop', models.ForeignKey(to='wiki.Shop')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
