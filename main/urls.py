from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^summernote/', include('lib.django_summernote.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    
    url(r'^store/$', 'main.views.store_page.main', name='store'),
    url(r'^store/delivery/$', 'main.views.store_page.delivery', name='store_delivery'),
    
    url(r'^basket/(?P<id>\d+)/toggle/$', 'main.views.basket.toggle', name='basket_toggle'),
    url(r'^basket/$', 'main.views.basket.main', name='basket'),
    
    url(r'^$', 'main.views.home', name='home'),
    
    url(r'^search/$', 'main.views.search', name='search'),
    url(r'^account/logout/$', 'main.views.logout', name='logout'),
    
    url(r'^category/(?P<id>\d+)/$', 'main.views.category.info', {'slug': '', 'other': False}),
    url(r'^category/(?P<id>\d+)/(?P<slug>[\w-]+)/$', 'main.views.category.info', {'other': False}, name='category', ),
    url(r'^category/(?P<id>\d+)/(?P<slug>[\w-]+)/other/$', 'main.views.category.info', {'other': True}),
    
    url(r'^brand/(?P<id>\d+)/$', 'main.views.brand.info', {'slug': ''}),
    url(r'^brand/(?P<id>\d+)/(?P<slug>[\w-]+)/$', 'main.views.brand.info', name='brand'),
    
    # url(r'^shop/list/$', 'main.views.shop.list', name='shop_list'),

    url(r'^shop/(?P<id>\d+)/reviews/$', 'main.views.shop.reviews', {'slug': ''}),
    url(r'^shop/(?P<id>\d+)/(?P<slug>[\w-]+)/reviews/$', 'main.views.shop.reviews', name='shop_reviews'),    
    
    url(r'^shop/(?P<id>\d+)/$', 'main.views.shop.info', {'slug': ''}),
    url(r'^shop/(?P<id>\d+)/(?P<slug>[\w-]+)/$', 'main.views.shop.info', name='shop'),
    
    url(r'^product/(?P<id>\d+)/ingredients/$', 'main.views.product.ingredients', {'slug': ''}),
    url(r'^product/(?P<id>\d+)/(?P<slug>[\w-]+)/ingredients/$', 'main.views.product.ingredients', name='product_ingredients'),
  
    url(r'^product/(?P<id>\d+)/reviews/$', 'main.views.product.reviews', {'slug': ''}),
    url(r'^product/(?P<id>\d+)/(?P<slug>[\w-]+)/reviews/$', 'main.views.product.reviews', name='product_reviews'),
  
    url(r'^product/(?P<id>\d+)/description/$', 'main.views.product.description', {'slug': ''}),
    url(r'^product/(?P<id>\d+)/(?P<slug>[\w-]+)/description/$', 'main.views.product.description', name='product_description'),
  
    url(r'^product/(?P<id>\d+)/$', 'main.views.product.prices', {'slug': ''}),
    url(r'^product/(?P<id>\d+)/(?P<slug>[\w-]+)/$', 'main.views.product.prices', name='product'),
  
    url(r'^jump/(?P<id>\d+)/$', 'main.views.product.item_jump', name='product_item_jump'),
    
    url(r'^shopping_list/clear/$', 'main.views.shopping_list.clear', name='shopping_list_clear'),
    url(r'^shopping_list/(?P<id>\d+)/toggle/$', 'main.views.shopping_list.toggle', name='shopping_list_toggle'),
    url(r'^shopping_list/$', 'main.views.shopping_list.main', name='shopping_list'),
    
	url(r'^admin/', include(admin.site.urls)),
	url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
	url(r'', include('social_auth.urls')),
)
