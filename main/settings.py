"""
Django settings for BlogTender project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os,sys
from conf import *

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
ROOT_DIR = os.path.dirname(BASE_DIR)

sys.path.append(os.path.join(BASE_DIR, "lib"))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'y1sn&0w#rsgwae6uctv8(1awjjv06ms6sy0wsg)15^6ed@ll5k'

# SECURITY WARNING: don't run with debug turned on in production!

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'fuel4bull@gmail.com'
EMAIL_HOST_PASSWORD = 'xkuebonyxfikhvff'
EMAIL_PORT = 587 
EMAIL_USE_TLS = True
FEEDBACK_EMAILS = ['t.mars@mail.ru', 'denisov.g@gmail.com']

ROOT_URLCONF = 'main.urls'
SITE_ID = 1
ALLOWED_HOSTS = [
    'fuel4bull.ru',
    'www.fuel4bull.ru',
    'pureprotein.fuel4bull.ru',
    'blog.fuel4bull.ru',
]
# Application definition
INSTALLED_APPS = (
    'lib.grappelli',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.sites',

    'lib.subdomains',
    
    'sorl.thumbnail',
    'captcha',
    'lib.password_reset',

    'lib.mptt',
    'lib.cuser',
    'lib.social_auth',
    'lib.djangosphinx',
    'lib.django_summernote',
    'tinymce',
    'widget_tweaks',
    
    'forum',
    'pureprotein',
    'blog',
    'wiki',
    'store',
    'main',
    'qa',
)

THUMBNAIL_DEBUG = True

TINYMCE_DEFAULT_CONFIG = {
    'plugins': "table,paste,searchreplace",
    'theme': "advanced",
    'cleanup_on_startup': True,
    'custom_undo_redo_levels': 10,
}

SPHINX_API_VERSION = 0x116
SPHINX_PORT = 9312
SPHINX_SERVER = '127.0.0.1'

AUTHENTICATION_BACKENDS = (
    'lib.social_auth.backends.contrib.vk.VKOAuth2Backend',
    'lib.social_auth.backends.facebook.FacebookBackend',
    'lib.social_auth.backends.twitter.TwitterBackend',
    'lib.social_auth.backends.google.GoogleOAuth2Backend',
    'lib.social_auth.backends.contrib.odnoklassniki.OdnoklassnikiBackend',
    'django.contrib.auth.backends.ModelBackend',
)

VK_APP_ID = '4686310'
VK_API_SECRET = 'ejkJ2nbG38R4BCfpDStd'

FACEBOOK_APP_ID = '294601757322662'
FACEBOOK_API_SECRET = '349cff8e08e76a2263b93215002f5be1'

TWITTER_CONSUMER_KEY = 'P5Rg76KXzzjbatw9BGiZHxEDY'
TWITTER_CONSUMER_SECRET = 'hVpxiN8LxwFDSJZy4J1vDaJoYwSJ7mIk8aoIuk48C5QfDcxi31'

GOOGLE_OAUTH2_CLIENT_ID = '481660475953-1p8b907smglkrj9enqj6iq31qq1dekts.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET = '-kbVNc9c60vciv-BZc6_6jY9'

ODNOKLASSNIKI_OAUTH2_CLIENT_KEY = '123'
ODNOKLASSNIKI_OAUTH2_APP_KEY = '123'
ODNOKLASSNIKI_OAUTH2_CLIENT_SECRET = '123'

LOGIN_REDIRECT_URL = '/profile/logged-in/'

SESSION_SERIALIZER='django.contrib.sessions.serializers.PickleSerializer'

MIDDLEWARE_CLASSES = (
    'subdomains.middleware.SubdomainURLRoutingMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'lib.cuser.middleware.CuserMiddleware',
    'blog.dispatchers.ScroringMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
)


WSGI_APPLICATION = 'main.wsgi.application'

LOG_DIR = os.path.join(ROOT_DIR, 'logs')
CONF_DIR = os.path.join(ROOT_DIR, 'conf')
TMP_DIR = os.path.join(ROOT_DIR, 'tmp')

SHOP_PARSER_CONF = os.path.join(CONF_DIR, 'shop')
SHOP_PARSER_LOG = os.path.join(LOG_DIR, 'parser')
SHOP_PARSER_TMP = os.path.join(TMP_DIR, 'parser')

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

# USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_ROOT = os.path.join(ROOT_DIR, "httpdocs")
STATIC_URL = '/static/'
MEDIA_URL = '/static/media/'
MEDIA_ROOT = os.path.join(STATIC_ROOT, "media")

UPLOAD_DIR = os.path.join(STATIC_ROOT, "upload")
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)


PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))

TEMPLATE_DIRS = (
    os.path.join(PROJECT_PATH, "templates"),
    os.path.join(BASE_DIR, "main", "templates"),
    os.path.join(BASE_DIR, "wiki", "templates"),
    os.path.join(BASE_DIR, "forum", "templates"),
    os.path.join(BASE_DIR, "blog", "templates"),
    os.path.join(BASE_DIR, "lib", "mptt", "templates"),   
    os.path.join(BASE_DIR, "lib", "django_object_actions", "templates"),   
)
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'lib.apptemplates.Loader',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'logfile': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': ROOT_DIR + "/logs/logfile",
            'maxBytes': 50000,
            'backupCount': 2,
            'formatter': 'standard',
        },
        'console':{
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level':'WARN',
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'MYAPP': {
            'handlers': ['logfile'],
            'level': 'DEBUG',
        },
    }
}

SOCIAL_AUTH_PIPELINE = (
    'social_auth.backends.pipeline.social.social_auth_user',
    'blog.models.get_username',
    'social_auth.backends.pipeline.user.get_username',
    'social_auth.backends.pipeline.user.create_user',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.social.load_extra_data',
    'social_auth.backends.pipeline.user.update_user_details',
    'blog.models.user_profile_data',
)

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

# from blog.dispatchers import *