from django.shortcuts import render
from django.shortcuts import render_to_response
from django.contrib import admin
from django import forms

from models import *

class ProductReviewForm(forms.ModelForm):
	comment= forms.CharField(widget=forms.Textarea)
	class Meta:
		model = ProductReview
		fields = ['product', 'user', 'fake_username',
			'rating', 'created']

class ProductReviewAdmin(admin.ModelAdmin):
	form = ProductReviewForm
	list_display = ['user','fake_username','product','rating','created']

	raw_id_fields = ('product',)
	related_lookup_fields = {
		'fk': ['product'],
	}

	def save_model(self, request, obj, form, change):
		super(ProductReviewAdmin, self).save_model(request, obj, form, change)
		obj.product.reset_rating()
		
admin.site.register(ProductReview, ProductReviewAdmin)