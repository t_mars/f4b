#coding=utf8
import datetime

from django.db import models
from django.contrib.auth.models import User

from wiki.models import *

class ProductReview(models.Model):
	product = models.ForeignKey(Product, blank=False, null=False)
	user = models.ForeignKey(User, blank=False, null=False)
	fake_username = models.CharField(max_length=100, blank=True, null=True)

	rating = models.PositiveIntegerField()
	comment = models.CharField(max_length=1000, blank=False, null=False)
	
	created = models.DateTimeField()
	
	def save(self, *args, **kwargs):
		if not self.id and not self.created:
			self.created = datetime.datetime.today()
		return super(ProductReview, self).save(*args, **kwargs)

class ShopReview(models.Model):
	shop = models.ForeignKey(Shop, blank=False, null=False)
	user = models.ForeignKey(User, blank=False, null=False)
	
	rating = models.PositiveIntegerField()
	comment = models.CharField(max_length=1000, blank=False, null=False)
	
	created = models.DateTimeField(auto_now_add=True)