# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('qa', '0004_questionsubs'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='questionsubs',
            unique_together=set([('question', 'user')]),
        ),
    ]
