# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('qa', '0005_auto_20150611_0906'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='subs_count',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
