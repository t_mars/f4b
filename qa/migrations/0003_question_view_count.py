# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('qa', '0002_question_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='view_count',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
