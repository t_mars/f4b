from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'qa.views.list', name='qa_home'),
    url(r'^new/$', 'qa.views.list', name='qa_list'),
    url(r'^without_answer/$', 'qa.views.list', {'without_answer': True}, name='qa_list_without_answer'),
    url(r'^create/$', 'qa.views.create', name='qa_create'),
    url(r'^(?P<id>\d+)/subscribe/$', 'qa.views.subscribe', name='qa_subscribe'),
    url(r'^(?P<id>\d+)/unsubscribe/$', 'qa.views.unsubscribe', name='qa_unsubscribe'),
    url(r'^(?P<id>\d+)/$', 'qa.views.question_show', {'slug': ''}),
    url(r'^(?P<id>\d+)/(?P<slug>[\w-]+)/$', 'qa.views.question_show', name='qa_question'),
    url(r'^(?P<qid>\d+)/select/(?P<aid>\d+)/$', 'qa.views.select_answer', name='qa_select_answer'),
)
