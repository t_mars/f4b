from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

from unidecode import unidecode

def get_slug(name):
	return slugify(unidecode(name))

from blog.models import InvocationObject

class Question(models.Model, InvocationObject):
	title = models.CharField(max_length=200)
	slug = models.CharField(max_length=200)
	_content = models.TextField(db_column='content')

	created_at = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User)

	correct_answer = models.ForeignKey('Answer',null=True,default=None,related_name='correct_answer')
	answer_count = models.PositiveIntegerField(default=0)
	view_count = models.PositiveIntegerField(default=0)
	subs_count = models.PositiveIntegerField(default=0)

	def save(self, *args, **kwargs):
		self.slug = get_slug(self.title)
		super(Question, self).save(*args, **kwargs)

class Answer(models.Model):
	content = models.TextField()
	is_correct = models.BooleanField(default=False)
	question = models.ForeignKey(Question)
	
	created_at = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User)

class QuestionSubs(models.Model):
	question = models.ForeignKey(Question)
	user = models.ForeignKey(User)

	class Meta:
		unique_together = (('question', 'user'),)