#coding=utf8
from django.views.decorators.csrf import csrf_protect
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django import forms

from models import *
from blog.models import Notification, ViewLog
from blog.views.helpers import user_access

def list(request, without_answer=False):
	questions = Question.objects.all().order_by('-created_at')
	if without_answer:
		questions = questions.filter(correct_answer=None)
	
	paginator = Paginator(questions, 20)
	page = request.GET.get('page')
	try:
		questions = paginator.page(page)
	except:
		questions = paginator.page(1)

	return render(request, 'qa/list.html', {
		'questions': questions,
		'without_answer': without_answer,
	})

@user_access
def create(request):
	class QuestionForm(forms.Form):
		title = forms.CharField(min_length=5,max_length=200)
		content = forms.CharField(widget=forms.Textarea(),min_length=20,max_length=3000)
	
	if request.method == 'POST':
		form = QuestionForm(request.POST)
		if form.is_valid():
			question = Question()
			question.title = form.cleaned_data['title']
			question.content = form.cleaned_data['content']
			question.user = request.user
			question.save()

			Notification().new_question(question)

			return HttpResponseRedirect(reverse('qa_question', args=[question.id,question.slug]))
	else:
		form = QuestionForm()

	return render(request, 'qa/create.html', {
		'breadcrumbs': [('Вопросы', reverse('qa_home')), ('Создать', '')],
		'form': form,
	})

def question_show(request, id, slug):
	try:
		question = Question.objects.get(id=id)
	except:
		raise Http404
	
	if slug != question.slug:
		url = reverse('qa_question', args=[id,question.slug])
		return HttpResponseRedirect(url)

	class AnswerForm(forms.Form):
		content = forms.CharField(widget=forms.Textarea)

	form = None
	if request.user.is_authenticated() and question.user.id != request.user.id:
		if request.method == 'POST':
			form = AnswerForm(request.POST)
			if form.is_valid():
				answer = Answer()
				answer.question = question
				answer.user = request.user
				answer.content = form.cleaned_data['content']
				answer.save()

				Notification().new_answer(answer)

				url = reverse('qa_question', args=[question.id,question.slug])
				return HttpResponseRedirect('%s#%s' % (url, answer.id))
		else:
			form = AnswerForm()

	# считаем кол-во просмотров
	ViewLog.add(request, 'question', question.id)
	question.view_count = ViewLog.count('question', question.id)
	
	# смотрим ответы
	answers = Answer.objects\
		.filter(question=question)\
		.order_by('created_at')

	# пересчитываем кол-во ответов
	question.answer_count = answers.count()
	question.save()

	# делим на страницы
	paginator = Paginator(answers, 20)
	page = request.GET.get('page')
	try:
		answers = paginator.page(page)
	except:
		answers = paginator.page(paginator.num_pages)

	# флаг выбора правильного ответа
	select_correct_answer = False
	if request.user.id == question.user.id and question.correct_answer == None:
		select_correct_answer = True

	# флаг подписки
	has_subs = None
	if request.user.is_authenticated() and request.user.id != question.user.id:
		try:
			QuestionSubs.objects.get(user=request.user,question=question)
			has_subs = True
		except:
			has_subs = False
			pass

	return render(request, 'qa/question.html', {
		'breadcrumbs': [('Вопросы', reverse('qa_home')), (question.title, '')],
		'question': question,	
		'answers': answers,	
		'form': form,
		'select_correct_answer': select_correct_answer,
		'has_subs': has_subs
	})

@csrf_protect
def subscribe(request, id):
	import json

	if not request.user.is_authenticated():
		return HttpResponse(json.dumps({
			'status': 'error',
			'message': 'not authenticated',	
		})) 

	try:
		question = Question.objects.get(id=id)
	except:
		return HttpResponse(json.dumps({
			'status': 'error',
			'message': 'not found',	
		}))

	QuestionSubs.objects.get_or_create(user=request.user,question=question)
	question.subs_count = QuestionSubs.objects.filter(question=question).count()
	question.save()

	return HttpResponse(json.dumps({
		'status': 'success',
		'count': question.subs_count,
	}))

@csrf_protect
def unsubscribe(request, id):
	import json

	if not request.user.is_authenticated():
		return HttpResponse(json.dumps({
			'status': 'error',
			'message': 'not authenticated',	
		})) 

	try:
		question = Question.objects.get(id=id)
	except:
		return HttpResponse(json.dumps({
			'status': 'error',
			'message': 'not found',	
		}))

	try:
		QuestionSubs.objects.get(user=request.user,question=question).delete()
		status = 'success'
	except:
		status = 'not subscribed'

	question.subs_count = QuestionSubs.objects.filter(question=question).count()
	question.save()
	
	return HttpResponse(json.dumps({
		'status': status,
		'count': question.subs_count,
	}))

@user_access
def select_answer(request, qid, aid):
	try:
		question = Question.objects.get(id=qid)
	except:
		raise Http404('question not found')

	if question.user.id != request.user.id:
		raise Http404('access denied')

	try:
		answer = Answer.objects.get(id=aid)
	except:
		raise Http404('question not found')

	if answer.question.id != question.id:
		raise Http404('logic error')
	
	url =  request.build_absolute_uri(reverse('qa_question', args=[question.id,question.slug]))
	
	if url != request.META.get('HTTP_REFERER'):
		raise Http404('invalid request')

	answer.is_correct = True
	answer.save()

	question.correct_answer = answer
	question.save()

	return HttpResponseRedirect(url)
