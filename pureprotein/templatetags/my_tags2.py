from django import template

from wiki.models import Category

register = template.Library()

@register.inclusion_tag('pureprotein/catalog_menu.html')
def catalog_menu():
	cs = Category.objects\
		.raw(
			'SELECT category_id as id FROM wiki_product '
			'WHERE has_offers=1 '
			'ORDER BY category_id ASC'
		)
	category_ids = set()
	categories = []
	for category in cs:
		root = category.get_root()
		if root.id in category_ids:
			continue
		category_ids.add(root.id)
		categories.append(root)
	return {'categories': categories}