from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'pureprotein.views.home', name='home'),
	url(r'^delivery/$', 'pureprotein.views.delivery', name='delivery'),
	url(r'^opt/$', 'pureprotein.views.opt', name='opt'),
	url(r'^product/(?P<slug>[\w-]+)/$', 'pureprotein.views.product', name='product'),
    url(r'^basket/$', 'pureprotein.views.basket', name='basket'),
    url(r'^basket/(?P<id>\d+)/toggle/$', 'main.views.basket.toggle', name='basket_toggle'),
)
