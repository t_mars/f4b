#coding=utf8
from django.shortcuts import render
from django.core.urlresolvers import reverse

from store.models import *
from wiki.models import *
from main.models import *
from main.views.helpers import *

def home(request):
	products = Product.objects\
		.filter(has_offers=True)
	
	categories = Category.objects\
		.raw(
			'SELECT category_id as id FROM wiki_product '
			'WHERE has_offers=1 '
			'ORDER BY category_id ASC'
		)

	groups = []
	category_ids = []
	for category in categories:
		root = category.get_root()
		if root.id in category_ids:
			continue
		category_ids.append(root.id)
		ps = []
		for p in products:
			if p.category.get_root().id == root.id:
				ps.append(p)
		groups.append({
			'category': root,
			'products': ps,	
		})
	return render(request, 'pureprotein/home.html', {
		'groups': groups,
	})

def delivery(request):
	products = Product.objects\
		.filter(has_offers=True)\
		.order_by('?')[:6]

	return render(request, 'pureprotein/delivery.html', {
		'breadcrumbs': [('Доставка', '')], 
		'products': products,
	})

def opt(request):
	products = Product.objects\
		.filter(has_offers=True)\
		.order_by('?')[:6]

	return render(request, 'pureprotein/opt.html', {
		'breadcrumbs': [('Для оптовиков', '')], 
		'products': products,
	})

def product(request, slug):
	product = Product.objects\
		.select_related('category','brand')\
		.get(slug=slug,has_offers=True)
	offers = ProductOffer.objects\
		.filter(product_id=product.id)
	
	category = product.category.get_root()
	category_ids = [c.id for c in category.children.all()]
	breadcrumbs = [
		(category.name, '%s#cat%d' % (reverse('home'), category.id)),
		(product.name, '')
	]

	reviews = ProductReview.objects\
		.filter(product=product)\
		.order_by('-created')
	
	# предложения магазина
	related_products = Product.objects\
		.select_related('category','brand')\
		.filter(has_offers=True)\
		.exclude(id=product.id)\
		.order_by('?')[:3]

	return render(request, 'pureprotein/product/product.html', {
		'breadcrumbs': breadcrumbs,
		'product': product,
		'offers': offers,
		'reviews': reviews,
		'related_products': related_products,
	})

def basket(request):
	basket = request.session.get('basket', {})

	pos = ProductOffer.objects\
		.select_related('product,product__brand,product__category')\
		.filter(id__in=basket,active=True)
	offers = []

	# обновления количества товаров
	if 'refresh' in request.POST.keys():
		for id in basket:
			k = 'count_%s' % id
			if k in request.POST.keys():
				try:
					v = int(request.POST[k])
				except:
					v = 1
				basket[id] = v
		request.session['basket'] = basket

	from django import forms
	class ContactForm(forms.Form):
		comment = forms.CharField(required=False,
			widget=forms.Textarea(attrs={'class':'form-control block pull-left','rows':'3'}),
			max_length=1000)
		phone_number = forms.RegexField(
			regex=r'^\d{10}$',required=True,max_length=10,min_length=10,
			widget=forms.TextInput(attrs={'class':'form-control'})
		)
		name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))

	total_sum = 0
	for po in pos:
		count = int(basket.get(str(po.id), 1))
		s = count * po.real_price
		total_sum += s
		offers.append({
			'id': po.id,
			'product': po.product,
			'price': po.real_price,
			'count': count,
			'sum': s,
			'name': po.name,
		})

	form = None
	if len(offers):
		if 'order' in request.POST.keys():
			form = ContactForm(request.POST)
			if form.is_valid():
				order = Order()
				order.comment = form.cleaned_data['comment']
				order.phone_number = form.cleaned_data['phone_number']
				order.total_sum = total_sum
				order.raw_contact = u'Имя: %s' % form.cleaned_data['name']
				
				raw_products = ''
				for offer in offers:
					raw_products += '%s,%s,%s\r' % (offer['id'],offer['count'],offer['price'])
				order.raw_products = raw_products

				if request.user.is_authenticated():
					order.user = request.user
				order.save()
				
				del request.session['basket']

				return render(request, 'pureprotein/basket_thanks.html', {
					'breadcrumbs': [('Корзина', '')],
					'order_id': order.id,
					'total_sum': total_sum,
				})
		else:
			form = ContactForm()

	return render(request, 'pureprotein/basket.html', {
		'breadcrumbs': [('Корзина', '')],
		'form': form,
		'offers': offers,
		'total_sum': total_sum,	
	})