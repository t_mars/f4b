#coding=utf

import os, re
from pcsv import *

class ProductMatcher:
	def tofloat(self, str):
		try:
			str = re.findall(r"[-+]?[\d| ]*\.\d+|[\d| ]+", str)[0]
			str = str.replace(" ", "")
			return float(str)
		except:
			return None

	def get_signs(self, p):
		# список ключевых слов
		keywords = p['name'].lower().split()
		
		# числа
		digits = set()

		# добавляем упаковку
		if 'packing' in p and p['packing']:
			packing = self.tofloat(p['packing'])
			if packing:
				digits.add(packing)
		
		# составляем список чисел
		for w in keywords:
			d = self.tofloat(w)
			if d:
				digits.add(d)
		
		# добавляем производителя
		if 'brand' in p and p['brand']:
			keywords += p['brand'].lower().split()
		
		# добавляем категорию
		if 'category' in p and p['category']:
			keywords += p['category'].lower().split()
		
		# слова
		words = set([n for n in keywords if len(n) > 2])
		
		return {'words': words, 'digits': digits}

	def selection(self, matches):
		if len(matches):
			mx = max(matches.values())
			return set([i for i,v in matches.items() if v == mx])
		else:
			return set()

	def match(self, product, products):
		# вычисляем признаки
		orig_signs = self.get_signs(product)
		products_signs = {}
		
		selected = set()
		for i,p in enumerate(products):
			products_signs[i] = self.get_signs(p)
			selected.add(i)
		
		# отбор по словам
		matches = {}
		for i in selected:
			signs = products_signs[i]
			words = orig_signs['words'].intersection(signs['words'])
			matches[i] = len(words)
			
		selected = self.selection({i:v for i,v in matches.items() if v > 1})

		# отбор по цифрам
		matches = {}
		for i in selected:
			signs = products_signs[i]
			for d1 in orig_signs['digits']:
				for d2 in signs['digits']:
					t = abs(d1-d2)/((d1+d2)/2)
					if t < 0.1:
						matches[i] = matches.get(i, 0) + 1
		
		selected = self.selection(matches)
		
		# отбор по цене
		matches = {}
		for i in selected:
			p1 = float(product['price'])
			p2 = float(products[i]['price'])
			t = (abs(p1-p2)/(max(p1,p2)))
			matches[i] = -t	 

		selected = self.selection(matches)
		
		print '----'		
		for k,v in product.items():
				print '%s: %s' % (k,v)
		print '===='		
		for i in selected:
			for k,v in products[i].items():
				print '%s: %s' % (k,v)
			print '----'

count = 0
products = {}
for path in os.listdir('out'):
	if path.endswith('products.csv'):
		ps = readCSV('out\\'+path)
		products[path] = ps
		count += len(ps)
		print path,len(ps)

print count
# product = products['5lb2_products.csv'][3012]
# #product = products['5lb2_products.csv'][1235]

# del(products['5lb2_products.csv'])

# matcher = ProductMatcher()

# for name,prods in products.items():
# 	print 'site', name
# 	matcher.match(product, prods)

