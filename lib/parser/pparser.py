#coding=utf8

from grab import Grab
import hashlib,re

class InfoParser():

	def __init__(self, selectors):
		self.selectors = selectors

	def pr_addstr(self, str, s):
		return s+str

	def pr_substr(self, str, ss, fs):
		if not len(ss):
			sp = 0
		else:
			sp = str.find(ss)
			if sp < 0:
				return None
			sp += len(ss)

		if not len(fs):
			fp = len(str)
		else:
			fp = str.find(fs, sp)
			if fp < 0:
				return None
		return str[sp:fp]

	def pr_search(self, str, n, f):
		res = str.find(n) >= 0
		if f == 'ne': res = not res
		return '1' if res else '0'

	def pr_price(self, s):
		r = re.findall('([1-9][(|,|.| )\d]*)', s)
		if not r:
			return None
		s = re.sub(ur'(\.|,)\s*(00|0000|\d{2})\s*(\D|$)', '', r[0], flags=re.I|re.U)
		# удаляем копейки если есть
		s = s.replace(" ", "")
		s = s.replace(",", "")
		s = s.replace(".", "")
		try:
			return float(s)
		except:
			return None
	
	def get_info(self, url):
		if len(self.selectors) == 0:
			raise Exception("To configurate selectors")
		
		try:
			grab = Grab()
			grab.go(url)
		except Exception as e:
			raise Exception('Error grab url "%s": %s' % (url, e))
		
		return self.parse_info(grab)

	def get_value(self, grab, selector):
		if selector['type'].startswith('default'):
			return selector['type'][8:]

		pathes = selector['path']
		if not isinstance(pathes, list):
			pathes = [pathes]
		
		elem = None
		msg = ''
		for path in pathes:
			try:
				elem = grab.doc.select(path)
				if len(elem):
					break
			except Exception as e:
				continue
		
		if elem == None:
			raise Exception('Error in selector %s' % msg)

		if selector['type'] == 'exist':
			return  '1' if len(elem) > 0 else '0'
		if selector['type'] == 'noexist':
			return  '0' if len(elem) > 0 else '1'

		if len(elem) == 0:
			raise Exception('Selector dont found')

		if len(elem) > 1 and 'multi' in selector and selector['multi'] == '1':
			return [self.parse_value(e, grab, selector) for e in elem]

		return self.parse_value(elem[0], grab, selector)

		
	def parse_value(self, elem, grab, selector):
		# удаляем пробелы лишние
		if selector['type'] == 'text':
			value = elem.text().strip()
		elif selector['type'] == 'html':
			value = elem.html()
		elif selector['type'].startswith('attr'):
			value = elem.attr(selector['type'][5:]).strip()
		
		if 'parser' in selector:
			parsers = selector['parser']
			if not isinstance(parsers, list):
				parsers = [parsers]
			# парсим
			for parser in parsers:
				ps = parser.split('@')
				if ps[0] == 'url':
					value = grab.make_url_absolute(value)
				else:
					attr = 'pr_'+ps[0]
					value = getattr(self, attr)(value, *ps[1:])
				if value == None:
					break;
				value = unicode(value)
		return value


	def parse_info(self, grab):
		info = dict() 
		counts = dict()
		for key,selector in self.selectors.items():
			try:
				if 'path' not in selector:
					raise Exception('path error')
				value = self.get_value(grab, selector)
				if value == None:
					raise Exception('value error')
			except Exception as e:
				if selector.get('required', '0') == '1':
					raise Exception('Error grab for key "%s": %s' % (key, str(e)))
				else:
					value = ''
			info[key] = value
			
			if isinstance(value, list):
				counts[key] = len(value)

		res = list()

		# разбиваем на несколько блоков данные
		if len(counts):
			# проверяем количества ответов
			if max(counts.values()) == min(counts.values()):
				for i in range(0, counts.values()[0]):
					item = dict(info)
					for key in counts.keys():
						item[key] = info[key][i]
					res.append(item) 
				
			else:
				print counts
				raise Exception("не совпадает количество значений")	
		else:
			res.append(info)

		return res

