#coding=utf8

import urlparse, sys, re, hashlib, os
from time import sleep
import logging, pickle

from grab.spider import Spider, Task
from url_filter import UrlFilter

class ScanSpider(Spider):
	
	def __init__(self, host, parser, proxy, walk, callback,  
			base_urls=[], excludes=[], includes=[], *args, **kwargs):
		super(ScanSpider, self).__init__(*args, **kwargs)

		self.parser = parser
		path = os.path.dirname(os.path.abspath(__file__))
		self.proxy_list_fn = os.path.join(path, 'proxy-list.conf')
		if proxy:
			self.use_proxy()
		
		self.walk = walk
		self.host = host

		self.ignore_urls = set()
		self.task_urls = set()
		self.passed_urls = set()
		self.initial_urls = []
		self.base_urls = set(base_urls)
		self.initial_urls.extend(list(base_urls))
		if walk:
			self.initial_urls.append(host)
		
		self.url_filter = UrlFilter(host, excludes, includes);
		# запрещаем переадресацию
		self.setup_grab(follow_location=False)
		self.debug = False
		self.callback=callback

	def use_proxy(self):
		print 'proxy on'
		self.load_proxylist(self.proxy_list_fn, 'text_file')
		return True
	
	# сохраняем статистику для пропуска не нужных станиц
	def dump_stat(self, path):
		stat = self.url_filter.dump_stat()
		pickle.dump(stat, open(path, "wb"))

	def load_stat(self, path):
		try:
			stat = pickle.load(open(path, "r"))
			self.url_filter.load_stat(stat)
			return True
		except:
			return False

	def prepare(self):
		self.row_errors = 0
		self.success = 0
		self.infos = dict()
		self.error_urls = dict()
		self.passed_urls = set()

	def _parse(self, grab, task):
		# ищем информацию на странице
		try:
			info = self.parser.parse_info(grab)
		except:
			info = None
		self.passed_urls.add(task.url)

		if info:
			if self.debug:
				logging.debug('info true')
			if self.callback(self.infos, info, task.url):
				self.url_filter.include(task.url)
				self.success += 1
				self.row_errors = 0
		else:
			if self.debug:
				logging.debug('info false')
			self.url_filter.exclude(task.url)

	def _walk(self, grab, task):
		# выборка ссылок
		link = grab.doc.select('//*[@href]')
		for s in link:
			url = s.attr('href')
			try:
				url = grab.make_url_absolute(url)
				self.create_task(url)	
			except:
				continue

	def task_initial(self, grab, task):
		if self.debug:
			logging.debug('---')
			logging.debug('total %d', self.success)
			logging.debug('errors %d', self.row_errors)
			logging.debug('http code %s', grab.response.code)
		
		# игнорируем
		if task.url in self.ignore_urls:
			if self.walk:
				self._walk(grab, task)
			return

		# от зацикливания проверяем 
		# только в случае полного сканировани
		if self.walk == True:
			if self.row_errors > 50:
				self.url_filter.exclude(task.url)
			if self.row_errors > 200:
				self.stop()
				return

		self.row_errors += 1
		
		if task.url in self.base_urls: 
			self._parse(grab, task)
		elif self.url_filter.is_valid(task.url, dbg=self.debug):
			if grab.response.code != 200:
				task = Task('initial', url=grab.config['url'], 
					task_try_count=task.task_try_count + 1, valid_status=task.valid_status)
				self.add_task(task)
			else:
				self._parse(grab, task)

		if self.walk:
			self._walk(grab, task)

	def create_task(self, url):
		if len(url) == 0:
			return

		if '#' in url:
			url = url.split('#')[0]
		if ';' in url:
			url = url.split(';')[0]

		# отбираем
		if url[-5:].lower() in ['.jpeg']:
			return
		if url[-4:].lower() in ['.jpg', '.css', '.png', '.mp3', '.avi', '.svg', '.pdf', '.ico']:
			return
		if url[-3:].lower() in ['.js']:
			return

		# преобразование и проверка на домен
		if not url.startswith(self.host):
			return

		if url in self.passed_urls:
			return

		if url in self.task_urls:
			return
		
		if not self.url_filter.is_valid(url):
			return 
	
		task = Task('initial', 
			url=url, 
			valid_status=(200, 403, 500)
		)
		self.task_urls.add(url)
		self.add_task(task)
