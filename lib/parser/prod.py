#coding=utf8

import logging, hashlib, os
from pparser import InfoParser
from spider import ScanSpider

class ProductParser():
	def calc_hash(self, info):
		m = hashlib.md5()
		for k in ['name', 'brand', 'packing']:
			m.update(info.get(k, '').encode('utf-8'))
		return m.hexdigest()

	def __init__(self, host, selectors, proxy=False, thread_number=3, excludes=[], dump_path=None):
		selectors['name']['required'] = '1'

		selectors['price']['required'] = '1'
		if 'parser' in selectors['price']:
			selectors['price']['parser'] = [selectors['price']['parser'], 'price']
		else:
			selectors['price']['parser'] = 'price'


		selectors['image']['required'] = '0'
		if 'parser' in selectors['image']:
			if isinstance(selectors['image']['parser'], list):
				selectors['image']['parser'].append('url')
			else:
				selectors['image']['parser'] = [selectors['image']['parser'], 'url']
		else:
			selectors['image']['parser'] = 'url'
		
		if 'available' not in selectors:
			selectors['available'] = {}
		selectors['available']['required'] = '0'
		
		if 'brand' not in selectors:
			selectors['brand'] = {}
		selectors['brand']['required'] = '0'
		
		if 'category' not in selectors:
			selectors['category'] = {}
		selectors['category']['required'] = '0'
		
		if 'packing' not in selectors:
			selectors['packing'] = {}
		selectors['packing']['required'] = '0'

		if 'description' not in selectors:
			selectors['description'] = {}
		selectors['description']['required'] = '0'

		self.excludes = excludes
		self.proxy = proxy
		self.thread_number = thread_number
		self.selectors = selectors
		self.dump_path = dump_path
		self.host = host

	def scan(self, base_urls, walk, reset=False):
		def callback(lst, infos, url):
			infos = self.pack(infos)
			for info in infos:
				hash = self.calc_hash(info)

				info['hash'] = hash
				info['url'] = url
				
				# исключаем повторения
				if hash not in lst or len(url) < len(lst[hash]['url']):
					lst[hash] = info
			return True

		parser = InfoParser(self.selectors)
		spider = ScanSpider(host=self.host, parser=parser, base_urls=base_urls, walk=walk,
			thread_number=self.thread_number, proxy=self.proxy, excludes=self.excludes,
			callback=callback)
		
		if self.dump_path:
			spider.load_stat(self.dump_path)
		
		spider.run()
		
		if self.dump_path and (not os.path.isfile(self.dump_path) or reset):
			spider.dump_stat(self.dump_path)

		infos = spider.infos
		urls = spider.passed_urls
		
		info_list = {}
		for hash,info in infos.iteritems():
			if info['url'] not in info_list:
				info_list[info['url']] = {}
			info_list[info['url']][hash] = info
		
		return info_list, urls
		
	def find(self, url):
		parser = InfoParser(self.selectors)
		info = parser.get_info(url)
		return self.pack(info)

	def pack(self, data):
		res = []
		for d in data:
			# на некоторых сайтах цена при отсутствии товара
			# if d['price'] == '':
			# 	# available или не определен или '0'
			# 	if d['available'] in ['', '0']:
			# 		d['price'] = None
			# 		d['available'] = False
			# 	# иначе ошибка
			# 	else:
			# 		continue
			# else:
			if d['price'] == None:
				continue
			d['price'] = float(d['price'])
			if d['available'] == '1':
				d['available'] = True
			elif d['available'] == '0':
				d['available'] = False
			else:
				d['available'] = None
			res.append(d)
		return res

	# pack = staticmethod(pack)