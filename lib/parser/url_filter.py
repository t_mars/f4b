from urlparse import urlparse
import re, logging

class UrlFilter:

	def __init__(self, host, exclude_patterns, include_patterns=[]):
		self.expatterns = []
		for ex in  exclude_patterns:
			self.expatterns.append(re.compile(ur'^%s$' % ex, flags=re.I|re.U))
		
		self.inpatterns = []
		for ex in  include_patterns:
			self.inpatterns.append(re.compile(ur'^%s$' % ex, flags=re.I|re.U))

		self.rexcludes = {}
		self.rincludes = {}
		self.excludes = {}
		self.includes = {}
		self.host = host

	def dump_stat(self):
		return {
			'rexcludes': self.rexcludes,
			'rincludes': self.rincludes,
			'excludes': self.excludes,
			'includes': self.includes,
		}

	def load_stat(self, stat):
		self.rexcludes = stat['rexcludes']
		self.rincludes = stat['rincludes']
		self.excludes = stat['excludes']
		self.includes = stat['includes']
	
	def get_path(self, url):
		ps = urlparse(url)
		res = []
		paths = ps[2].split('/')
		for path in paths:
			if not len(path):
				continue
			res.append([path])
		
		if len(ps[4]):
			params = ps[4]
			res.append([param.split('=')[0] for param in params.split('&')])
		
		return res

	def include(self, url):
		self.append(url, False)
	
	def exclude(self, url):
		self.append(url, True)

	def printt(self, root, ex, level=0):
		if ex:
			rr = self.excludes
		else:
			rr = self.includes
		
		for key in root.keys():
			try:
				l = rr[level][key]
			except:
				l = 0
			print '\t'*level,key,l
			self.printt(root[key],ex,level+1)

	def append(self, url, ex):
		if ex:
			root = self.rexcludes
		else:
			root = self.rincludes
		
		paths = self.get_path(url)
		for level,parts in enumerate(paths):
			path = ''.join(parts)
			if path not in root:
				root[path] = {}
			root = root[path]	
			for p in parts:
				self.inc_root(level, p, ex)
		
	def inc_root(self, level, path, ex):
		if ex:
			root = self.excludes
		else:
			root = self.includes
		
		if level not in root:
			root[level] = {}
		if path not in root[level]:
			root[level][path] = 0
		root[level][path]+=1
	
	def get_scores(self, paths, ex):
		if ex:
			root = self.excludes
		else:
			root = self.includes
		
		scores = []
		for level,parts in enumerate(paths):
			mx = 0
			for p in parts:
				try:
					mx = max(mx, root[level][p])
				except:
					pass
			scores.append(mx)
		return scores
	
	def get_score(self, paths, ex):
		scores = self.get_scores(paths, ex)
		if (len(scores)):
			return sum(scores) / float(len(scores))
		else:
			return 0

	def is_valid(self, url, dbg=False):
		if dbg:
			logging.debug(u'url %s' % url)
		
		ps = ''.join(urlparse(url)[2:4])
		
		if len(self.inpatterns):
			for inp in self.inpatterns:
				res = inp.findall(ps)
				if len(res):
					return True
			return False

		for ex in self.expatterns:
			res = ex.findall(ps)
			if len(res):
				return False
		
		paths = self.get_path(url)
		sc_ex = self.get_scores(paths, True)
		sc_in = self.get_scores(paths, False)

		scores = []
		for i in range(0,len(paths)):
			if sc_in[i] > 0:
				scores.append(1)
			elif sc_ex[i] >= 10:
				scores.append(0)
			else:
				scores.append(1)
				
		res = len(scores) == sum(scores)
		res = res and len(url) < 200 and len(paths) < 6
		if dbg:
			logging.debug(u'paths %s' % paths)
			logging.debug(u'excludes %s' % sc_ex)
			logging.debug(u'includes %s' % sc_in)
			logging.debug(u'scores %s' % scores)
			logging.debug(u'res %s' % res)
	    
		return res