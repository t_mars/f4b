#coding=utf8

from url_filter import UrlFilter
from pcsv import *
from urlparse import urlparse
import StringIO
import sys

fn = sys.argv[1]
dbg = len(sys.argv) == 3 and sys.argv[2] == 'd'

products_fn = 'out\\%s_products.csv' % fn
urls_fn = 'out\\%s_urls.csv' % fn

products = readCSV(products_fn)
product_urls = [p['url'] for p in products]

urls = []
with open(urls_fn, 'rb') as f:
	f = StringIO.StringIO(f.read()[3:])
	r = csv.reader(f)
	for row in r:
		urls.append(', '.join(row))

ps = urlparse(urls[0]+'?asd=asd&asw=wer/info')
baseurl = ps[0]+'//'+ps[1]
print 'site url:', baseurl
flt = UrlFilter(baseurl);
success_true = 0
success_false = 0
fail_true = 0
fail_false = 0
success = 0
fail = 0
for url in urls:
	res = flt.is_valid(url, dbg=dbg)
	flag = url in product_urls

	if dbg:
		print '==', res, flag, res==flag
		#raw_input()
	

	if flag:
		success +=1
		if res == flag:
			success_true += 1
		else:
			# flt.is_valid(url, dbg=True)
			# print 'false', url
			# raw_input()
			success_false += 1
		flt.include(url)
	else:
		fail +=1
		if res == flag:
			fail_true += 1
		else:
			# flt.is_valid(url, dbg=True)
			# print 'false', url
			# raw_input()
			fail_false += 1
		
		flt.exclude(url)
print '---'
flt.printt(flt.rincludes,False)
print '---'
flt.printt(flt.rexcludes,True)

print 'success \n\t%2.2f=%d\n\t%2.2f=%d' % (success_true/float(success), success_true, success_false/float(success), success_false)
print 'fail \n\t%2.2f=%d\n\t%2.2f=%d' % (fail_true/float(fail), fail_true, fail_false/float(fail), fail_false)