from django.http import (
    HttpResponseBadRequest,
    HttpResponseServerError,
    HttpResponseForbidden,
)
from django.shortcuts import render
from django_summernote.models import Attachment
from django_summernote.settings import summernote_config


def editor(request, id):
    return render(
        request,
        'django_summernote/widget_iframe_editor.html',
        dict({
            'id_src': id,
            'id': id.replace('-', '_'),
        })
    )

def resize_uploaded_image(file):
    from StringIO import StringIO
    from PIL import Image

    image = Image.open(file)

    maxSize = (910, 910)
    image.thumbnail(maxSize, Image.ANTIALIAS)
    resizedImage = image
    
    # Turn back into file-like object
    resizedImageFile = StringIO()
    resizedImage.save(resizedImageFile,'PNG',optimize=True)
    resizedImageFile.seek(0)    # So that the next read starts at the beginning

    from django.core.files.uploadedfile import InMemoryUploadedFile

    return InMemoryUploadedFile(resizedImageFile, None, 
        file.name, file.content_type,resizedImageFile.len, None)

def upload_attachment(request):
    if request.method != 'POST':
        return HttpResponseBadRequest('Only POST method is allowed')

    if summernote_config['attachment_require_authentication']:
        if not request.user.is_authenticated():
            return HttpResponseForbidden('Only authenticated users are allowed')

    if not request.FILES.getlist('files'):
        return HttpResponseBadRequest('No files were requested')

    try:
        attachments = []

        for file in request.FILES.getlist('files'):
            file = resize_uploaded_image(file)
            attachment = Attachment()
            attachment.file = file
            attachment.name = file.name
            if file.size > summernote_config['attachment_filesize_limit']:
                return HttpResponseBadRequest(
                    'File size exceeds the limit allowed and cannot be saved'
                )

            attachment.save()
            attachments.append(attachment)

        return render(request, 'django_summernote/upload_attachment.json', {
            'attachments': attachments,
        })
    except IOError:
        return HttpResponseServerError('Failed to save attachment')
