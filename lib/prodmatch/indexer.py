#coding=utf8
import re, collections, pickle, os
import operator, sys

from django.conf import settings

import config

class Indexer:
	def __init__(self):
		self.packing_norm = config.packing_norm
		self.measures = config.packing_measures
		self.rations = config.packing_rations
		self.brands = config.brands

		self.trans_chars = config.trans_chars

		# компилируем регулярки
		self.float_reg = '(\d+(((\.|,)\d+)?))'
		self.float_pattern = re.compile(self.float_reg)
		self.measure_patterns = {}
		for k in self.measures:
			self.measure_patterns[k] = \
				re.compile(ur'^\s*(%s)(x|\b)' % (self.measures[k]), flags=re.I|re.U)

		# название продуктов по брендам
		self.brand_words_tmp = os.path.join(settings.TMP_DIR, 'brand_words.tmp')
		try:
			self.brand_words = pickle.load(open(self.brand_words_tmp, "r"))
		except:
			self.brand_words = {}

	# распечатка информации 
	def print_info(self):
		for brand, words in self.brand_words.items():
			if not len(words):
				continue

			print 'brand:', brand.encode('utf8')
			words = sorted(words.items(), key=lambda i:len(i[1]), reverse=True)
			for w,ids in words:
				# if len(ids) < 2:
				# 	break
				print '\t',w.encode('utf8'),len(ids),', '.join([str(i) for i in ids])

	# обучаемся на сопоставлении названиям товаров - брендам
	def save_brand_words(self, id, brand, name):
		if not brand:
			return

		if brand not in self.brand_words:
			self.brand_words[brand] = {}
		
		for w in name:
			if w not in self.brand_words[brand]:
				self.brand_words[brand][w] = set()
			self.brand_words[brand][w].add(id)

	def get_index(self, name, brand, packing):
		orig = {
			'name': self.trans_chars(name), 
			'brand': self.trans_chars(brand),
			'packing': self.trans_chars(packing)
		}
		
		self.index = {}

		self.pull_packing(orig)
		self.pull_name(orig)
		self.pull_brand(orig)

		self.index['name'] = ' '.join(self.index['name'])
		self.index['name_digit'] = ' '.join(['%d' % d for d in self.index['name_digit']])
		self.index['packing'] = self.get_index_packing(self.index['packing'])
		return self.index
	
	def get_index_packing(self, packing):
		return ','.join(['%d %s' % (v,k) for k,v in packing.items()])

	def get_sign_packing(self, packing):
		pack = {}
		if len(packing):
			for s in packing.split(','):
				v,k = s.split(' ')
				pack[k] = int(v)
		
		return pack

	def get_signs(self, name, name_digit, brand, packing, **kwargs):
		pack = self.get_sign_packing(packing)
		return {
			'name': 		set([w for w in name.split(' ')]),
			'name_digit': 	set([d for d in name_digit.split(' ')]),
			'brand': 		brand,
			'packing': 		pack,
 		}

	def clear(self):
		self.brand_words = {}
	
	def save(self):
		pickle.dump(self.brand_words, open(self.brand_words_tmp, "wb"))

	def make_index(self, datas, status=False):
		indexes = {}

		# сначало индексируем товары с брендами
		indexed_ids = set()
		for id,(n,b,p) in datas.iteritems():
			# отбрасываем без брендов
			if not len(b):
				break
			
			if status:
				sys.stdout.write('1 index: id=%d %d/%d            \r' % (id,len(indexes),len(datas)))
				sys.stdout.flush()
			
			self.get_index(n,b,p)
			indexes[id] = self.index

			# обучаемся на сопоставлении названиям товаров - брендам
			self.save_brand_words(id, self.index['brand'], self.index['name'])

			indexed_ids.add(id)

		# затем индксируем без брендов
		for id in set(datas.keys())-indexed_ids:
			if status:
				sys.stdout.write('2 index: id=%d %d/%d            \r' % (id,len(indexes),len(datas)))
				sys.stdout.flush()

			n,b,p = datas[id]
			indexes[id] = self.get_index(n,b,p)

			# обучаемся на сопоставлении названиям товаров - брендам
			self.save_brand_words(id, self.index['brand'], self.index['name'])
		
		return indexes

	def clean_text(self, n):
		# убираем все в скобках
		n = re.sub(ur'\([^()]*\)', '', n)
		
		# удаляем лишние пробелы
		n = re.sub(ur'\s*-\s*', r'-', n)
		n = re.sub(ur'\s+', ' ', n)

		# удаляем знаки препинания в начале и в конце
		n = re.sub(ur'\s*\W+\s*$', '', n, flags=re.I|re.U)
		n = re.sub(ur'^\s*\W+\s*', '', n, flags=re.I|re.U)
		
		n = n.strip()
		return n

	def text_tokens(self, n, save_digit=True, split=True,lower=True):
		# убираем все в скобках
		n = re.sub(ur'\([^()]*\)', '', n)
		
		if save_digit:
			# удаляем лищние символы кроме чисел с точками
			n = re.sub(ur'(\d)(\.|,)(\d)', ur'\1E\3 ', n, flags=re.I|re.U)
			n = re.sub(ur'\W', ' ', n, flags=re.I|re.U)
			n = re.sub(ur'(\d)E(\d)', ur'\1.\2 ', n, flags=re.I|re.U)
		else:
			n = re.sub(self.float_reg, '', n)
			n = re.sub(ur'\W', ' ', n, flags=re.I|re.U)
		
		# соединияем отдельные буквы
		rs = re.findall(ur'(\s+[\w](\s+[\w])+\s+)', ' %s ' % n, flags=re.I|re.U)
		for r in rs:
			s = r[0].strip()
			n = n.replace(s, ''.join(s.split(' ')))
		
		# удаляем лишние пробелы
		n = re.sub(ur'\s+', ' ', n)
		n = n.strip()

		if lower:
			n = n.lower()
		if split:
			n = set(n.split())

		return n

	def pull_name(self, orig):
		# индексируем имя
		self.index['name_norm'] = self.clean_text(orig['name'])
		n = self.text_tokens(orig['name'], save_digit=False,split=False,lower=False)
		self.index['name'] = set(n.lower().split())

	def pull_brand(self, orig):
		brand = ''
		exact = False
		
		if len(orig['brand']):
			exact, brand = self.find_brand(orig['brand'])
			
		if not exact:
			exact2, brand2 = self.find_brand(orig['name'])
			if exact2:
				brand = brand2
				exact = exact2
			
		if exact:
			p,r = self.brands[brand]
			self.index['brand'] = brand
			self.index['name'] -= r
			self.index['name_norm'] = self.clean_text(p.sub('', self.index['name_norm']))
		else:
			self.index['brand'] = ''
		
		return exact

	def find_brand(self, str):
		tokens = self.text_tokens(str, split=False)
		
		is_exact = False
		name = tokens
		
		brand_vars = {}
		for n,(p,r) in self.brands.items():
			f = p.findall(tokens)
			if len(f) and len(f[0]):
				brand_vars[n] = f[0]

		# точное совпадение
		if len(brand_vars) == 1:
			is_exact = True
			name = brand_vars.keys()[0]
		
		# конфликт имен 
		elif len(brand_vars) > 1:
			# смотрим по статистике слов
			counts = {}
			for b in brand_vars.keys():
				if b not in self.brand_words:
					continue
				for w in self.index['name']:
					if w in self.brand_words[b]:
						counts[b] = counts.get(b,0)+1
			if len(counts):
				counts = sorted(counts.items(), key=operator.itemgetter(1), reverse=True)
				is_exact = True
				name = counts[0][0]
			else:
				brand_vars = sorted(brand_vars.items(), key=lambda x:len(x), reverse=True)
				name = brand_vars[0][0]

		return is_exact, name

	def to_float(self, str):
		return float(str.replace(',', '.'))
	
	def pull_packing(self, orig):
		packing = {}

		if len(orig['packing']):
			_, packing, _ = self.parse_packing(orig['packing'])
			
		orig['name'], npacking, ndigits = self.parse_packing(orig['name'])
		
		# часть информации об упаковке может быть в названии 
		# например количество баночек
		packing.update(npacking)
		
		self.index['name_digit'] = ndigits
		self.index['packing'] = packing
		
	def parse_packing(self, str):
		packing = {}
		digits = set()
		
		# ищем числа в строке
		res = str
		for m in self.float_pattern.finditer(str):
			digit = self.to_float(m.group())
			# ищем измерение
			pack = None
			substr = str[m.end():]
			
			for measure in self.measure_patterns:
				# ищем по регулярке
				pattern = self.measure_patterns[measure]
				r = pattern.findall(substr)
				if not len(r):
					continue

				# удаляем совпадение
				value = digit
				pack = r[0][0]
				
				# изменяем систему исчисления если нужно
				if measure in self.rations:
					ratio, measure = self.rations[measure]
					value *= ratio

				packing[measure] = value

				break

			if pack:
				# убираем подстроку с упаковкой
				res = re.sub(ur'%s\s*%s' % (m.group().strip(), re.escape(pack)), '', res)
			else:
				# добавляем к числам
				digits.add(digit)
		
		return res, packing, digits