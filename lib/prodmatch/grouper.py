#coding=utf8
import re, collections
import logging, os

from django.conf import settings

import config

def dict_add(d, k, v):
	if k not in d:
		d[k] = []
	d[k].append(v)

class Grouper:

	def __init__(self):
		self.name_replaces = config.name_replaces
		self.name_keywords = config.name_keywords

		self.packing_norm = config.packing_norm
		self.packing_rules = config.packing_rules
		self.packing_ignores = config.packing_ignores
		# не найденные правила
		self.no_found_rules = set()

		self.is_rus_pattern = re.compile(ur'^[а-я]+$', re.U)

		fn = os.path.join(settings.LOG_DIR, 'grouper.log')
		fh = logging.FileHandler(fn)
		self.logger = logging.getLogger('grouper')
		self.logger.addHandler(fh)

	def print_group(self, g):
		print 'n:', ', '.join(n for n in g['name']).encode('utf8')
		print 'd:', ', '.join('%d' % n for n in g['name_digit']).encode('utf8')
		print 'b:', ', '.join(n for n in g['brand']).encode('utf8')
		print 'p:', '; '.join(['%d %s' % (v, k) for k,v in g['packing'].items()])
		print 'i:', ', '.join(['%d' % id for id in g['ids']])
		print ''

	def print_groups(self, gs):
		ind = 0
		cc = 0
		cc2 = 0
		for g in gs:
			if len(g['ids']) > 0:
				print 'group %d (%d)' % (ind, len(g['ids']))
				self.print_group(g)
				cc += len(g['ids'])
				if len(g['ids']) == 1:
					cc2 += 1
			ind += 1
		print 'count=%d' % cc
		print 'sigles count=%d' % cc2

	def print_count(self, gs):
		cc = 0
		cc2 = 0
		for g in gs:
			cc += len(g['ids'])
			if len(g['ids']) == 1:
				cc2 += 1
		print 'count=%d' % cc
		print 'sigles count=%d' % cc2
		print

	def p_name(self, name):
		for a,b in self.name_replaces:
			if name >= a:
				name -= a
				name |= b

		return name

	def c_packing(self, a, b, debug=False):
		if a == b:
			return 1

		ak, bk = set(a.keys()), set(b.keys())
		
		# проверяем на игнорируемые
		for l,r in self.packing_ignores:
			if l == ak and r == bk:
				return False
			if r == ak and l == bk:
				return False

		# подбираем правило, выбираем значения
		rule = None
		vals = []
		count = 0
		acount = 0
		bcount = 0
		ret = 0
		for r in self.packing_rules:
			cc = 0
			ac = 0
			bc = 0
			v = []
			# смотрим по мерам
			for p in r:
				am = list(ak & p['ms'])
				bm = list(bk & p['ms'])
				
				am = am[0] if len(am) else None
				bm = bm[0] if len(bm) else None

				ac += 1 if am else 0
				bc += 1 if bm else 0

				# не найдены меры - правило не подходит
				if not am and not bm:
					break

				if p.get('null', 0):
					default = a.get(am) or b.get(bm)
				elif p.get('def', None):
					default = a.get(am) or b.get(bm)
				else:
					default = None

				if am and bm:
					ret = 0
					cc += 1
					ret = 1
				else:
					ret = 0.5
				
				av = a.get(am, default)
				bv = b.get(bm, default)

				if not av or not bv:
					break

				v.append((av,bv))
				
			if len(v) == len(r) and r:
				rule = r
				vals = v
				count = cc
				acount = ac
				bcount = bc
		
		if not rule:
			aj = ','.join(ak) + ';'
			bj = ','.join(bk) + ';'
			if aj+bj not in self.no_found_rules and\
				bj+aj not in self.no_found_rules:
				self.no_found_rules.add(aj+bj)
				self.logger.debug('Rule not found. %s, %s' % (a, b))
			return 0
		if debug:
			print rule
		if acount == 0 or bcount == 0:
			return 0

		# сравнение
		res = 1
		if not count:
			res = 0.5

		for i,p in enumerate(rule):		
			av, bv = vals[i]
			if av == bv:
				continue

			devation = p.get('dev', 0)
			mv = max(av, bv) * (devation / 100.0)
			if abs(av-bv) > mv:
				res = 0
				break

		return res

	def in_group(self, s, g):
		if self.p_name(s['name']) != self.p_name(g['name']):
			return False

		if s['name_digit'] != g['name_digit']:
			return False

		if s['brand'] != g['brand']:
			return False

		if self.c_packing(s['packing'], g['packing']) != 1:
			return False

		return True

	def create_group(self, id, s):
		return {
			'name': s['name'],
			'name_digit': s['name_digit'],
			'brand': s['brand'],
			'packing': s['packing'],
			'fid': id,
			'ids': set([id]),
		}

	def get_prod_vals(self, g, prods):
		p = prods[g['fid']]
		packing = ['%d %s' % (v, self.packing_norm[m]) for m,v in g['packing'].items()]
		return {
			'name': p.name_norm,
			'brand': p.brand_norm,
			'packing': ', '.join(packing),
		}

	def diff_groups(self, s, g, strict=True,packing=True,left_prioritet=False):
		# 1.
		# пустой бренд - любой бренд
		if len(s['brand']) and s['brand'] != g['brand']:
			return -1

		# 2.
		sd = s['name_digit']
		gd = g['name_digit']
		# проверка одно включает другое
		if strict and not (sd <= gd or gd <= sd):
			return -2
		diff_d = len(gd - sd) / 2.0
		
		# 3.
		if packing:
			if self.c_packing(s['packing'], g['packing']) == 0:
				# в нестрогом режиме пустая упаковка - любая упаковка
				if strict or (len(s['packing']) and len(g['packing'])):
					return -3

		# 4.
		sn = self.p_name(s['name'])
		gn = self.p_name(g['name'])
		# должно быть пересечение по словам
		if len(sn & gn) == 0:
			return -4

		# обязательные слова должны 
		# присутствовать в обоих названиях
		for kw in self.name_keywords:
			if (kw <= sn) != (kw <= gn):
				return -5

		# проверяем наличие всех слов в левом объекте
		if left_prioritet and not sn <= gn:
			return -6
		
		diff_n = 0
		nn = sn ^ gn
		for w in nn:
			# русские слова меньше оцениваются
			if self.is_rus_pattern.match(w):
				diff_n += 0.5
			else:
				diff_n += 1

		return diff_d + diff_n

	def assort(self, shps):
		brand_heaps = self.assort_by_brand(shps, shps.keys())
		
		# с пустым брендом в отдельную кучу
		any_brand_heap = brand_heaps['']
		del brand_heaps['']

		for brand,heap in brand_heaps.iteritems():
			word_heaps, name_heaps = self.assort_by_name(shps, heap+any_brand_heap)

	# 1. разиваем по брендам
	def assort_by_brand(self, shps, ids):
		heaps = {}
		for id in ids:
			s = shps[id]
			dict_add(heaps, s['brand'], id)
		
		return heaps

	# 2. разбиваем по названиям
	def assort_by_name(self, shps, ids):
		word_heaps = {}
		digit_heaps = {}
		for id in ids:
			s = shps[id]
			for w in self.p_name(s['name']):
				dict_add(word_heaps, w, id)
			for d in s['name_digit']:
				dict_add(digit_heaps, d, id)
		
		return word_heaps,digit_heaps

	def group(self, shps, debug=False, inter=False):
		groups = []

		# 1
		print 'step 1'
		for id,s in shps.iteritems():
			print 'prod ',id,
			group = None
			for g in  groups:
				if self.in_group(s, g):
					group = g
					break
					
			if group:
				group['ids'].add(id)
				group['packing'].update(s['packing'])
				print ' added %d' % len(group['ids'])
			else:
				groups.append(self.create_group(id, s))
				print ' group created %d' % len(groups)

		if debug:
			print 'step 1'
			self.print_count(groups)

		# 2
		print 'step 2'
		for si,s in enumerate(groups):
			if len(s['ids']) != 1:
				continue
			group = None
			diff = 999
				
			for gi,g in enumerate(groups):
				if gi == si:
					continue
				# нельзя добавлять в группу без бренда
				if not len(g['brand']):
					continue
				d = self.diff_groups(s, g)
				if d >= 0 and d < diff:
					group = g
					diff = d
			
			if group:
				print 'group %d merge to %d' % (si, gi)
				if inter:
					print 'a'
					self.print_group(s)
					print 'b'
					self.print_group(group)
					print 'diff=', diff
					raw_input()
				
				group['packing'].update(s['packing'])
				group['ids'].update(s['ids'])
				s['ids'] = set()

		# s = groups[1]
		# g = groups[3]
		# d = self.diff_groups(s, g)

		# print '<pre>'
		# print 's'
		# self.print_group(s)
		# print 'g'
		# self.print_group(g)
		# print 'd', d
		# print '</pre>'

		if debug:
			print 'step 2'
			self.print_groups(groups)

		return groups