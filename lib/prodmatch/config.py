#coding=utf8
import re, os
import params

from wiki.models import Brand

# замена русских символов на латинский на случай опечатки
def trans_chars(s):
	for k,v in params.trans_chars.iteritems():
		s = s.replace(k, v)
	return s

# упаковки
packing_measures = {k:trans_chars(v) for k,v in params.packing_measures.iteritems()}
packing_norm = params.packing_norm

# преобразование мер
packing_rations = params.packing_rations

# бренды
brands = {} # brands[$name] = ($pattern, $removes)
for brand in Brand.objects.all():
	name = brand.name
	pats = trans_chars(brand.name_keywords).split(',')

	keywords = '|'.join(pats)
	pattern = re.compile(ur'\b(%s)\b' % keywords, flags=re.I|re.U)

	removes = set()
	for ws in pats:
		for w in ws.split():
			removes.add(w)
	brands[name] = (pattern, removes)

# правила сравнения упаковок
packing_rules = []
for rule_str in params.packing_rules:
	rule = []
	for rs in rule_str.split(';'):
		r = {}
		
		sp = rs.split(':')
		if len(sp) != 2:
			sp = sp[0], ''

		ms, ps = sp
		r['ms'] = set(ms.split('|'))
		for p in ps.split(','):
			if not len(p):
				continue
			k,v = p.split('=')
			r[k] = float(v)
		rule.append(r)

	packing_rules.append(rule)

# игнориремые сочетания упаковок
packing_ignores = []
for ignore in params.packing_ignores:
	a,b = ignore.split(';')
	packing_ignores.append( (set(a.split(',')),set(b.split(','))) )

# замена в названиях
name_replaces = []
for a,b in params.name_replaces.iteritems():
	a = set([p for p in trans_chars(a).split(' ')])
	b = set([p for p in trans_chars(b).split(' ') if len(p)])
	name_replaces.append( (a, b) )

name_keywords = [set(trans_chars(a).split(' ')) for a in params.name_keywords]
