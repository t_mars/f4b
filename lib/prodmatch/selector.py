#coding=utf8

import re

from fuzzywuzzy import fuzz
import jellyfish

class Selector:
	def is_exact(self, p1, p2):
		# слова в названии
		wname = fuzz.token_set_ratio(' '.join(p1['name']), ' '.join(p2['name']))
		if wname < 80:
			return -1

		# слова в производителе
		wbrand = fuzz.token_set_ratio(' '.join(p1['brand']), ' '.join(p2['brand']))
		if wbrand < 80:
			return -2

		# числа в названии
		sim = len(p1['name_digit'] & p2['name_digit'])
		count = float(len(p1['name_digit'] | p2['name_digit']))
		dname = (sim/count) if count > 0 else 0
		if dname == 0 and len(p1['name_digit']) != 0 and len(p2['name_digit']) != 0:
			return -3
		
		# числа в упаковке
		sim = 0
		count = float(len(p1['packing_digit'] | p2['packing_digit']))
		for d1 in p1['packing_digit']:
			for d2 in p2['packing_digit']:
				t = abs(d1-d2)/((d1+d2)/2)
				if t < 0.1:
					sim += (1-t)*(1-t)
		dpacking = (sim/count) if count > 0 else 0
		if dpacking == 0:
			return -4

		return 1

	def get_sim(self, p1, p2):
		sb = len(p1['brand'] & p2['brand'])
		pb = float(len(p1['brand'] | p2['brand']))
		
		
		sn = len(p1['name'] & p2['name'])
		pn = float(len(p1['name'] | p2['name']))
		
		sd = 0
		pd = float(len(p1['packing_digit'] | p2['packing_digit']))
		for d1 in p1['packing_digit']:
			for d2 in p2['packing_digit']:
				t = abs(d1-d2)/((d1+d2)/2)
				if t < 0.1:
					sd += (1-t)*(1-t)
		res = 0
		if pn > 0: res += 0.4 * (sn/pn)
		if pb > 0: res += 0.3 * (sb/pb)
		if pd > 0: res += 0.3 * (sd/pd)
		return res

	def ordering(self, matches, reverse=False):
		if len(matches):
			import operator
			return [k for k,v in sorted(matches.items(), key=operator.itemgetter(1), reverse=reverse)]
		else:
			return []

	def matching(self, orig_signs, products_signs, min_sim = 0.45, min_count=10):
		selected = []
		for i in products_signs.keys():
			selected.append(i)
		
		# отбор
		matches = {}
		for i in selected:
			matches[i] = self.get_sim(orig_signs, products_signs[i])

		selected = self.ordering(matches,reverse=True)
		
		res = []
		for ind in selected:
			if matches[ind] < min_sim and len(res) >= min_count:
				break
			res.append(ind)
		return res