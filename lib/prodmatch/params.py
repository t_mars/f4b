#coding=utf8

# замена русских символов на латинский на случай опечатки
trans_chars = {
	u'а': 'a',
	u'А': 'A',
	u'В': 'B',
	u'с': 'c',
	u'С': 'C',
	u'е': 'e',
	u'Е': 'E',
	u'Н': 'H',
	u'К': 'K',
	u'М': 'M',
	u'о': 'o',
	u'О': 'O',
	u'р': 'p',
	u'Р': 'P',
	u'Т': 'T',
	u'Х': 'X',
	u'у': 'y',
}
# упаковки
packing_measures = {
	#u'mg': u'мг|mg',
	u'gr': u'г(р(амм)?)?|g(r(am(m|s)?)?)?',
		u'lb': u'lb(s)?',
		u'kg': u'к(г)?|кило(грамм)?|k(g)?',    
		u'oz': u'oz(\.)?',    
	u'servings': u'п(орц(и(и|й))?)?|ш(т(ук)?)?|(\+\s*)?serv(ing(s)?)?|x|\*', # 24+ порций, 24x20ml, 24*20ml
		u'bars': u'bar(s)?',
	u'packs': u'па(кет(ик(а|ов)?|ов)|чек|к)|pa(ck(s)?|k(s)?)|pks',
	u'amp': u'амп(ул)?|флак(онов)?|amp',
		u'bottles': u'bottles|бут(ыл(ок|ка)?)?',
	u'tab': u'таб(л(еток)?)?|tab(s|l(et(s)?)?)?',
		u'cap': u'([а-я.]+ ?)?кап(с(ул)?|л(ет)?)?|([a-z.]+ ?)?cap(s(ul(es)?)?|let(s)?)?|softgel(s)?',
	u'ml': u'ml|мл',
		u'l': u'l|л(итр(ы)?)?',
		u'floz': 'fl(\.)?\s*oz(\.)?',
}

packing_norm = {
	u'gr': u'гр',
	u'servings': u'порц',
	u'packs': u'пак',
	u'amp': u'бут',
	u'tab': u'таб',
	u'ml': u'мл',
}

# преобразование мер
packing_rations = {
	u'lb': (453.592, u'gr'),
	u'kg': (1000, u'gr'),
	u'oz': (28.349, u'gr'),
	u'l': (1000, u'ml'),
	u'floz': (29.573, u'ml'),
	u'cap': (1, u'tab'),
	u'bottles': (1, u'amp'),
	u'bars': (1, u'servings'),
}

# правила сравнения упаковок
packing_rules = [
	'tab',
	'tab;servings|packs',
	'packs|servings|amp',
	'gr:dev=10',
	'gr:dev=10;packs|servings:def=1',
	'ml:dev=10',
	'ml:dev=10;servings|amp:def=1',
]

# игнориремые сочетания упаковок
packing_ignores = [
	'ml;gr',
	'gr;servings',
	'gr;servings,packs',
	'gr;amp',
	'gr;tab',
	'gr;tab',
	'gr,servings;amp',
	'servings;amp',
	'servings,packs;amp',
]

# замена в названиях
name_replaces = {
	u'coenzyme q': u'coq',
		u'coenzyme': u'coq',
		u'co q': u'coq',
u'caffeine free': u'cf',
		u'без кофеина': u'cf',
	u'carnitine': u'carnitin',
		u'карнитин': u'carnitin',
		u'л карнитин': u'carnitin',
	u'caseine': u'casein',
		u'казеин': u'casein',
		u'казеиновый': u'casein',
	u'glutamine': u'glutamin', 
		u'глютамин': u'glutamin',
	u'creatine': u'creatin',
		u'креатин': u'creatin',
	u'соевый': u'soy',
	
	u'гр': u'',
	u's': u'',
	u'l': u'',
	u'mcg': u'',
	u'мкг': u'',
	u'мг': u'',
	u'mg': u'',
	u'capsules': u'',
	u'caps': u'',
}
name_keywords = [
	u'cf',
	u'c', u'b', u'e', # vitamin
	u'nt', # no xplode
	u'creatin', 
	u'glutamin', 
	u'carnitin', 
	u'casein', 
	u'soy',
	u'egg', 
	u'whey', 
	u'bcaa',
	u'amino',
]