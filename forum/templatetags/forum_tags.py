from django import template

register = template.Library()

@register.simple_tag(takes_context=True)
def topics(context):
	from forum.models import Topic

	context['topics'] = Topic.objects.filter(level=0)
	
	return ''