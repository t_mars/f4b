from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'forum.views.home', name='forum_home'),
    
    url(r'^ajax/prev_messages/$', 'forum.views.ajax_prev_messages', name='ajax_prev_messages'),
    url(ur'^admin/message/(?P<id>\d+)/delete/$', 'forum.views.message_delete', name='admin_message_delete'),
    url(r'^topic/(?P<id>\d+)/$', 'forum.views.topic', {'slug': ''}),
    url(ur'^topic/(?P<id>\d+)/(?P<slug>[\w\s-]+)/$', 'forum.views.topic', name='forum_topic'),
    
    url(ur'^thread/(?P<id>\d+)/(?P<slug>[\w\s-]+)/$', 'forum.views.thread', name='forum_thread'),
    url(ur'^topic/(?P<id>\d+)/thread/create/$', 'forum.views.create_thread', name='forum_create_thread'),
)
