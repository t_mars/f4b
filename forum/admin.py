from django.contrib import admin
from forum.models import *

admin.site.register(Message)
admin.site.register(Topic)