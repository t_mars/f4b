#coding=utf8
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django import forms

from forum.models import *
from blog.views.helpers import user_access

def home(request):
	topics = Topic.objects.filter(level=0)
	
	return render(request, 'forum/subtopics.html', {
		'breadcrumbs': [('Форум', '')],
		'topics': topics,
		'title': 'Форум',
	})

def topic(request, id, slug):
	try:
		topic = Topic.objects.get(id=id)
	except:
		raise Http404
	
	if slug != topic.slug:
		url = reverse('forum_topic', args=[id,topic.slug])
		return HttpResponseRedirect(url)

	breadcrumbs = [('Форум', reverse('forum_home'))]
	for t in topic.get_ancestors():
		breadcrumbs.append( 
			(t.title, reverse('forum_topic', args=[t.id,t.slug])) 
		)
	breadcrumbs.append( (topic.title, '') )

	if topic.is_leaf_node():
		threads = Thread.objects\
			.filter(topic=topic)\
			.order_by('-created_at')
		
		paginator = Paginator(threads, 20)
		page = request.GET.get('page', 1)
		try:
			threads = paginator.page(page)
		except EmptyPage:
			threads = paginator.page(paginator.num_pages)

		return render(request, 'forum/topic.html', {
			'breadcrumbs': breadcrumbs,
			'topic': topic,
			'threads': threads,
		})

	return render(request, 'forum/subtopics.html', {
		'breadcrumbs': breadcrumbs,
		'title': topic.title,
		'topics': topic.children.all(),
	}) 
	
def thread(request, id, slug):
	try:
		thread = Thread.objects.get(id=id)
	except:
		raise Http404
	
	if slug != thread.slug:
		url = reverse('forum_thread', args=[id,thread.slug])
		return HttpResponseRedirect(url)
	config = {
		'iframe': False,
		'airMode': True,
		'cleanPaste': True,
		'letterCount': True,
		'styleWithTags': True,
		'direction': 'ltr',
		'width': '100%',
		'height': '300',
		'lang': 'ru',
		'toolbar': [
			['para', ['ul', 'ol']],
			['insert', ['link', 'picture', 'video']],
		],
		'attachment_require_authentication': True,
	}
	
	from lib.django_summernote.widgets import SummernoteWidget
	class MessageForm(forms.Form):
		content = forms.CharField(widget=SummernoteWidget(config=config))
		reply_to = forms.IntegerField(required=False,widget=forms.HiddenInput())
		
		def cleaned_content(self, content):
			from lxml.html.clean import Cleaner

			cleaner = Cleaner()
			cleaner.javascript = True # This is True because we want to activate the javascript filter
			cleaner.style = True      # This is True because we want to activate the styles & stylesheet filter
			cleaner.embedded = False # iframe save
			cleaner.kill_tags = ['table']
			return cleaner.clean_html(content)

		def clean(self):
			cleaned_data = super(MessageForm, self).clean()
			
			if cleaned_data.get('reply_to'):
				try:
					message = Message.objects.get(pk=cleaned_data['reply_to'])
					cleaned_data['reply_to'] = message
					cleaned_data['root'] = message.root or message
				except:
					self.add_error('reply_to', u'ошибка #1')

			return cleaned_data

	form = None
	if request.user.is_authenticated():
		if request.method == 'POST':
			form = MessageForm(request.POST)
			if form.is_valid():
				
				message = Message()
				message.thread = thread
				message.user = request.user

				message.content = form.cleaned_data['content']
				message.root = form.cleaned_data.get('root')
				
				# не возможно ответить на свое сообщение
				reply_to = form.cleaned_data.get('reply_to')
				if reply_to and reply_to.user_id != message.user_id:
					message.reply_to = reply_to
					return

				message.save()

				from blog.models import Notification
				Notification().forum_message_answer(message)

				url = reverse('forum_thread', args=[thread.id,thread.slug])
				return HttpResponseRedirect('%s#%s' % (url, message.id))
		else:
			form = MessageForm()

	messages = Message.objects\
		.filter(thread=thread,reply_to=None)
	
	paginator = Paginator(messages, 10)
	page = request.GET.get('page', paginator.num_pages)
	try:
		messages = paginator.page(page)
	except EmptyPage:
		messages = paginator.page(paginator.num_pages)

	breadcrumbs = [('Форум', reverse('forum_home'))]
	for topic in thread.topic.get_ancestors(include_self=True):
		breadcrumbs.append( 
			(topic.title, reverse('forum_topic', args=[topic.id,topic.slug])) 
		)
	breadcrumbs.append( (thread.title, '') )

	return render(request, 'forum/thread.html', {
		'breadcrumbs': breadcrumbs,
		'messages': messages,
		'thread': thread,	
		'form': form,	
	})
	
@user_access
def create_thread(request, id):
	try:
		topic = Topic.objects.get(id=id)
	except:
		raise Http404
		
	class ThreadForm(forms.Form):
		title = forms.CharField(min_length=5,max_length=150)
		
	form = ThreadForm()

	if request.method == 'POST':
		form = ThreadForm(request.POST)
		if form.is_valid():
			thread = Thread()
			thread.title = form.cleaned_data['title']
			thread.topic = topic
			thread.user = request.user
			thread.save()

			url = reverse('forum_thread', args=[thread.id,thread.slug])

			return HttpResponseRedirect(url)
	else:
		form = ThreadForm()
	
	breadcrumbs = [('Форум', reverse('forum_home'))]
	for t in topic.get_ancestors(include_self=True):
		breadcrumbs.append( 
			(t.title, reverse('forum_topic', args=[t.id,t.slug])) 
		)
	breadcrumbs.append( ('Создать тему', '') )

	return render(request, 'forum/create_thread.html', {
		'topic': topic,
		'form': form,	
		'breadcrumbs': breadcrumbs,	
	})

def ajax_prev_messages(request):
	message = Message.objects\
		.get(pk=request.GET.get('message_id'))
	messages = Message.objects\
		.filter(root__id=request.GET.get('message_id'))\
		.order_by('-created_at')
	
	from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
	
	paginator = Paginator(messages, Message.per_page)
	page = request.GET.get('page')
	try:
		messages = paginator.page(page)
	except:
		messages = paginator.page(1)
	messages.object_list = messages.object_list[::-1]
	messages.end_index = min(messages.end_index()+Message.per_page, messages.paginator.count)
	# raise Exception(dir(messages), dir(messages.paginator))
	return render(request, 'forum/messages.html', {
		'message': message,
		'messages': messages,
	})

@user_access
def message_delete(request, id):
	if not request.user.is_superuser:
		raise Http404
	
	try: 
		message = Message.objects.get(id=id)
		message.delete()
	except Exception as e:
		raise e
	
	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))