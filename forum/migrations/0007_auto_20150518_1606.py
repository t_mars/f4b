# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0006_auto_20150513_1512'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='root',
            field=models.ForeignKey(related_name='root_reverse', default=None, to='forum.Message', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='reply_to',
            field=models.ForeignKey(related_name='reply_to_reverse', default=None, to='forum.Message', null=True),
            preserve_default=True,
        ),
    ]
