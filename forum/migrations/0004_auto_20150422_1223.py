# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0003_auto_20150422_1115'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='thread',
            name='level',
        ),
        migrations.RemoveField(
            model_name='thread',
            name='lft',
        ),
        migrations.RemoveField(
            model_name='thread',
            name='rght',
        ),
        migrations.RemoveField(
            model_name='thread',
            name='tree_id',
        ),
    ]
