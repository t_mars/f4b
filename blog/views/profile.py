#coding=utf8
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.shortcuts import render, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect, Http404 
from django import forms
from django.core.files.images import get_image_dimensions
		
from blog.models import *
from helpers import *

import re

class LoginForm(forms.Form):
	login = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput())

	def __init__(self, request, *args, **kwargs):
		self.ip_key = 'login_%s' % get_client_ip(request)
		super(LoginForm, self).__init__(*args, **kwargs)
		
		from django.core.cache import cache
		if cache.get(self.ip_key, 0) >= 5:
			from captcha.fields import CaptchaField
			self.fields['captcha'] = CaptchaField()

	def inc_counter(self):
		from django.core.cache import cache
		cache.set(self.ip_key, 
			cache.get(self.ip_key, 0)+1, timeout=None)
			
		
	def clear_counter(self):
		from django.core.cache import cache
		cache.delete(self.ip_key)

class RegisterForm(forms.Form):
	MIN_LENGTH = 8

	login = forms.CharField()
	email = forms.EmailField()
	password1 = forms.CharField(widget=forms.PasswordInput(),min_length=8)
	password2 = forms.CharField(widget=forms.PasswordInput(),min_length=8)

	def clean(self):
		cleaned_data = super(RegisterForm, self).clean()
		
		if self.is_valid():
			
			if len(cleaned_data['password1']) < self.MIN_LENGTH:
				raise forms.ValidationError("Пароль должен быть не меньше %d символов." % self.MIN_LENGTH)

			first_isalpha = cleaned_data['password1'][0].isalpha()
			if all(c.isalpha() == first_isalpha for c in cleaned_data['password1']):
				self.add_error('password1', "Пароль должен содержать, как минимум одну букву и хотя бы одну цифру или знак препинания.")
			elif not re.match("^[A-Za-z0-9]*$", cleaned_data['password1']):
				self.add_error('password1', "Пароль должен состоять только из латинских букв и цифр.")

			else:
				if cleaned_data['password1'] != cleaned_data['password2']:
					self.add_error('password2', u'Пароли не совпадают')

				try:
					User.objects.get(email=cleaned_data['email']) 
					self.add_error('email', u'Пользователь с такой почтой уже зарегистрирован')
				except:
					pass

				try:
					User.objects.get(username=cleaned_data['login']) 
					self.add_error('login', u'Пользователь с таким логином уже зарегистрирован')
				except:
					pass

		return cleaned_data

# перехват ошибки авторизации
def social_auth(request, backend, *args, **kwargs):
	from lib.social_auth.views import complete
	
	try:
		return complete(request, backend, *args, **kwargs)
	except Exception as e:
		cls = e.__class__.__name__
		if cls == 'AuthAlreadyAssociated':
			request.session['profile_edit_message'] = u'Этот аккаунт уже прикреплен к пользователю.'
			return HttpResponseRedirect(reverse('profile_edit'))
		else:
			raise Http404

# отпрвка кода подтверждения
def send_confirm(request):
	if not request.user.is_authenticated():
		raise Http404

	user_profile = UserProfile.by_user(request.user)

	if not user_profile.confirm_key or len(user_profile.confirm_key) != 48:
		import os
		user_profile.confirm_key = os.urandom(24).encode('hex')
	user_profile.save()
	
	confirm_url = reverse('profile_confirm', args=[user_profile.confirm_key])
	subject, frm, to = 'Подтверждение почты', 'fuel4bull@gmail.com', request.user.email
	html_content = 'Для подтверждения перейдите по <a href="http://blog.fuel4bull.ru%s">ссылке</a>' % confirm_url
	
	from django.core.mail import EmailMultiAlternatives, send_mail
	msg = EmailMultiAlternatives(subject, '', frm, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()

	request.session['profile_edit_message'] = u'Вам на почту %s выслано письмо для подтверждения.' % request.user.email

	return HttpResponseRedirect(reverse('profile_edit'))

# подтверждения почты
def confirm(request, key):
	if not request.user.is_authenticated():
		raise Http404

	user_profile = UserProfile.by_user(request.user)
	
	if key != user_profile.confirm_key:
		raise Http404
	
	user_profile.is_active = True
	request.user.save()

	user_profile.confirm_key = ''
	user_profile.save()
	
	request.session['profile_edit_message'] = u'Ваша почта %s подтверждена.' % user_profile.user.email

	return HttpResponseRedirect(reverse('profile_edit'))

# страница после авторизации social_auth
@user_access
def logged_in(request):
	return HttpResponseRedirect(reverse('home'))

def login(request):
	if request.user.is_authenticated():
		raise Http404

	if request.method == 'POST':
		form = LoginForm(request,request.POST)
		if form.is_valid():
			form.inc_counter()

			from django.contrib.auth import authenticate
			from django.contrib import auth
			user = auth.authenticate(
				username=form.cleaned_data['login'], 
				password=form.cleaned_data['password']
			)
			if user:
				auth.login(request, user)
				
				form.clear_counter()

				# если не активный переадресуем на редактирование профиля
				user_profile = UserProfile.by_user(request.user)
				if not user_profile.is_active:
					return HttpResponseRedirect(reverse('profile_edit'))

				return HttpResponseRedirect(reverse('home'))
			else:
				form.add_error('login', 'Пользователь не найден')
	else:
		form = LoginForm(request)

	return render(request, 'blog/profile/login_page.html', {
		'breadcrumbs': [('Вход', '')],
		'form': form,
	})

def register(request):
	if request.user.is_authenticated():
		raise Http404

	if request.method == 'POST':
		form = RegisterForm(request.POST)
		if form.is_valid():
			from django.contrib.auth import authenticate
			from django.contrib import auth
			user = User.objects.create_user(
				form.cleaned_data['login'], 
				form.cleaned_data['email'], 
				form.cleaned_data['password1']
			)
			user = auth.authenticate(
				username=form.cleaned_data['login'], 
				password=form.cleaned_data['password1']
			)
			if user:
				auth.login(request, user)
				return HttpResponseRedirect(reverse('profile_edit'))
	else:
		form = RegisterForm()

	return render(request, 'blog/profile/register_page.html', {
		'breadcrumbs': [('Регистрация', '')],
		'form': form,
	})

def logout(request):
	from django.contrib.auth import logout
	logout(request)

	return HttpResponseRedirect(reverse('home'))

def edit(request):

	if not request.user.is_authenticated():
		raise Http404

	class ProfileForm(forms.Form):
		avatar = forms.ImageField(required=False)
		username = forms.CharField()
		email = forms.EmailField()
		first_name = forms.CharField()
		last_name = forms.CharField()
		about = forms.CharField(widget=forms.Textarea(),required=False)
		about = forms.CharField(widget=forms.Textarea(),required=False)

		def clean_avatar(self):
			avatar = self.cleaned_data['avatar']
			if not avatar:
				return False

			try:
				w, h = get_image_dimensions(avatar)

				#validate dimensions
				# max_width = max_height = 100
				# if w > max_width or h > max_height:
				# 	raise forms.ValidationError(
				# 		u'Please use an image that is '
				# 		 '%s x %s pixels or smaller.' % (max_width, max_height))

				#validate content type
				main, sub = avatar.content_type.split('/')
				if not (main == 'image' and sub in ['jpeg', 'pjpeg', 'png']):
					raise forms.ValidationError(u'Допустимы форматы a JPEG или PNG.')

				#validate file size
				# if len(avatar) > (20 * 1024):
				# 	raise forms.ValidationError(u'Не больше 2МБ.')

			except AttributeError:
				pass

			return avatar
	
		def clean(self):
			cleaned_data = super(ProfileForm, self).clean()
			
			try:
				user = User.objects.get(username=cleaned_data['username'])
				if user.id != self.user.id:
					self.add_error('username', u'Пользователь с таким логином уже зарегистрирован')
			except Exception as e:
				raise e

			try:
				user = User.objects.get(email=cleaned_data['email'])
				if user.id != self.user.id:
					self.add_error('email', u'Пользователь с такой почтой уже зарегистрирован')
			except Exception as e:
				pass

			return cleaned_data

	user_profile = UserProfile.by_user(request.user)

	if request.method == 'POST' and 'password-reset' not in request.POST:
		form = ProfileForm(request.POST, request.FILES)
		form.user = request.user
		
		if form.is_valid():
			import os
			from django.conf import settings

			avatar = form.cleaned_data['avatar']
			if avatar:
				_,extension = avatar.content_type.split('/')
				filename = 'upload/img/user/%d_avatar.%s' % (request.user.id, extension)
				path = os.path.join(settings.MEDIA_ROOT, filename)
				try:
					os.remove(path)
					from sorl.thumbnail import delete
					delete(filename, delete_file=False)
				except:
					pass
				fd = os.open(path, os.O_RDWR|os.O_CREAT)
				path = os.write(fd, avatar.read())
				user_profile.avatar = filename
			
			user_profile.about = form.cleaned_data['about']
			user_profile.save()

			# деактивируем пользователя если сменился email
			if request.user.email != form.cleaned_data['email']:
				request.user.email = form.cleaned_data['email']
				user_profile.is_active = False
				send_confirm(request)
			
			request.user.username = form.cleaned_data['username']
			request.user.first_name = form.cleaned_data['first_name']
			request.user.last_name = form.cleaned_data['last_name']
			request.user.save()

			return HttpResponseRedirect(reverse('profile_edit'))
	else:
		form = ProfileForm(initial={
			'username': request.user.username,	
			'email': request.user.email,	
			'first_name': request.user.first_name,	
			'last_name': request.user.last_name,	
			'about': user_profile.about,	
		})

	message = '' 
	if 'profile_edit_message' in request.session: 
		message = request.session['profile_edit_message']
		del request.session['profile_edit_message']

	# social auth
	from lib.social_auth.models import UserSocialAuth
	socials = UserSocialAuth.objects\
		.filter(user=request.user)\
		.values_list('provider', flat=True)
	
	return render(request, 'blog/profile/edit.html', {
		'user_profile': user_profile,
		'form': form,
		'message': message,
		'socials': socials,
	})

def change_password(request):
	if not request.user.is_authenticated():
		raise Http404

	from lib.password_reset.forms import PasswordResetForm as PasswordResetFormBase
	class PasswordResetForm(PasswordResetFormBase):
		pass

	if request.method == 'POST':
		form = PasswordResetForm(data=request.POST,user=request.user)
			
		if form.is_valid():
			form.save()
			request.session['profile_edit_message'] = 'Ваш пароль изменен'
			return HttpResponseRedirect(reverse('profile_edit'))
	else:
		form = PasswordResetForm(user=request.user)

	return render(request, 'blog/profile/change_password.html', {
		'form': form,
	})

@user_access
def posts(request):
	if 'draft' in request.GET:
		published = False
	elif 'moderate' in request.GET:
		published = None
	else:
		published = True

	posts = Post.objects\
		.filter(is_deleted=False)\
		.filter(is_published=published)\
		.filter(user=request.user)\
		.order_by('-created_at')
	
	paginator = Paginator(posts, 10)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		posts = paginator.page(1)
	except EmptyPage:
		posts = paginator.page(paginator.num_pages)

	return render(request, 'blog/profile/posts.html', {
		'breadcrumbs': [('Мои посты', '')],
		'posts': posts,
		'published': published,
		'queries': queries_without_page(request),
	})

@user_access
def questions(request):
	from qa.models import Question
	questions = Question.objects\
		.filter(user=request.user)\
		.order_by('-created_at')
	
	paginator = Paginator(questions, 10)
	page = request.GET.get('page')
	try:
		questions = paginator.page(page)
	except:
		questions = paginator.page(1)
	
	return render(request, 'blog/profile/questions.html', {
		'breadcrumbs': [('Мои вопросы', '')],
		'questions': questions,
	})

@user_access
def subs_questions(request):
	from qa.models import Question, QuestionSubs

	subs = QuestionSubs.objects\
		.filter(user=request.user)\
		.order_by('-question__created_at')\
		.select_related('question')
	
	paginator = Paginator(subs, 10)
	page = request.GET.get('page')
	try:
		subs = paginator.page(page)
	except:
		subs = paginator.page(1)

	return render(request, 'blog/profile/subs_questions.html', {
		'subs': subs,
		'questions': [sub.question for sub in subs],
	})

def scores(request):
	if not request.user.is_authenticated():
		raise Http404

	user = request.user
	breadcrumbs = [('Мои баллы', '')]
	profile = UserProfile.by_user(user)

	tender = Tender.active()
	if 'likes' in request.GET:
		mode = 'likes'
		if tender:
			items = Post.objects\
				.filter(user=user)\
				.filter(moderated_at__range=tender.date_range(),is_moderated=True)\
				.order_by('-shares_updated_at')
		else:
			items = []

	else:
		mode = 'basic'
		if tender:
			items = Scoring.objects\
				.filter(user=user)\
				.filter(created_at__range=tender.date_range())\
				.order_by('-created_at','-id')
		
			from django.db.models import Sum
			new_items = items.filter(is_active=True)
			scores_sum = new_items.aggregate(sum=Sum('value'))['sum'] or 0
			scores_charges = new_items.count()
		
		else:
			items = []
			scores_sum = 0
			scores_charges = 0	

	paginator = Paginator(items, 30)
	page = request.GET.get('page')
	try:
		items = paginator.page(page)
	except:
		items = paginator.page(1)

	# сбрасываем счетчики
	if mode == 'basic':
		for item in items:
			if item.is_active:
				scores_sum -= item.value
				scores_charges -= 1
				item.is_active = False
				item.save()
				item.is_active = True

		profile.new_scores_sum = scores_sum
		profile.new_scores_charges = scores_charges
		profile.save()
	
	return render(request, 'blog/profile/scores.html', {
		'items': items,
		'mode': mode,
		'profile': profile,
		'queries': queries_without_page(request),
	})

def liked_posts(request):
	from django.contrib.contenttypes.models import ContentType
	
	post_ids = Like.objects\
		.filter(content_type=ContentType.objects.get_for_model(Post))\
		.filter(user=request.user)\
		.values_list('object_id', flat=True)
	
	posts = Post.objects\
		.get_active()\
		.filter(id__in=post_ids)\
		.order_by('-created_at')
	
	paginator = Paginator(posts, 10)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except:
		posts = paginator.page(1)

	return render(request, 'blog/profile/liked.html', {
		'breadcrumbs': [('Избранное', '')],
		'posts': posts,
		'queries': queries_without_page(request),
	})

def notifications(request):
	if not request.user.is_authenticated():
		raise Http404

	items = Notification.objects\
		.filter(user=request.user)\
		.order_by('-created_at')

	count = items.filter(is_active=True).count()
	
	paginator = Paginator(items, 10)
	page = request.GET.get('page')
	try:
		items = paginator.page(page)
	except:
		items = paginator.page(1)

	for item in items:
		if item.is_active:
			count -= 1
			item.is_active = False
			item.save()
			item.is_active = True

	profile = UserProfile.by_user(request.user)
	profile.new_notification_count = count
	profile.save()

	return render(request, 'blog/profile/notifications.html', {
		'items': items,
		'queries': queries_without_page(request),
	})