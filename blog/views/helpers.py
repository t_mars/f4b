#coding=utf8
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404

class RedirectException(Exception):
	def __init__(self, url):
		self.url = url

	def render(self, request):
		return HttpResponseRedirect(self.url)

def get_client_ip(request):
	x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
	if x_forwarded_for:
		ip = x_forwarded_for.split(',')[0]
	else:
		ip = request.META.get('REMOTE_ADDR')
	return ip

def sorter(fields, request, default=None):
	modes = {}
	for name,field,desc in fields:
		modes[name] = field
		modes['-'+name] = '-'+field
	
	if 'sort' in request.GET and request.GET['sort'] in modes:
		s = request.GET['sort']
	else:
		s = default or fields[0][0]
	
	return s,modes[s]

def queries_without_page(request):
	r = request.GET.copy()
	if r.has_key('page'):
		del r['page']
	return r
	
def user_access(func):
	def wrapper(request, *args, **kwargs):
		if not request.user.is_authenticated():
			raise Http404
		
		from blog.models import UserProfile
		user_profile = UserProfile.by_user(request.user)
		if not user_profile.is_active:
			request.session['profile_edit_message'] = u'Для дальнейшей работы необходимо подтвердить аккаунт.'
			return HttpResponseRedirect(reverse('profile_edit'))

		return func(request, *args, **kwargs)
	return wrapper