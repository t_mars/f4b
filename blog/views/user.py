#coding=utf8
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect, Http404 

from blog.models import *

def rating_scores(request):
	profiles = UserProfile.objects\
		.all()\
		.order_by('-scores')

	paginator = Paginator(profiles, 10)
	page = request.GET.get('page')
	try:
		profiles = paginator.page(page)
	except:
		profiles = paginator.page(1)

	return render(request, 'blog/user/rating_scores.html', {
		'breadcrumbs': [('Пользователи', '')],
		'profiles': profiles,
	})

def rating_pubs(request):
	tender = Tender.active()
	
	if tender:
		from django.db.models import Count
		profiles = UserProfile.objects\
			.filter(
				user__post__moderated_at__range=tender.date_range(),
				user__post__is_moderated=True,
				user__post__is_deleted=False
			)\
			.annotate(pub_count=Count('user__post'))\
			.order_by('-pub_count')
	else:
		profiles = []

	paginator = Paginator(profiles, 10)
	page = request.GET.get('page')
	try:
		profiles = paginator.page(page)
	except:
		profiles = paginator.page(1)

	return render(request, 'blog/user/rating_pubs.html', {
		'breadcrumbs': [('Пользователи', '')],
		'profiles': profiles,
	})

def rating_posts(request):
	tender = Tender.active()

	if tender:
		from django.db.models import Max
		profiles = UserProfile.objects\
			.filter(
				user__post__moderated_at__range=tender.date_range(),
				user__post__is_moderated=True,
				user__post__is_deleted=False
			)\
			.annotate(max_view_count=Max('user__post__view_count'))\
			.order_by('-max_view_count')
		values = tuple([(int(profile.user_id), profile.max_view_count) for profile in profiles])
		
		if values:
			posts = Post.objects\
				.extra(where=['(user_id, view_count) IN %s' % (values,)])\
				.order_by('-view_count')
		else:
			posts = []
	else:
		posts = []

	paginator = Paginator(posts, 10)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except:
		posts = paginator.page(1)

	return render(request, 'blog/user/rating_posts.html', {
		'breadcrumbs': [('Пользователи', '')],
		'posts': posts,
	})


def profile(request, id):
	try:
		user = User.objects.get(pk=id)
		profile = UserProfile.by_user(user)
	except:
		raise Http404

	breadcrumbs = [('Пользователи', reverse('users')), (user.username, '')]

	return render(request, 'blog/user/profile.html', {
		'breadcrumbs': breadcrumbs,
		'user': user,
		'profile': profile,
	})

def posts(request, id):
	try:
		user = User.objects.get(pk=id)
		profile = UserProfile.by_user(user)
	except:
		raise Http404

	breadcrumbs = [
		('Пользователи', reverse('users')), 
		(user.username, reverse('user', args=[user.id])),
		('Посты', '')
	]

	posts = Post.objects\
		.get_active()\
		.filter(user=user)\
		.order_by('-created_at')

	paginator = Paginator(posts, 10)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		posts = paginator.page(1)
	except EmptyPage:
		posts = paginator.page(paginator.num_pages)

	return render(request, 'blog/user/posts.html', {
		'breadcrumbs': breadcrumbs,
		'posts': posts,
		'user': user,
		'profile': profile,
	})
