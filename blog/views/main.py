#coding=utf8
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect, Http404 

from blog.models import *

from helpers import *

def feedback(request):
	from django import forms
	class FeedbackForm(forms.Form):
		name = forms.CharField(max_length=200,required=False)
		email = forms.EmailField(max_length=200,required=False)
		theme = forms.CharField(max_length=200,required=False)
		content = forms.CharField(widget=forms.Textarea())
	
	if request.method == 'POST':
		form = FeedbackForm(request.POST)
		if form.is_valid():
			from django.conf import settings
			from django.core.mail import EmailMultiAlternatives, send_mail

			subject, frm, to = 'Обратная связь', 'fuel4bull@gmail.com', settings.FEEDBACK_EMAILS
			html_content = ''
			if request.user.is_authenticated():
				html_content += u'Пользователь: <a href="%s">%s</a><br>' % (
					request.build_absolute_uri(reverse('user',args=[request.user.id])), 
					request.user.username
				)
			else:
				html_content += u'Имя: %s<br>' % form.cleaned_data['name']
				html_content += u'Email: %s<br>' % form.cleaned_data['email']
			html_content += u'Тема: %s<br>' % form.cleaned_data['theme']
			html_content += u'Содержание: %s<br>' % form.cleaned_data['content']

			msg = EmailMultiAlternatives(subject, '', frm, to)
			msg.attach_alternative(html_content, "text/html")
			msg.send()

			return HttpResponseRedirect(reverse('home'))
	else:
		form = FeedbackForm()

	return render(request, 'blog/feedback.html', {
		'breadcrumbs': [('Обратная связь', '')],
		'form': form,
	})

def home(request):
	posts = Post.objects\
		.get_active()\
		.order_by('-created_at')

	paginator = Paginator(posts, 10)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except:
		posts = paginator.page(1)

	random_posts = Post.objects\
		.get_active()\
		.order_by('?')[:10]

	return render(request, 'blog/home.html', {
		'posts': posts,	
		'random_posts': random_posts,	
	})

def search(request):
	q = request.GET.get('q', '')

	error = ''
	if len(q) < 3:
		error = 'Поисковый запрос должен быть больше 3 символов'
		posts = []
	else:
		posts = Post.objects.\
			filter(title__icontains=q)

	paginator = Paginator(posts, 10)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		posts = paginator.page(1)
	except EmptyPage:
		posts = paginator.page(paginator.num_pages)

	return render(request, 'blog/search.html', {
		'posts': posts,
		'query': q,
		'error': error,
	})

def winners(request):
	tender = Tender.last()
	if not tender:
		return Http404

	from django.template import Context, Template
	t = Template('{% load blog_tags %} ' + tender.winners_page)
	c = Context()
	content = t.render(c)

	return render(request, 'blog/page.html', {
		'content': content,
		'title': 'Призеры',
		'tender': tender,
	})

def prizes(request):
	tender = Tender.last()
	if not tender:
		return Http404

	from django.template import Context, Template
	t = Template('{% load blog_tags %} ' + tender.prizes_page)
	c = Context()
	content = t.render(c)

	return render(request, 'blog/page.html', {
		'content': content,
		'title': 'Наборы призов',
		'tender': tender,
	})

def page(request, mode):
	try:
		page = Page.objects.get(slug=mode)
		return render(request, 'blog/page.html', {
			'title': page.title,
			'content': page.content,
		})

	except:
		raise Http404