#coding=utf8
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django import forms

from blog.models import *
from wiki.models import Product, Category
import json

def get_select(options, value=None, readonly=False):
	attrs = {'class':'form-control'}
	if readonly:
		attrs['disabled'] = 'disabled'

	return forms.ChoiceField(
		choices=[('','---')]+options,
		initial=value,
		widget=forms.widgets.Select(attrs=attrs)
	)

def get_theme_selector(id=None, readonly=False):
	res = []
	
	root = ThemePost(id)
	themes = root.pre_themes + [root]
	
	value = None
	for theme in themes[::-1]:
		if not theme.is_leaf:
			options = [(t.id, t.title) for t in theme.sub_themes]
			res.append(get_select(options, value=value, readonly=readonly))
		value = theme.id

	# ThemeForm = type(
	# 	str('ThemeForm'), 
	# 	(forms.Form,), 
	# 	{'%d' % (len(res)-i-1):field for i,field in enumerate(res)}
	# )
	class ThemeForm(forms.Form):
		def __init__(self, fields):
			forms.Form.__init__(self)
			for i,field in enumerate(fields[::-1]):
				 self.fields['level_%d'%i] = field
	
	return ThemeForm(res)

def scores_for_post(request):
	from lib.django_summernote.widgets import SummernoteWidget
	class PostForm(forms.Form):
		content = forms.CharField(widget=SummernoteWidget())
	
	content = ''
	if request.method == 'POST':
		form = PostForm(request.POST)
		if form.is_valid():
			content = form.cleaned_data['content']
	
	scores, data = Scoring.for_post(content, data=True)
	data['scores'] = scores

	return render(request, 'blog/post/scores.html', data)

def subtopic(request, id):
	return HttpResponse(get_theme_selector(id))

def post_like(request, id):
	if not request.user.is_authenticated():
		return HttpResponse(json.dumps({
			'status': 'error',
			'message': 'not authenticated',	
		})) 

	try:
		post = Post.objects.get(pk=id)
	except:
		return HttpResponse(json.dumps({
			'status': 'error',
			'message': 'not found',	
		}))

	count, created = post.add_like(request.user)

	return HttpResponse(json.dumps({
		'status': 'success',
		'count': count,
	}))

def post_complaint(request, id):
	if not request.user.is_authenticated():
		return HttpResponse(json.dumps({
			'status': 'error',
			'message': 'not authenticated',	
		}))

	try:
		post = Post.objects.get(pk=id)
	except:
		return HttpResponse(json.dumps({
			'status': 'error',
			'message': 'not found',	
		}))

	post.add_complaint(request.user)

	return HttpResponse(json.dumps({
		'status': 'success',
	}))

def looked_intro(request, name):
	if not request.user.is_authenticated():
		return HttpResponse(json.dumps({
			'status': 'error',
			'message': 'not authenticated',	
		}))

	profile = UserProfile.by_user(request.user)
	if name == 'menu':
		profile.looked_intro_menu = True
	elif name == 'post':
		profile.looked_intro_post = True
	elif name == 'forum':
		profile.looked_intro_forum = True
	elif name == 'qa':
		profile.looked_intro_qa = True
	profile.save()

	return HttpResponse(json.dumps({
		'status': 'success',
	}))