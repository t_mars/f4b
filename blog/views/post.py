#coding=utf8
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.files.images import get_image_dimensions
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django import forms


from lib.django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget

from blog.models import *

from helpers import *

SUMMERNOTE_CONFIG = {
    # Using SummernoteWidget - iframe mode
    'iframe': False,  # or set False to use SummernoteInplaceWidget - no iframe mode
    'cleanPaste': True,

    # Using Summernote Air-mode
    'airMode': True,

    # Use native HTML tags (`<b>`, `<i>`, ...) instead of style attributes
    # (Firefox, Chrome only)
    'styleWithTags': True,

    # Set text direction : 'left to right' is default.
    'direction': 'ltr',

    # Change editor size
    'width': '100%',
    'height': '480',

    # Use proper language setting automatically (default)
    # 'lang': None,

    # Or, set editor language/locale forcely
    'lang': 'ru-RU',

    # Customize toolbar buttons
    'toolbar': [
        ['style', ['style']],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video', 'hr']],
    ],
   # Need authentication while uploading attachments.
    'attachment_require_authentication': True,
}

def clean_post_content(content):
	from lxml.html.clean import Cleaner

	cleaner = Cleaner()
	cleaner.javascript = True # This is True because we want to activate the javascript filter
	cleaner.style = True      # This is True because we want to activate the styles & stylesheet filter
	cleaner.embedded = False # iframe save
	
	return cleaner.clean_html(content)

class BasePostForm(forms.Form):
	title = forms.CharField(max_length=150,min_length=5)
	description = forms.CharField(max_length=150,min_length=50)
	content = forms.CharField(widget=SummernoteWidget(config=SUMMERNOTE_CONFIG))
	external_link = forms.URLField(max_length=500,required=False)

	def clean_content(self):
		content = self.cleaned_data['content']
		content = clean_post_content(content)

		if Scoring.letter_count(content) < 100:
			raise forms.ValidationError('Длина содержания поста должна быть не меньше 100 символов')
		
		return content

@user_access
def create(request):

	class PostForm(BasePostForm):
		theme = forms.CharField(widget=forms.HiddenInput())
		
		def __init__(self, *args, **kwargs):
			captcha = kwargs.pop('captcha', False)
			super(PostForm, self).__init__(*args, **kwargs)
			if captcha:
				from captcha.fields import CaptchaField
				self.fields['captcha'] = CaptchaField()

		def clean(self):
			cleaned_data = super(PostForm, self).clean()
			
			try:
				theme = ThemePost(cleaned_data['theme'])
				object_type, object_id = theme.value
				cleaned_data['object_type'] = object_type
				cleaned_data['object_id'] = object_id
			except:
				self.add_error('theme', u'Выберите тему для поста')

			
			return cleaned_data
	
	# определяем выводить ли капчу
	captcha = False
	if not request.user.is_superuser:
		import datetime
		a = datetime.date.today()
		b = a + datetime.timedelta(days=1)
		count = Post.objects.filter(
			user=request.user,
			created_at__range=[a,b]
		).count()
		if count >= 5:
			captcha = True
	
	if request.method == 'POST':
		form = PostForm(request.POST, request.FILES, captcha=captcha)
		if form.is_valid():
			
			post = Post()
			post.title = form.cleaned_data['title']
			post.description = form.cleaned_data['description']
			post.external_link = form.cleaned_data.get('external_link')
			post.content = form.cleaned_data['content']
			post.object_id = form.cleaned_data['object_id']
			post.object_type = form.cleaned_data['object_type']
			# post.preview_image = form.cleaned_data['preview']
			post.user = request.user
			post.save()

			Admin.reset_counters()
			UserProfile.by_user(post.user).reset_counters()

			return HttpResponseRedirect(reverse('profile_posts'))
	else:
		form = PostForm(captcha=captcha)
	
	from ajax import get_theme_selector
	if 'theme' in request.POST:
		theme_selector = get_theme_selector(request.POST.get('theme'))
	else:
		theme_selector = get_theme_selector()

	return render(request, 'blog/post/create.html', {
		'form': form,
		'theme_selector': theme_selector,
	})

@user_access
def edit(request, id):
	try:
		post = Post.objects\
			.get_active(check_pub=False)\
			.select_related('user')\
			.get(pk=id)
	except:
		raise Http404('Пост не найден')
		
	if not request.user.is_superuser:
		if request.user.id != post.user.id:
			raise Http404('Ошибка доступа')
	
		if post.is_moderated:
			raise Http404('Нельзя редактировать модерированные статьи')

	from ajax import get_theme_selector
	theme_selector = get_theme_selector(post.type_mode, readonly=True)

	class PostForm(BasePostForm):
		pass

	if request.method == 'POST':
		form = PostForm(request.POST)
		if form.is_valid():
			post.title = form.cleaned_data['title']
			post.description = form.cleaned_data['description']
			post.external_link = form.cleaned_data.get('external_link')
			post.content = form.cleaned_data['content']
		
			# отправка на модерацию
			if post.is_published == False and 'publicate' in request.POST:
				post.is_published = None

				Admin.reset_counters()
				UserProfile.by_user(post.user).reset_counters()

			post.save()

			return HttpResponseRedirect(
				reverse('post', args=[post.id, post.slug])
			)
	else:
		form = PostForm(initial={
			'title': post.title, 
			'description': post.description,
			'content': post.content,
			'external_link': post.external_link,
		})

	return render(request, 'blog/post/edit.html', {
		'theme_selector': theme_selector,
		'post': post,	
		'form': form,
	})

def show(request, id, slug):
	try:
		post = Post.objects\
			.get_active(check_pub=False)\
			.get(id=id)
	except:
		raise Http404
	
	# проверка доступа
	if not post.is_published and request.user.id != post.user.id:
		raise Http404

	# проверка адреса
	if slug != post.slug:
		url = reverse('post', args=[id,post.slug])
		return HttpResponseRedirect(url)
	
	class CommentForm(forms.Form):
		comment = forms.CharField(min_length=2,max_length=3000,required=True,widget=forms.Textarea())

	post_type = ContentType.objects.get_for_model(post)
	
	form = None
	if request.user.is_authenticated():
		if request.method == 'POST':
			form = CommentForm(request.POST)
			if form.is_valid():
				comment = Comment()
				comment.content = form.cleaned_data['comment']
				comment.user = request.user
				comment.object_id = post.id
				comment.content_type = post_type
				comment.save()

				Notification().post_comment(comment)

				post.comment_count += 1
				post.save()
				
				url = reverse('post', args=[post.id,post.slug])
				return HttpResponseRedirect(url)
		else:
			form = CommentForm()
				
	comments = Comment.objects\
		.filter(content_type=post_type,object_id=post.id)\
		.order_by('created_at')[:10]
	
	paginator = Paginator(comments, 10)
	page = request.GET.get('page', paginator.num_pages)
	try:
		comments = paginator.page(page)
	except PageNotAnInteger:
		comments = paginator.page(1)
	except EmptyPage:
		comments = paginator.page(paginator.num_pages)


	breadcrumbs = [
		('Посты', reverse('post_list')),
		(post.title, ''),
	]
	
	# считаем кол-во просмотров
	ViewLog.add(request, 'post', post.id)
	post.view_count = ViewLog.count('post', post.id)
	
	from datetime import datetime
	post.last_viewed_at = datetime.now()
	post.save()
	
	# связанные посты
	recent_posts = Post.objects.get_active().order_by('?')[:10]
	random_posts = Post.objects.get_active().order_by('?')[:10]

	return render(request, 'blog/post/show.html', {
		'breadcrumbs': breadcrumbs,
		'post': post,
		'has_like': post.has_like(request.user),
		'has_complaint': post.has_complaint(request.user),
		'form': form,
		'comments': comments,
		'random_posts': random_posts,
		'recent_posts': recent_posts,
	})

@user_access
def delete(request, id):
	try:
		post = Post.objects\
			.get_active()\
			.select_related('user')\
			.get(pk=id)
	except:
		raise Http404

	if not request.user.is_superuser:
		if request.user.id != post.user.id:
			raise Http404
	
		if post.is_moderated:
			raise Http404('Нельзя удалять модерированные статьи')

	post.is_deleted = True
	post.save()

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def list(request, mode):
	posts = Post.objects.get_active()

	if mode == 'top':
		from datetime import date
		from datetime import timedelta

		date_from = date.today() - timedelta(days=7)
		
		posts = posts.filter(created_at__gte=date_from)\
			.order_by('-view_count')
		
	elif mode == 'new':
		posts = posts.order_by('-created_at')

	paginator = Paginator(posts, 20)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except:
		posts = paginator.page(1)

	return render(request, 'blog/post/posts.html', {
		'posts': posts,
		'mode': mode,
	})

def category(request, slug, id):
	posts = Post.objects\
		.get_active()\
		.order_by('-created_at')

	breadcrumbs = [
		('Посты', reverse('post_list')),
	]

	product = None
	if slug == 'descriptions':
		title = 'Описания спортивного питания'
		posts = posts.filter(object_type=0)
		if id:
			try:
				from wiki.models import Product
				product = Product.objects.get(pk=id)
				p = '%s %s' % (product.brand.name, product.name)
			except:
				raise Http404

			posts = posts.filter(object_id=id)
			breadcrumbs.append((title, reverse('post_category',args=[slug])))
			breadcrumbs.append((p, ''))
			title = 'Описания %s' % p.encode('utf8')
		else:
			breadcrumbs.append((title, ''))
	elif slug == 'reviews':
		title = 'Отзывы о спортивном питании'
		posts = posts.filter(object_type=1)
		if id:
			try:
				from wiki.models import Product
				product = Product.objects.get(pk=id)
				p = '%s %s' % (product.brand.name, product.name)
			except:
				raise Http404

			posts = posts.filter(object_id=id)
			breadcrumbs.append((title, reverse('post_category',args=[slug])))
			breadcrumbs.append((p, ''))
			title = 'Отзывы %s' % p.encode('utf8')
		else:
			breadcrumbs.append((title, ''))
	
	else:
		try:
			topic = Topic.objects.get(slug=slug)
		except:
			raise Http404('Категория не найдена')
		breadcrumbs.append((topic.title, ''))
		title = topic.title
		posts = posts.filter(object_type=2,object_id=topic.id)

	paginator = Paginator(posts, 20)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except:
		posts = paginator.page(1)

	return render(request, 'blog/post/category.html', {
		'product': product,
		'breadcrumbs': breadcrumbs,
		'posts': posts,	
		'title': title,
	})