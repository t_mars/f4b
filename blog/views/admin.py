#coding=utf8
from django.shortcuts import render
from django.http import HttpResponseRedirect,Http404
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.cache import cache

from blog.models import *
from helpers import queries_without_page

def check_access(request):
	if not request.user.is_superuser:
		raise Http404

def post_admin_panel(request, post):
	if request.is_ajax():
		return render(request, 'blog/post/admin.html', {
			'post': post
		})

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def recalc_scores(request):
	check_access(request)

	count = 0
	profiles = UserProfile.objects.all().select_related('user')
	for profile in profiles:
		scores = profile.scores
		Scoring.reset_user_scores(profile.user)
		if scores != profile.scores:
			count += 1

	request.session['admin_message'] = 'Пересчитано %d, исправлено %d' % (profiles.count(), count)

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def show_panel(request):
	check_access(request)
	request.session['show_admin_panel'] = True

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def hide_panel(request):
	check_access(request)
	if 'show_admin_panel' in request.session:
		del request.session['show_admin_panel']

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def unique_check(request, id):
	check_access(request)

	from blog.common import unique_check
	
	try: 
		post = Post.objects.get(pk=id)
		post.uniqueness = unique_check(post.content)
		post.save()
	except:
		pass

	return post_admin_panel(request, post)

def post_accept(request, id):
	check_access(request)

	try: 
		post = Post.objects.get(pk=id)
		post.moderate()

		Admin.reset_counters()
		UserProfile.by_user(post.user).reset_counters()
	
		return post_admin_panel(request, post)
	
	except Exception as e:
		raise Http404


def post_deny(request):
	check_access(request)

	try: 
		post = Post.objects.get(pk=request.POST.get('id', 40))
		post.unmoderate(reason=request.POST.get('message', ''))
		
		Admin.reset_counters()
		UserProfile.by_user(post.user).reset_counters()
		
		return post_admin_panel(request, post)
	
	except Exception as e:
		raise Http404

def posts(request):
	check_access(request)

	if not request.user.is_authenticated():
		raise Http404

	posts = Post.objects\
		.order_by('-created_at')
	
	if 'draft' in request.GET:
		mode = 'draft'
		posts = posts.filter(is_published=False,is_moderated=False)
	
	elif 'pub' in request.GET:
		mode = 'pub'
		posts = posts.filter(is_published=True)
	
	elif 'moderate' in request.GET:
		mode = 'moderate'
		posts = posts.filter(is_published=None,is_moderated=False)
	
	elif 'new' in request.GET:
		mode = 'new'
		posts = posts.filter(is_moderated=None,is_published=True)
	
	else:
		mode = 'new'
		posts = posts.filter(is_moderated=None,is_published=True)

	paginator = Paginator(posts, 10)
	page = request.GET.get('page')
	try:
		posts = paginator.page(page)
	except PageNotAnInteger:
		posts = paginator.page(1)
	except EmptyPage:
		posts = paginator.page(paginator.num_pages)

	return render(request, 'blog/admin/posts.html', {
		'posts': posts,
		'queries': queries_without_page(request),
		'mode': mode,
		'admin': Admin(),
	})

def user_scores(request, id):
	check_access(request)
	try:
		user = User.objects.get(pk=id)
	except:
		raise Http404
		
	breadcrumbs = [
		('Пользователи', reverse('users')), 
		(user.username, reverse('user', args=[user.id])),
		('Баллы', '')
	]
	
	tender = Tender.active()
	if 'likes' in request.GET:
		mode = 'likes'
		if tender:
			items = Post.objects\
				.filter(user=user)\
				.filter(moderated_at__range=tender.date_range(),is_moderated=True)\
				.order_by('-updated_at')
		else:
			items = []

	else:
		mode = 'basic'
		if tender:
			items = Scoring.objects\
				.filter(user=user)\
				.filter(created_at__range=tender.date_range())\
				.order_by('-created_at','-id')
		else:
			items = []

	from django import forms
	class ChargeForm(forms.Form):
		reason = forms.CharField(max_length=90,min_length=5)
		scores = forms.IntegerField()

	if request.method == 'POST':
		form = ChargeForm(request.POST)
		if form.is_valid():
			Scoring.admin_charge(user=user,**form.cleaned_data)
			
			return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
	else:
		form = ChargeForm()

	paginator = Paginator(items, 30)
	page = request.GET.get('page')
	try:
		items = paginator.page(page)
	except:
		items = paginator.page(1)
	
	return render(request, 'blog/admin/user_scores.html', {
		'form': form,
		'breadcrumbs': breadcrumbs,
		'items': items,
		'mode': mode,
		'user': user,
		'profile': UserProfile.by_user(user),
		'queries': queries_without_page(request),
	})