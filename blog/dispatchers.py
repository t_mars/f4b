#coding=utf8
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.contrib.auth.models import User


from models import *
from forum.models import *
from qa.models import *

class ScroringMiddleware:
	def process_request(self, request):
		if not request.user.is_authenticated():
			return

		Scoring().user_enter(request.user)

@receiver(post_save, sender=Question)
def question_save(sender, instance, created, **kwargs):
	if not created:
		return
	Scoring().new_question(instance)

@receiver(post_save, sender=Answer)
def answer_save(sender, instance, created, **kwargs):
	Scoring().correct_answer(instance)

@receiver(post_save, sender=Comment)
def comment_save(sender, instance, created, **kwargs):
	if not created:
		return
	Scoring().new_post_comment(instance)

@receiver(post_save, sender=Message)
def message_save(sender, instance, created, **kwargs):
	if not created:
		return
	Scoring().new_forum_message(instance)