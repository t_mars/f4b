#coding=utf8
from django.contrib import admin
from django import forms

from lib.django_summernote.widgets import SummernoteWidget

from blog.common import unique_check
from blog.models import *


class PostForm(forms.ModelForm):
	content = forms.CharField(widget=SummernoteWidget())
	
	class Meta:
		model = Post
		fields = ['title','slug','preview_image','description','content']

class PostAdmin(admin.ModelAdmin):
	form = PostForm
	list_display = ['title','is_moderated','is_deleted','uniqueness','scores','created_at']
	actions = ['check_unique', 'moderate', 'unmoderate']

	def check_unique(self, request, queryset):
		for post in queryset:
			post.uniqueness = unique_check(post.content)
			post.save()
	check_unique.short_description = u"Проверить уникальность"

	def moderate(self, request, queryset):
		from datetime import datetime
		for post in queryset:
			post.is_moderated = True
			post.moderated_at = datetime.now()
			post.save()
	moderate.short_description = u"Одобрить посты"

	def unmoderate(self, request, queryset):
		from datetime import datetime
		for post in queryset:
			post.is_moderated = False
			post.moderated_at = datetime.now()
			post.save()
	unmoderate.short_description = u"Запретить посты"

admin.site.register(Post, PostAdmin)

admin.site.register(Topic)
admin.site.register(Comment)

class UserProfileAdmin(admin.ModelAdmin):
	list_display = ['user','is_active']

admin.site.register(UserProfile, UserProfileAdmin)

class TenderForm(forms.ModelForm):
	winners_page = forms.CharField(widget=SummernoteWidget())
	prizes_page = forms.CharField(widget=SummernoteWidget())
	
	class Meta:
		model = Tender

class TenderAdmin(admin.ModelAdmin):
	form = TenderForm
	list_display = ['id','start','finish']

admin.site.register(Tender, TenderAdmin)

class PageForm(forms.ModelForm):
	content = forms.CharField(widget=SummernoteWidget())

	class Meta:
		model = Page

class PageAdmin(admin.ModelAdmin):
	form = PageForm
	list_display = ['title', 'slug']
admin.site.register(Page, PageAdmin)