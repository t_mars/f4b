#coding=utf8
from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.template.defaultfilters import slugify
from django.core.cache import cache

from unidecode import unidecode
import datetime

from lib.mptt.models import MPTTModel, TreeForeignKey

from wiki.models import Product, Category

class InvocationObject():
	def invocations(self):
		users = []
		import re
		usernames = set([n.lower for n in re.findall('@([a-zA-Z_]+)', self._content)])
		for username in usernames:
			try:
				users.append(User.objects.get(username=username))
			except:
				continue

		return users

	@property
	def content(self):
		from django.core.urlresolvers import reverse
		for user in self.invocations():
			link = '<a href="%s">@%s</a>' % (reverse('user',args=[user.id]), user.username)
			self._content = self._content.replace('@%s' % user.username, link)
			
		return self._content

	@content.setter
	def content(self, value):
		self._content = value

def get_username(backend, details, *args, **kwargs):
	if backend.name == 'vk-oauth':
		username = '%s %s' % (details['first_name'], details['last_name'])
		details['username'] = username 
	details['username'] = unidecode(details['username'])

def user_profile_data(backend, user, response, *args, **kwargs):
	image_url = None
	if backend.name == 'vk-oauth':
		image_url = response.get('user_photo')
	elif backend.name == 'facebook':
		image_url = 'http://graph.facebook.com/%s/picture?type=square' % response.get('id')
	elif backend.name == 'google-oauth2':
		image_url = response.get('picture')
	elif backend.name == 'twitter':
		image_url = response.get('profile_image_url')
	else:
		raise Exception(backend.name, response)

	if image_url:
		import os
		from django.conf import settings
		from urllib2 import urlopen
		from mimetypes import guess_extension

		avatar = urlopen(image_url)
		extension = avatar.info()['Content-Type'].split('/')[1]
		
		filename = 'upload/img/user/%d_avatar.%s' % (user.id, extension)
		path = os.path.join(settings.MEDIA_ROOT, filename)
		try:
			os.remove(path)
		except:
			pass
		fd = os.open(path, os.O_RDWR|os.O_CREAT)
		path = os.write(fd, avatar.read())
		user_profile = UserProfile.by_user(user)
		user_profile.avatar = filename
		user_profile.save()



def get_slug(name):
	return slugify(unidecode(name))

class Comment(models.Model, InvocationObject):
	_content = models.TextField(db_column='content')
	
	content_type = models.ForeignKey(ContentType)
	object_id = models.PositiveIntegerField()
	content_object = GenericForeignKey('content_type', 'object_id')

	created_at = models.DateTimeField(auto_now_add=True)
	is_moderated = models.BooleanField(default=True)
	user = models.ForeignKey(User)

	def save(self, *args, **kwargs):
		pk = self.pk
		super(Comment, self).save(*args, **kwargs)
		if pk is None:
			Scoring().new_post_comment(self)

class Like(models.Model):
	content_type = models.ForeignKey(ContentType)
	object_id = models.PositiveIntegerField()
	content_object = GenericForeignKey('content_type', 'object_id')

	created_at = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User)

class Complaint(models.Model):
	content_type = models.ForeignKey(ContentType)
	object_id = models.PositiveIntegerField()
	content_object = GenericForeignKey('content_type', 'object_id')

	created_at = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User)

class Topic(MPTTModel):
	title = models.CharField(max_length=200)
	created_at = models.DateTimeField(auto_now_add=True)
	parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
	slug = models.CharField(max_length=200)

	def __unicode__(self):
		return self.title

	def save(self, *args, **kwargs):
		self.slug = get_slug(self.title)
		super(Topic, self).save(*args, **kwargs)

class PostManager(models.Manager):
	
	def get_active(self, check_pub=True):
		objs = self.filter(is_deleted=False)
		if check_pub:
			objs = objs.filter(is_published=True)

		return objs

OBJECT_TYPE_CHOICE = {
	0: 'description',
	1: 'review',
	2: 'topic',
}
class Post(models.Model, InvocationObject):
	objects = PostManager()
	
	title = models.CharField(max_length=200)
	preview_image = models.ImageField(upload_to='upload/img/post',null=False,blank=False)
	description = models.CharField(max_length=200)
	slug = models.CharField(max_length=200)
	_content = models.TextField(db_column='content')
	# указание ссылки на оригинальный пост
	external_link = models.CharField(max_length=500,null=True,default=None)

	uniqueness = models.PositiveIntegerField(default=None,null=True)
	is_moderated = models.NullBooleanField(default=None)
	scores = models.PositiveIntegerField(default=0)
	
	refusal_reason = models.CharField(max_length=200,null=True,default=None)
	is_published = models.NullBooleanField(default=True)
	# True - опубликован
	# False - НЕ опубликован
	# None - отправлен на публикацию, ждет модерации

	moderated_at = models.DateTimeField(null=True,default=None)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_deleted = models.BooleanField(default=False)
	last_viewed_at = models.DateTimeField(auto_now_add=True)
	
	shares_total = models.PositiveIntegerField(default=0)
	shares_facebook = models.PositiveIntegerField(default=0)
	shares_twitter = models.PositiveIntegerField(default=0)
	shares_vkontakte = models.PositiveIntegerField(default=0)
	shares_odnoklassniki = models.PositiveIntegerField(default=0)
	shares_googleplus = models.PositiveIntegerField(default=0)
	shares_updated_at = models.DateTimeField(auto_now_add=True)

	user = models.ForeignKey(User)

	# объект
	TYPE_DESCRIPTION = 0
	TYPE_REVIEW = 1
	TYPE_TOPIC = 2

	object_type = models.IntegerField(choices=OBJECT_TYPE_CHOICE.items(),null=False,blank=False)
	object_id = models.PositiveIntegerField()
	
	like_count = models.PositiveIntegerField(default=0)
	comment_count = models.PositiveIntegerField(default=0)
	view_count = models.PositiveIntegerField(default=0)
	
	def recount_shares_counts(self):
		if self.last_viewed_at >= self.shares_updated_at:
			import urllib, json

			url = 'http://blog.fuel4bull.ru/post/%d/%s/' % (self.id,self.slug)
			request_url = 'http://rest.sharethis.com/v1/count/urlinfo?url=%s' % url
			response = urllib.urlopen(request_url);
			data = json.loads(response.read())

			for mode,d in data.items():
				attr = 'shares_%s' % mode
				if hasattr(self, attr):
					setattr(self, attr, int(d['outbound']))
		import datetime
		self.shares_updated_at = datetime.datetime.now()
		self.save()

	def save(self, *args, **kwargs):
		# preview image
		from lxml.html import fromstring
		tree = fromstring(self.content)
		imgs = tree.xpath(".//img")
		if len(imgs):
			image = imgs[0].attrib['src']
		else:
			image = None

		if self.preview_image != image:
			from sorl.thumbnail import delete
			delete(self.preview, delete_file=False)
			self.preview_image = image

		self.slug = get_slug(self.title)
		super(Post, self).save(*args, **kwargs)
	
	@property
	def preview(self):
		if self.preview_image:
			s = str(self.preview_image)
			return '/'.join(s.split('/')[3:])
		return 'avatar.png'

	@property
	def type(self):
		return OBJECT_TYPE_CHOICE[self.object_type]
	
	@property
	def type_mode(self):
		if self.object_type == 0:
			return 'product/%d/description' % self.object_id
		if self.object_type == 1:
			return 'product/%d/review' % self.object_id
		if self.object_type == 2:
			return 'topic/%d' % self.object_id


	@property
	def object(self):
		if self.object_type in [0, 1]:
			from wiki.models import Product
			return Product.objects.get(pk=self.object_id,primary_product=None)
		else:
			return Topic.objects.get(pk=self.object_id)

	def has_like(self, user):
		content_type = ContentType.objects.get_for_model(self)
		try:
			Like.objects.get(
				content_type=content_type,
				object_id=self.id,
				user=user
			)
			return True
		except:
			return False

	def add_like(self, user):
		content_type = ContentType.objects.get_for_model(self)
		like,created = Like.objects.get_or_create(
			content_type=content_type,
			object_id=self.id,
			user=user
		)
		if created:
			self.like_count += 1
			self.save()

		return self.like_count, created

	def add_complaint(self, user):
		content_type = ContentType.objects.get_for_model(self)
		complaint,created = Complaint.objects.get_or_create(
			content_type=content_type,
			object_id=self.id,
			user=user
		)

		return created

	def has_complaint(self, user):
		content_type = ContentType.objects.get_for_model(self)
		try:
			Complaint.objects.get(
				content_type=content_type,
				object_id=self.id,
				user=user
			)
			return True
		except:
			return False

	def __unicode__(self):
		return self.title

	def moderate(self):
		if self.is_moderated:
			return
		
		self.is_moderated = True
		self.is_published = True
		self.refusal_reason = None
		self.moderated_at = datetime.datetime.now()
		self.save()

		Scoring().post_pub(self)	
		Scoring().post_first_description(self)	
		Scoring().post_each_5(self)

	def unmoderate(self, reason):
		if not self.is_moderated:
			return

		self.is_moderated = False
		self.is_published = False
		self.refusal_reason = reason
		self.moderated_at = datetime.datetime.now()
		self.save()

	def inc_view_count(self):
		self.view_count += 1
		self.save()

		Scoring().post_1000_views(self)

class UserProfile(models.Model):
	user = models.OneToOneField(User)
	avatar = models.ImageField(null=True)
	about = models.TextField(null=True)
	scores = models.PositiveIntegerField(default=0)
	confirm_key = models.CharField(max_length=48,null=True)
	is_active = models.BooleanField(default=False)
	
	looked_intro_menu = models.BooleanField(default=False)
	looked_intro_post = models.BooleanField(default=False)
	looked_intro_forum = models.BooleanField(default=False)
	looked_intro_qa = models.BooleanField(default=False)
	
	new_scores_sum = models.PositiveIntegerField(default=0)
	new_scores_charges = models.PositiveIntegerField(default=0)
	new_notification_count = models.PositiveIntegerField(default=0)

	@staticmethod
	def by_user(user):
		u, _ = UserProfile.objects.get_or_create(user=user)
		return u

	def cache_key(self):
		return 'user_counters_%d_' % self.user_id

	def reset_counters(self):
		key = self.cache_key()
		cache.delete_pattern('user_counters_*')
		
		posts = Post.objects\
			.filter(is_deleted=False,user_id=self.user_id)
		
		cache.set(
			key+'posts', 
			posts.count(),
			timeout=None
		)
		cache.set(
			key+'posts_draft', 
			posts.filter(is_published=False).count(),
			timeout=None
		)
		cache.set(
			key+'posts_pub', 
			posts.filter(is_published=True).count(),
			timeout=None
		)
		cache.set(
			key+'posts_moderate', 
			posts.filter(is_published=None).count(),
			timeout=None
		)

	def __getattr__(self, prop):
		if prop[:6] == 'count_':
			k = self.cache_key()+prop[6:]
			val = cache.get(k)
			if not val:
				self.reset_counters()
				val = cache.get(k)
			return val 
		
		return super(UserProfile, self).__getattr__(self,prop)

	def news_count(self):
		return self.new_scores_charges+self.new_notification_count

	def reset_notification_count(self):
		self.new_notification_count = Notification.objects\
			.filter(user=self.user)\
			.filter(is_active=True).count()
		self.save()

	def avatar_image(self):
		if self.avatar:
			return self.avatar
		return 'avatar.png'

def scoring_class(cls):
	targets = {}
	for name, method in cls.__dict__.iteritems():
		if hasattr(method, 'type'):
			targets[method.type] = method.target_class

	cls.targets = targets

	return cls

def scoring_method(target_class=None,value=None):
	def real_decorator(function):
		def wrapper(self, *args, **kwargs):
			self.value = value
			self.score_type = function.__name__
			function(self, *args, **kwargs)
		wrapper.type = function.__name__
		wrapper.target_class = target_class
		return wrapper
	return real_decorator

def today_range():
	today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
	today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)
	
	return (today_min,today_max)

from qa.models import Question,Answer
from forum.models import Message

@scoring_class
class Scoring(models.Model):
	user = models.ForeignKey(User)
	score_target = models.PositiveIntegerField()
	score_type = models.CharField(max_length=100,null=False,blank=False)

	created_at = models.DateTimeField(auto_now_add=True)
	value = models.PositiveIntegerField(default=0)
	is_active = models.BooleanField(default=True)
	
	class Meta:
		unique_together = (('user', 'score_target', 'score_type'),)

	@staticmethod
	def admin_charge(reason, scores, user):
		import datetime,time

		s = Scoring()
		s.user = user
		s.value = scores
		s.score_type = 'charge:%s' % reason
		s.score_target = time.mktime(datetime.datetime.now().timetuple())
		s.save()

	@scoring_method(value=5)
	def user_enter(self, user):
		import datetime,time

		self.user = user
		self.score_target = time.mktime(datetime.date.today().timetuple())
		self.save_if_new()

	@scoring_method(Question, 1)
	def new_question(self, question):
		count = Scoring.objects\
			.filter(user=question.user)\
			.filter(created_at__range=today_range())\
			.filter(score_type=self.score_type)\
			.count()
		
		if count >= 5:
			return
			
		self.user = question.user
		self.score_target = question.id
		self.save_if_new()

	@scoring_method(Answer, 5)
	def correct_answer(self, answer):
		if not answer.is_correct:
			return

		self.user = answer.user
		self.score_target = answer.id
		self.save_if_new()

	@scoring_method(Comment, 1)
	def new_post_comment(self, comment):
		count = Scoring.objects\
			.filter(user=comment.user)\
			.filter(created_at__range=today_range())\
			.filter(score_type=self.score_type)\
			.count()
		
		if count >= 5:
			return
			
		self.user = comment.user
		self.score_target = comment.id
		self.save_if_new()

	@scoring_method(Message, 1)
	def new_forum_message(self, message):
		# начисление баллов за сообщения
		if Scoring.letter_count(message.content) < 50:
			return

		count = Scoring.objects\
			.filter(user=message.user)\
			.filter(score_type=self.score_type)\
			.filter(created_at__range=today_range())\
			.count()
		
		if count >= 10:
			return

		self.user = message.user
		self.score_target = message.id
		self.save_if_new()

	# за содержание поста
	@scoring_method(Post)
	def post_pub(self, post):
		if post.is_moderated != True:
			return

		self.user = post.user
		self.score_target = post.id
		self.value = Scoring.for_post(post.content)
		self.save_if_new()
		
	# награждение за первое описание товара
	@scoring_method(Post, 20)
	def post_first_description(self, post):
		if post.is_moderated != True or post.type != 'description':
			return

		# проверяем есть ли еще описания этого товара
		count = Post.objects\
			.get_active()\
			.filter(is_moderated=True)\
			.filter(object_type=post.object_type)\
			.filter(object_id=post.object_id)\
			.exclude(id=post.id)\
			.count()
		
		if count != 0:
			return

		self.user = post.user
		self.score_target = post.id
		self.save_if_new()

	# за количество постов
	@scoring_method(Post, 50)
	def post_each_5(self, post):
		try:
			s = Scoring.objects.get(
				user=post.user,
				score_type='post_pub',
				score_target=post.id
			)
		except:
			return

		count = Scoring.objects\
			.filter(user=post.user)\
			.filter(score_type='post_pub')\
			.filter(id__lte=s.id)\
			.count()
		
		if count % 5 != 0:
			return

		self.user = post.user
		self.score_target = post.id
		self.save_if_new()

	# за количество просмотров поста
	@scoring_method(Post, 20)
	def post_1000_views(self, post):
		if post.is_moderated != True:
			return

		if post.view_count < 1000:
			return

		self.user = post.user
		self.score_target = post.id
		self.save_if_new()
		
	def save_if_new(self):
		from django.core.exceptions import ObjectDoesNotExist
		try:
			Scoring.objects.get(
				user_id=self.user_id,
				score_type=self.score_type,
				score_target=self.score_target
			)
		except ObjectDoesNotExist as e:
			self.save()
		except:
			pass
			
	@property
	def target(self):
		target_class = self.targets[self.score_type]
		if target_class:
			return target_class.objects.get(pk=self.score_target)

	@staticmethod
	def letter_count(text):
		from lxml.html import fromstring
		tree = fromstring(text)
		
		import re
		text = tree.text_content()
		text = re.sub(ur'[^\w]', '', text, flags=re.U|re.I)

		return len(text)

	@staticmethod
	def for_post(content, data=False):

		try:
			# from django.utils.html import strip_tags
			# text = strip_tags(content)
			
			from lxml.html import fromstring
			tree = fromstring(content)
			
			import re
			text = tree.text_content()
			text = re.sub(ur'[^\w]', '', text, flags=re.U|re.I)

			d = {
				'image_count': len(tree.xpath(".//img")),
				'video_count': len(tree.xpath(".//iframe")),
				'length': len(text),
			}

			scores = 10
			if d['length'] > 2500:
				scores *= 2

			if d['image_count'] >= 2:
				scores += 5
			if d['video_count'] >= 1:
				scores += 5
		except:
			d = {
				'image_count': 0,
				'video_count': 0,
				'length': 0,
			}
			scores = 0

		if data:
			return scores, d
		return  scores

	@staticmethod
	def reset_user_scores(user):
		tender = Tender.active()
		profile = UserProfile.by_user(user)
		
		scores = 0
		if tender:
			from django.db.models import Sum
			scores = Scoring.objects\
				.filter(user_id=user.id)\
				.filter(created_at__range=tender.date_range())\
				.aggregate(sum=Sum('value'))['sum'] or 0
			posts = Post.objects \
				.get_active() \
				.filter(is_moderated=True,user=user)
			scores += posts.aggregate(sum=Sum('like_count'))['sum'] or 0
			scores += posts.aggregate(sum=Sum('shares_total'))['sum'] or 0
		
		profile.scores = scores
		profile.save()

	def save(self, *args, **kwargs):
		if self.pk is None:
			self.reset_new_scores()
			Scoring.reset_user_scores(self.user)		
		return super(Scoring, self).save(*args, **kwargs)

	def reset_new_scores(self):
		if not self.value:
			return
		profile = UserProfile.by_user(self.user)
		profile.new_scores_sum += self.value
		profile.new_scores_charges += 1
		profile.save()


class ThemePost:

	def __init__(self, id=None, obj=None, typ=None, obj_tuple=None, sub=None):
		
		if id:
			path = id.split('/')
			try:
				self.object_type = path[0] 
				self.object_id = path[1]
			except:
				raise Exception(path)
			self.id = '%s/%s' % (self.object_type, self.object_id)
			self.sub = None 
			if len(path) >= 3 and len(path[2]): 
				self.sub = path[2] 
				self.id = '%s/%s' % (self.id, self.sub)
			self.object = None

			try:
				self.object_id = int(self.object_id)
				if self.object_type in ('description','review') and self.object_id != 'root':
					self.object = Category.objects.get(pk=self.object_id)
				
				elif self.object_type.startswith('product_'):
					self.object = Product.objects.get(pk=self.object_id,primary_product=None)
				
				elif self.object_type == 'topic' and self.object_id != 'root':
					self.object = Topic.objects.get(pk=self.object_id)
			except:
				pass

			return

		if obj:
			self.object = obj
			self.sub = sub

			if isinstance(obj, Category):
				self.id = '%s/%d' % (typ,obj.id)
				self.object_type = typ
				self.object_id = obj.id
			
			if isinstance(obj, Product):
				self.id = 'product_%s/%d' % (typ,obj.id)
				self.object_type = 'product_%s' % typ
				self.object_id = obj.id

			if isinstance(obj, Topic):
				self.id = 'topic/%d' % obj.id
				self.object_type = 'topic'
				self.object_id = obj.id
			
			if self.sub:
				self.id = '%s/%s' % (self.id, self.sub)
		
			return
		
		self.id = 'root'
		self.object_type = 'root'
		self.object = None
		self.sub = None

	@property
	def value(self):
		if not self.is_leaf:
			raise Exception('error val')

		if self.object_type == 'topic':
			object_type = 2
		elif self.object_type == 'product_review':
			object_type = 1
		elif self.object_type == 'product_description':
			object_type = 0

		return object_type, self.object_id
	
	@property
	def title(self):
		if self.object_type == 'root':
			return '---'

		if self.object_type == 'description':
			if self.object_id == 'root':
				return u'Описание спортивного питания'
			if self.sub:
				return u'Остальные'
			return self.object.name

		if self.object_type == 'review':
			if self.object_id == 'root':
				return u'Отзыв о спортивном питании'
			if self.sub:
				return u'Остальные'
			return self.object.name		

		if self.object_type == 'topic':
			if self.object_id == 'root':
				return u'Темы'
			return self.object.title

		if self.object_type.startswith('product_'):
			return '%s %s' % (self.object.brand.name, self.object.name)
			
	@property
	def sub_themes(self):
		ps = []
		
		if self.object_type == 'root':
			ps.append(ThemePost(id='description/root'))
			ps.append(ThemePost(id='review/root'))
			for node in Topic.objects.filter(level=0):
				ps.append(ThemePost(obj=node))

		elif self.object_type in ('description','review'):
			if self.object_id == 'root':
				for node in Category.objects.filter(level=0):
					ps.append(ThemePost(obj=node,typ=self.object_type))	
			else:
				if self.object.is_leaf_node() or self.sub:
					for node in Product.objects.filter(category_id=self.object.id,primary_product=None):
						ps.append(ThemePost(obj=node,typ=self.object_type))
				elif not self.sub:
					for node in self.object.children.all():
						ps.append(ThemePost(obj=node,typ=self.object_type))
					ps.append(ThemePost(obj=self.object,typ=self.object_type,sub='other'))

		elif self.object_type == 'topic':
			if self.object_id == 'root':
				for node in Topic.objects.filter(level=0):
					ps.append(ThemePost(obj=node))	
			elif not self.object.is_leaf_node():
				for node in self.object.children.all():
					ps.append(ThemePost(obj=node))
		
		return ps

	@property
	def pre_themes(self):
		ps = []

		if self.id != 'root':
			ps.append(ThemePost())

		if self.object_type in ('description','review'):
			if self.object_id != 'root':
				ps.append(ThemePost(id='%s/root' % self.object_type))
				include_self = True if self.sub else False
				for node in self.object.get_ancestors(include_self=include_self):
					ps.append(ThemePost(obj=node,typ=self.object_type))

		elif self.object_type.startswith('product_'):
			typ = self.object_type[8:]
			ps.append(ThemePost(id='%s/root' % typ))
			for node in self.object.category.get_ancestors(include_self=True):
				ps.append(ThemePost(obj=node,typ=typ))
			
			# если есть "Остальные"
			if not node.is_leaf_node():
				ps.append(ThemePost(obj=node,typ=typ,sub='other'))

		elif self.object_type == 'topic':
			if self.object_id != 'root':
				for node in self.object.get_ancestors(include_self=False):
					ps.append(ThemePost(obj=node))

		return ps

	@property
	def is_leaf(self):
		if not self.object:
			return False

		if self.object_type.startswith('product_'):
			return True

		elif self.object_type == 'topic':
			if self.object.is_leaf_node():
				return True
		return False

class Admin():
	@staticmethod
	def reset_counters():
		cache.delete_pattern('admin_counters_*')

		posts = Post.objects.filter(is_deleted=False)
		cache.set(
			'admin_counters_posts_all', 
			posts.count(),
			timeout=None
		)
		cache.set(
			'admin_counters_posts_new',
			posts.filter(is_moderated=None,is_published=True).count(),
			timeout=None
		)
		cache.set(
			'admin_counters_posts_draft', 
			posts.filter(is_published=False,is_moderated=False).count(),
			timeout=None
		)
		cache.set(
			'admin_counters_posts_pub', 
			posts.filter(is_published=True).count(),
			timeout=None
		)
		cache.set(
			'admin_counters_posts_moderate', 
			posts.filter(is_published=None,is_moderated=False).count(),
			timeout=None
		)

	def __getattr__(self, prop):
		if prop[:6] == 'count_':
			k = 'admin_counters_'+prop[6:]
			val = cache.get(k)
			if val == None:
				Admin.reset_counters()
				val = cache.get(k)
			return val

		return super(Admin, self).__getattr__(self, prop)

class Tender(models.Model):
	start = models.DateTimeField()
	finish = models.DateTimeField()
	
	winners_page = models.TextField(null=True,blank=True)
	prizes_page = models.TextField(null=True,blank=True)

	@staticmethod
	def active():
		import datetime
		now = datetime.datetime.now()

		tenders = Tender.objects\
			.filter(start__lt=now)\
			.filter(finish__gt=now)

		if len(tenders) != 1:
			return None

		return tenders[0]
	
	@staticmethod
	def last():
		import datetime
		now = datetime.datetime.now()

		tenders = Tender.objects\
			.filter(start__lt=now)\
			.order_by('-finish')

		if len(tenders) != 1:
			return None

		return tenders[0]

	def diff_seconds(self):
		import datetime, time
		now = datetime.datetime.now()

		# convert to unix timestamp
		d1_ts = time.mktime(now.timetuple())
		d2_ts = time.mktime(self.finish.timetuple())

		# they are now in seconds, subtract and then divide by 60 to get minutes.
		return int(d2_ts-d1_ts)

	def date_range(self):
		return [self.start,self.finish]

def ntf_class(cls):
	targets = {}
	for name, method in cls.__dict__.iteritems():
		if hasattr(method, 'type'):
			targets[method.type] = method.target_class

	cls.targets = targets

	return cls

def ntf_method(target_class=None):
	def real_decorator(function):
		def wrapper(self, *args, **kwargs):
			self.event_type = function.__name__
			function(self, *args, **kwargs)
		wrapper.type = function.__name__
		wrapper.target_class = target_class
		return wrapper
	return real_decorator

from forum.models import Message
from qa.models import Answer
@ntf_class
class Notification(models.Model):
	user = models.ForeignKey(User)
	event_target = models.PositiveIntegerField()
	event_type = models.CharField(max_length=100,null=False,blank=False)
	created_at = models.DateTimeField(auto_now_add=True)
	is_active = models.BooleanField(default=True)
	
	class Meta:
		unique_together = (('user', 'event_target', 'event_type'),)

	@ntf_method(Message)
	def forum_message_answer(self, message):
		if not message.reply_to:
			return

		self.user = message.reply_to.user
		self.event_target = message.reply_to.id
		self.save()

	@ntf_method(Comment)
	def post_comment(self, comment):
		if comment.user.id != comment.content_object.user.id:
			self.user = comment.content_object.user
			self.event_target = comment.id
			self.save()

		for user in comment.invocations():
			self.invocation_post_comment(comment, user)

	@ntf_method(Comment)
	def invocation_post_comment(self, comment, user):
		self.user = user
		self.event_target = comment.id
		self.save()

	def new_question(self, question):
		for user in question.invocations():
			self.invocation_question(question, user)

	@ntf_method(Question)
	def invocation_question(self, question, user):
		self.user = user
		self.event_target = question.id
		self.save()

	@ntf_method(Answer)
	def new_answer(self, answer):
		# сообщаем подписавшимся на вопрос
		self.send_question_subs(answer)

		self.user = answer.question.user
		self.event_target = answer.id
		self.save()

	# сообщаем подписавшимся на вопрос
	def send_question_subs(self, answer):
		from qa.models import QuestionSubs
		subs = QuestionSubs.objects.filter(question=answer.question)
		for sub in subs:
			if answer.user.id != sub.user.id:
				Notification().subs_question_new_answer(answer, sub.user)

	@ntf_method(Answer)
	def subs_question_new_answer(self, answer, user):
		self.user = user
		self.event_target = answer.id
		self.save() 

	def save(self, *args, **kwargs):
		if self.pk is None:
			profile = UserProfile.by_user(self.user)
			profile.new_notification_count += 1
			profile.save()
		
		return super(Notification, self).save(*args, **kwargs)

	def save_if_new(self):
		from django.core.exceptions import ObjectDoesNotExist
		try:
			Notification.objects.get(
				user_id=self.user_id,
				event_type=self.event_type,
				event_target=self.event_target
			)
		except ObjectDoesNotExist as e:
			self.save()
		except:
			pass

	@property
	def target(self):
		return self.targets[self.event_type].objects.get(pk=self.event_target)

class Page(models.Model):
	title = models.CharField(max_length=200)
	slug = models.CharField(max_length=200)
	content = models.TextField()

class ViewLog(models.Model):
	ip = models.CharField(max_length=15)
	
	view_type = models.CharField(max_length=20)
	view_target = models.PositiveIntegerField()
	
	created_at = models.DateTimeField(auto_now_add=True)

	@staticmethod
	def add(request, type, target, limit=5):
		key = 'view_list_%s' % type
		view_list = request.session.get(key, [])
		if target in view_list:
			return False

		view_list.append(target)
		request.session[key] = view_list

		x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
		if x_forwarded_for:
			ip = x_forwarded_for.split(',')[0]
		else:
			ip = request.META.get('REMOTE_ADDR')
		
		count = ViewLog.objects.filter(
			ip=ip,
			view_type=type,
			view_target=target
		).count()

		if count >= limit:
			return False

		log = ViewLog()
		log.ip = ip
		log.view_type = type
		log.view_target = target
		log.save()

		return True

	@staticmethod
	def count(type, target):
		return ViewLog.objects.filter(
			view_type=type,
			view_target=target
		).count()

	