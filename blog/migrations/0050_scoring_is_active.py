# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0049_notification_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='scoring',
            name='is_active',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
