# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0056_auto_20150609_1429'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tender',
            name='prizes_page',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tender',
            name='winners_page',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
    ]
