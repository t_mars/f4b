# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0035_auto_20150520_0819'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='refusal_reason',
            field=models.CharField(default=None, max_length=200, null=True),
            preserve_default=True,
        ),
    ]
