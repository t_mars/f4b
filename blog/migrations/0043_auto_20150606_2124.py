# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0042_tender'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scoring',
            name='score_type',
            field=models.IntegerField(choices=[(0, b'POST_PUB'), (1, b'POSTS_5'), (2, b'POST_1000_VIEWS'), (3, b'FORUM_MESSAGE'), (4, b'USER_ENTER'), (5, b'POST_COMMENT'), (6, b'SHARE_POST_COUNT_FB'), (7, b'SHARE_POST_COUNT_TW'), (8, b'SHARE_POST_COUNT_VK'), (9, b'SHARE_POST_COUNT_OK'), (10, b'SHARE_POST_COUNT_GP'), (11, b'POST_FIRST_DESCRIPTION'), (12, b'NEW_QUESTOIN'), (13, b'CORRECT_ANSWER')]),
            preserve_default=True,
        ),
    ]
