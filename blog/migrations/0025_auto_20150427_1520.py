# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0024_post_uniqueness'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='is_moderated',
            field=models.NullBooleanField(default=None),
            preserve_default=True,
        ),
    ]
