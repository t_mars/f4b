# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0055_auto_20150609_1226'),
    ]

    operations = [
        migrations.AddField(
            model_name='tender',
            name='prizes_page',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tender',
            name='winners_page',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
