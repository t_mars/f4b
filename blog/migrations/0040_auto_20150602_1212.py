# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0039_auto_20150602_0923'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eventcounter',
            name='user',
        ),
        migrations.DeleteModel(
            name='EventCounter',
        ),
        migrations.AddField(
            model_name='scoring',
            name='score_target',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='post',
            name='preview_image',
            field=models.ImageField(upload_to=b'upload/img/post'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='scoring',
            name='score_type',
            field=models.IntegerField(choices=[(b'POST_PUB', 0)]),
            preserve_default=True,
        ),
    ]
