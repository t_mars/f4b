# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0066_userprofile_looked_intro'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='looked_intro',
            field=models.NullBooleanField(default=False),
            preserve_default=True,
        ),
    ]
