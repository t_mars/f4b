# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0025_auto_20150427_1520'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='scores',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
