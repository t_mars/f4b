# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0044_auto_20150606_2201'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='new_score_charges',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='new_score_sum',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
