# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0019_auto_20150421_1427'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='complaints',
        ),
        migrations.RemoveField(
            model_name='post',
            name='likes',
        ),
    ]
