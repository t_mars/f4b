# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0026_post_scores'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scoring',
            name='object_id',
        ),
        migrations.AlterField(
            model_name='eventcounter',
            name='object_type',
            field=models.IntegerField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='scoring',
            name='score_type',
            field=models.IntegerField(),
            preserve_default=True,
        ),
    ]
