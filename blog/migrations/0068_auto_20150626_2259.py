# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0067_auto_20150626_1058'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='looked_intro',
        ),
        migrations.AddField(
            model_name='userprofile',
            name='looked_intro_forum',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='looked_intro_menu',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='looked_intro_post',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='looked_intro_qa',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
