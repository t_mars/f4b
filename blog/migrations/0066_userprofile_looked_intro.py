# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0065_auto_20150623_1255'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='looked_intro',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
