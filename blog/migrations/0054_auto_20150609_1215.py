# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0053_auto_20150609_1128'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='scoring',
            unique_together=set([('score_target', 'score_type')]),
        ),
    ]
