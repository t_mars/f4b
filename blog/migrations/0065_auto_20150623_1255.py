# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0064_auto_20150623_1220'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='content',
            new_name='_content'
        ),
        migrations.AlterField(
            model_name='post',
            name='_content',
            field=models.TextField(db_column=b'content'),
            preserve_default=True,
        ),
    ]
