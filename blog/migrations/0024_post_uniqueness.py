# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0023_auto_20150422_1426'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='uniqueness',
            field=models.PositiveIntegerField(default=None, null=True),
            preserve_default=True,
        ),
    ]
