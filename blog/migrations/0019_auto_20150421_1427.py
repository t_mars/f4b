# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0018_auto_20150421_1412'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='complaints',
            field=models.ManyToManyField(to='blog.Complaint'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='likes',
            field=models.ManyToManyField(to='blog.Like'),
            preserve_default=True,
        ),
    ]
