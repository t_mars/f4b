# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0051_auto_20150609_0956'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='last_viewed_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 9, 7, 59, 19, 836095, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
