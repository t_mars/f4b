# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0014_auto_20150421_0942'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='scores',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
