# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0010_eventcounter'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventcounter',
            name='value',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
