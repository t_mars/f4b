# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0059_auto_20150609_2059'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='notification',
            unique_together=set([('user', 'event_target', 'event_type')]),
        ),
    ]
