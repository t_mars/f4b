# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0063_auto_20150623_1211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='_content',
            field=models.TextField(db_column=b'content'),
            preserve_default=True,
        ),
    ]
