# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0054_auto_20150609_1215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='last_viewed_at',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='shares_updated_at',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
    ]
