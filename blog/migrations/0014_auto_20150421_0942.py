# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0013_auto_20150421_0935'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scoring',
            name='date',
        ),
        migrations.AddField(
            model_name='scoring',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 21, 6, 42, 40, 688320, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
