# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0022_post_preview_image'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='is_published',
            new_name='is_moderated',
        ),
    ]
