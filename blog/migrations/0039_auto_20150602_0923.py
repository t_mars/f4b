# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0038_auto_20150521_1348'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='preview_image',
            field=models.ImageField(default='', upload_to=b''),
            preserve_default=False,
        ),
    ]
