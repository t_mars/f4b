# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0027_auto_20150427_1552'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comment',
            old_name='created_date',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='complaint',
            old_name='created_date',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='like',
            old_name='created_date',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='post',
            old_name='created_date',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='post',
            old_name='updated_date',
            new_name='updated_at',
        ),
        migrations.RenameField(
            model_name='scoring',
            old_name='created',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='topic',
            old_name='created_date',
            new_name='created_at',
        ),
        migrations.AddField(
            model_name='post',
            name='moderated_at',
            field=models.DateTimeField(default=None, null=True),
            preserve_default=True,
        ),
    ]
