# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import lib.mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20150409_1003'),
    ]

    operations = [
        migrations.AddField(
            model_name='topic',
            name='parent',
            field=lib.mptt.fields.TreeForeignKey(related_name='children', blank=True, to='blog.Topic', null=True),
            preserve_default=True,
        ),
    ]
