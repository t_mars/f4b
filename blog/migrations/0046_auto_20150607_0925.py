# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0045_auto_20150607_0848'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='new_score_charges',
            new_name='new_scores_charges',
        ),
        migrations.RenameField(
            model_name='userprofile',
            old_name='new_score_sum',
            new_name='new_scores_sum',
        ),
    ]
