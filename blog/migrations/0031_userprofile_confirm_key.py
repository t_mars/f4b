# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0030_post_view_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='confirm_key',
            field=models.CharField(default='', max_length=48),
            preserve_default=False,
        ),
    ]
