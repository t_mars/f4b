#coding=utf8
from django import template

register = template.Library()

@register.inclusion_tag('blog/profile/register_modal.html', takes_context=True)
def register_modal(context):
	from blog.views.profile import RegisterForm

	return {'form': RegisterForm()}

@register.inclusion_tag('blog/profile/login_modal.html', takes_context=True)
def login_modal(context):
	from blog.views.profile import LoginForm

	return {'form': LoginForm(context['request'])}

@register.inclusion_tag('blog/countdown.html', takes_context=True)
def countdown(context):
	from blog.models import Tender

	return {'tender': Tender.active()}

@register.inclusion_tag('blog/admin/menu.html', takes_context=True)
def admin_menu(context):
	from blog.models import Admin

	return {'admin': Admin}

@register.simple_tag(takes_context=True)
def user_profile(context, user=None):
	user = user or context['request'].user
	if user:
		from blog.models import UserProfile
		profile = UserProfile.by_user(user)
	else:
		profile = None
	
	context['profile'] = profile
	
	return ''
	
@register.inclusion_tag('blog/breadcrumbs.html', takes_context=True)
def breadcrumbs_block(context):
	return {'breadcrumbs': context.get('breadcrumbs')}
	
def my_naturaltime(date):
	from django.contrib.humanize.templatetags.humanize import naturalday, naturaltime
	from datetime import datetime
	if date.date() < datetime.today().date():
		return naturalday(date)
	return naturaltime(date)

register.filter('my_naturaltime', my_naturaltime)

@register.inclusion_tag('blog/winner.html', takes_context=True)
def winner(context, user_id, scores=None):
	from django.contrib.auth.models import User
	from blog.models import UserProfile

	user = User.objects.get(pk=user_id)
	profile = UserProfile.by_user(user)

	return {
		'user': user, 
		'profile': profile, 
		'scores': scores,
	}

@register.simple_tag(takes_context=True)
def post_categories(context):
	from blog.models import Topic

	categories = [
		{'title': 'Описания спортпита', 'slug': 'descriptions'}, 
		{'title': 'Отзывы о спортпите', 'slug': 'reviews'}
	]
	for node in Topic.objects.filter(level=0):
		categories.append({'title':node.title, 'slug': node.slug})
	
	context['categories'] = categories
	
	return ''