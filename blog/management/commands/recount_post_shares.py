#coding=utf8

from django.core.management.base import BaseCommand, CommandError

from blog.models import Post
class Command(BaseCommand):

	def handle(self, *args, **options):
		posts = Post.objects.get_active().filter(is_moderated=True)
		for post in posts:
			post.recount_shares_counts()