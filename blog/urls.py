#coding=utf8
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^summernote/', include('lib.django_summernote.urls')),
    url(r'^forum/', include('forum.urls')),
    url(r'^qa/', include('qa.urls')),
    url(r'^captcha/', include('captcha.urls')),

    url(r'^$', 'blog.views.main.home', name='home'),
    url(r'^search/', 'blog.views.main.search', name='search'),
    url(r'^feedback/', 'blog.views.main.feedback', name='feedback'),
    url(r'^winners/', 'blog.views.main.winners', name='winners'),
    url(r'^prizes/', 'blog.views.main.prizes', name='prizes'),
    url(r'^about/(?P<mode>[a-z-]*)/', 'blog.views.main.page', name='page'),

    # администраторские функции
    url(r'^admin/unique_check/(?P<id>\d+)/$', 'blog.views.admin.unique_check', name='admin_post_unique_check'),
    url(r'^admin/post/(?P<id>\d+)/accept/$', 'blog.views.admin.post_accept', name='admin_post_accept'),
    url(r'^admin/post/deny/$', 'blog.views.admin.post_deny', name='admin_post_deny'),
    url(r'^admin/posts/$', 'blog.views.admin.posts', name='admin_posts'),
    url(r'^admin/panel/show/$', 'blog.views.admin.show_panel', name='admin_show_panel'),
    url(r'^admin/panel/hide/$', 'blog.views.admin.hide_panel', name='admin_hide_panel'),
    url(r'^admin/scores/recalc/$', 'blog.views.admin.recalc_scores', name='admin_recalc_scores'),
    url(r'^admin/user/(?P<id>\d*)/scores/$', 'blog.views.admin.user_scores', name='admin_user_scores'),

    url(r'^users/$', 'blog.views.user.rating_scores', name='users'),
    url(r'^users/scores/$', 'blog.views.user.rating_scores', name='users_rating_scores'),
    url(r'^users/pubs/$', 'blog.views.user.rating_pubs', name='users_rating_pubs'),
    url(r'^users/posts/$', 'blog.views.user.rating_posts', name='users_rating_posts'),
    
    url(r'^user/(?P<id>\d*)/$', 'blog.views.user.profile', name='user'),
    url(r'^user/(?P<id>\d*)/posts/$', 'blog.views.user.posts', name='user_posts'),
    
    url(r'^ajax/intro/looked/$', 'blog.views.ajax.looked_intro', {'name': ''}, name='ajax_looked_intro'),
    url(r'^ajax/intro/looked/(?P<name>[\w_]+)$', 'blog.views.ajax.looked_intro'),
    
    url(r'^ajax/scores_for_post/$', 'blog.views.ajax.scores_for_post', name='ajax_scores_for_post'),
    
    url(r'^ajax/subtopic/$', 'blog.views.ajax.subtopic', {'id':''}, name='ajax_subtopic_root'),
    url(r'^ajax/subtopic/(?P<id>.*)$', 'blog.views.ajax.subtopic'),
    
    url(r'^ajax/post_like/$', 'blog.views.ajax.post_like', name='ajax_post_like'),
    url(r'^ajax/post_like/(?P<id>(\d+))/$', 'blog.views.ajax.post_like'),
    
    url(r'^ajax/post_complaint/$', 'blog.views.ajax.post_complaint', name='ajax_post_complaint'),
    url(r'^ajax/post_complaint/(?P<id>(\d+))/$', 'blog.views.ajax.post_complaint'),

    url(r'', include('social_auth.urls')),
    url(r'^profile/social_auth/([\w\d-]+)/$', 'blog.views.profile.social_auth', name='socialauth_complete'),
    
    url(r'^profile/logged-in/$', 'blog.views.profile.logged_in', name='profile_logged_in'),
    url(r'^profile/send_confirm/$', 'blog.views.profile.send_confirm', name='profile_send_confirm'),
    url(r'^profile/profile_confirm/(?P<key>[\w\d]{48})/$', 'blog.views.profile.confirm', name='profile_confirm'),
    url(r'^profile/register/$', 'blog.views.profile.register', name='register'),
    url(r'^profile/login/$', 'blog.views.profile.login', name='login'),
    url(r'^profile/logout/$', 'blog.views.profile.logout', name='logout'),
    url(r'^profile/edit/$', 'blog.views.profile.edit', name='profile_edit'),
    url(r'^profile/post/liked/$', 'blog.views.profile.liked_posts', name='profile_liked_posts'),
    url(r'^profile/posts/$', 'blog.views.profile.posts', name='profile_posts'),
    url(r'^profile/scores/$', 'blog.views.profile.scores', name='profile_scores'),
    url(r'^profile/notifications/$', 'blog.views.profile.notifications', name='profile_notifications'),
    url(r'^profile/questions/$', 'blog.views.profile.questions', name='profile_questions'),
    url(r'^profile/questions/subs$', 'blog.views.profile.subs_questions', name='profile_subs_questions'),
    url(r'^profile/change-password/', 'blog.views.profile.change_password', name='profile_change_password'),
    url(r'^profile/password-reset/', include('lib.password_reset.urls')),

    url(r'^posts/top/$', 'blog.views.post.list', {'mode': 'top'}, name='post_list_top'),
    url(r'^posts/$', 'blog.views.post.list', {'mode': 'new'}, name='post_list'),
    url(r'^posts/(?P<slug>[\w-]+)/(?P<id>[\d]+)/$', 'blog.views.post.category', name='post_category_id'),
    url(r'^posts/(?P<slug>[\w-]+)/$', 'blog.views.post.category', {'id': ''}, name='post_category'),

    url(r'^post/create/$', 'blog.views.post.create', name='post_create'),
    url(r'^post/(?P<id>\d+)/delete/$', 'blog.views.post.delete', name='post_delete'),
    url(r'^post/(?P<id>\d+)/edit/$', 'blog.views.post.edit', name='post_edit'),
    url(r'^post/(?P<id>\d+)/$', 'blog.views.post.show', {'slug': ''}),
    url(ur'^post/(?P<id>\d+)/(?P<slug>[\w\s-]+)/$', 'blog.views.post.show', name='post'),

)
